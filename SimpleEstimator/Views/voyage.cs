﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using SimpleEstimator.Views;
using Model.DataSet;

namespace SimpleEstimator.Views
{
    public partial class voyage : DevExpress.XtraEditors.XtraUserControl
    {
        SEData sEData;
        ViewModel.voyageViewModel viewModel;
        public double FrtSummary=0, AcommSummary=0, BrkgSummary=0, FrtTaxSummary=0;
        public DateTime TimeMin = new DateTime();

        public voyage()
        {
            InitializeComponent();
        }

        private void CargoGrid_CustomDrawColumnHeader(object sender, DevExpress.XtraGrid.Views.Grid.ColumnHeaderCustomDrawEventArgs e)
        {
            if (e.Column == null) return;
            if (e.Column.AppearanceHeader.BackColor != Color.Empty)
            {
                e.Info.AllowColoring = true;
            }
        }

        private void voyage_Load(object sender, EventArgs e)
        {
            sEData = new Model.DataSet.SEData();
            sEData.DataInit();
            sEDataBindingSource.DataSource = sEData;
            CargoView.Columns[4].ColumnEdit = persistentRepository1.Items[1];
            viewModel = new ViewModel.voyageViewModel(sEData);
        }

        private void VesselMVView_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Tab && VesselMVView.FocusedColumn.AbsoluteIndex == 3)
            {
                VesselSpdView.FocusedColumn.AbsoluteIndex = 0;
                VesselSpdView.Focus();
            } //화살표 이벤트
            if (e.KeyCode == Keys.Down)
            {
                CargoView.Focus();
            }
        }

        private void VesselSpdView_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Tab && VesselSpdView.FocusedColumn.AbsoluteIndex == 1)
            {
                VesselFOView.FocusedColumn.AbsoluteIndex = 0;
                VesselFOView.FocusedRowHandle = 0;
                VesselFOView.Focus();
            }
            //화살표 이벤트
            if (e.KeyCode == Keys.Down)
            {
                CargoView.Focus();
            }
        }

        private void VesselFOView_KeyDown(object sender, KeyEventArgs e)
        {
            if ((e.KeyCode == Keys.Tab || e.KeyCode == Keys.Right) && VesselFOView.FocusedColumn.AbsoluteIndex == 5 && VesselFOView.FocusedRowHandle == 0)
            {
                VesselDOView.FocusedRowHandle = 0;
                VesselDOView.FocusedColumn.AbsoluteIndex = 1;
                VesselDOView.Focus();
            }
            if ((e.KeyCode == Keys.Tab || e.KeyCode == Keys.Right) && VesselFOView.FocusedColumn.AbsoluteIndex == 5 && VesselFOView.FocusedRowHandle == 1)
            {
                VesselDOView.FocusedRowHandle = 1;
                VesselDOView.FocusedColumn.AbsoluteIndex = 1;
                VesselDOView.Focus();
            }
            //화살표 이벤트
            if (e.KeyCode == Keys.Down && VesselFOView.FocusedRowHandle == 1)
            {
                CargoView.Focus();
            }
        }

        private void VesselDOView_KeyDown(object sender, KeyEventArgs e)
        {//tab Key event
            if ((e.KeyCode == Keys.Tab || e.KeyCode == Keys.Right) && VesselDOView.FocusedColumn.AbsoluteIndex == 4 && VesselDOView.FocusedRowHandle == 0)
            {
                VesselFOView.FocusedRowHandle = 1;
                VesselFOView.FocusedColumn.AbsoluteIndex = 1;
                VesselFOView.Focus();
            }
            if (e.KeyCode == Keys.Tab && VesselDOView.FocusedColumn.AbsoluteIndex == 4 && VesselDOView.FocusedRowHandle == 1)
            {
                CargoView.FocusedRowHandle = 0;
                CargoView.FocusedColumn.AbsoluteIndex = 1;
                CargoView.Focus();
            }
            //화살표 이벤트
            if (e.KeyCode == Keys.Down && VesselDOView.FocusedRowHandle == 1)
            {
                CargoView.Focus();
            }
        }

        private void CargoView_KeyDown(object sender, KeyEventArgs e)
        {
            //tab Key event
            if (e.KeyCode == Keys.Tab && CargoView.FocusedColumn.AbsoluteIndex == CargoView.Columns.Count - 1 && CargoView.FocusedRowHandle == CargoView.RowCount - 1)
            {
                PortRotationView.FocusedRowHandle = 0;
                PortRotationView.FocusedColumn = PortRotationView.Columns[2];
                PortRotationView.Focus();
            }// up key event
            if (e.KeyCode == Keys.Up && CargoView.FocusedRowHandle == 0)
            {
                VesselSpdView.Focus();
            }//down key event
            if (e.KeyCode == Keys.Down && CargoView.FocusedRowHandle == CargoView.RowCount - 1)
            {
                PortRotationView.Focus();
            }
        }

        private void PortRotationView_KeyDown(object sender, KeyEventArgs e)
        {   //tab Key eventeu
            if (e.KeyCode == Keys.Tab)
            {
                if (PortRotationView.FocusedRowHandle == PortRotationView.RowCount-1)
                {
                    if (getIdle() <= 0 && PortRotationView.FocusedColumn.Name == "PortRSea")
                    {
                        ResultOEView.FocusedRowHandle = 1;
                        ResultOEView.FocusedColumn = ResultOEView.Columns[3];
                        ResultOEView.Focus();
                        return;
                    }
                    else if (getWork() <= 0 && PortRotationView.FocusedColumn.Name == "PortRIdle")
                    {
                        ResultOEView.FocusedRowHandle = 1;
                        ResultOEView.FocusedColumn = ResultOEView.Columns[3];
                        ResultOEView.Focus();
                        return;
                    }
                    else if(PortRotationView.FocusedColumn.Name == "PortRWork")
                    {
                        ResultOEView.FocusedRowHandle = 1;
                        ResultOEView.FocusedColumn = ResultOEView.Columns[3];
                        ResultOEView.Focus();
                        return;
                    }                    
                }
                
            }// up key event
            if (e.KeyCode == Keys.Up && PortRotationView.FocusedRowHandle == 0)
            {
                CargoView.Focus();
            }//down key event
            if (e.KeyCode == Keys.Down && PortRotationView.FocusedRowHandle == PortRotationView.RowCount - 1)
            {
                ResultOEView.FocusedRowHandle = 1;
                ResultOEView.FocusedColumn = ResultOEView.Columns[3];
                ResultOEView.Focus();
            }
        }

        private void ResultOEView_KeyDown(object sender, KeyEventArgs e)
        {//tab 이벤트
            if ((e.KeyCode == Keys.Tab) && (ResultOEView.FocusedColumn.AbsoluteIndex) == 3 && (ResultOEView.FocusedRowHandle == 5))
            {
                ResultBEView.FocusedRowHandle = 0;
                ResultBEView.FocusedColumn = ResultBEView.Columns[2];
                ResultBEView.Focus();
            }  //화살표 이벤트
            if (e.KeyCode == Keys.Right && ResultOEView.FocusedColumn.AbsoluteIndex == 3)
            {
                ResultBEView.FocusedColumn = ResultBEView.Columns[2];
                ResultBEView.Focus();
            }
        }

        private void ResultBEView_KeyDown(object sender, KeyEventArgs e)
        {
            //tab 이벤트
            if ((e.KeyCode == Keys.Tab) && ResultBEView.FocusedColumn.AbsoluteIndex == 4 && ResultBEView.FocusedRowHandle == 0)
            {
                ResultBEView.FocusedRowHandle = 3;
                ResultBEView.FocusedColumn = ResultBEView.Columns[1];

            }
            if ((e.KeyCode == Keys.Tab) && ResultBEView.FocusedColumn.AbsoluteIndex == 4 && ResultBEView.FocusedRowHandle == 3)
            {
                ResultCBView.FocusedRowHandle = 0;
                ResultCBView.FocusedColumn = ResultBEView.Columns[1];
                ResultCBView.Focus();
            }  //화살표 이벤트
            if (e.KeyCode == Keys.Right && ResultBEView.FocusedColumn.AbsoluteIndex == 4)
            {
                ResultCBView.FocusedColumn = ResultBEView.Columns[1];
                ResultCBView.Focus();
            }
            if (e.KeyCode == Keys.Left && ResultBEView.FocusedColumn.AbsoluteIndex == 2)
            {
                ResultOEView.Focus();
            }
        }

        private void ResultCBView_KeyDown(object sender, KeyEventArgs e)
        {  //화살표 이벤트
            if (e.KeyCode == Keys.Left)
            {
                ResultBEView.FocusedColumn.AbsoluteIndex = 4;
                ResultBEView.Focus();
            }
            if (e.KeyCode == Keys.Tab)
            {
                if (ResultCBView.FocusedRowHandle == 1)
                {
                    ResultCBView.FocusedRowHandle = 0;
                }
            }
        }

        private void VesselMVControl_EditorKeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                SendKeys.Send("{TAB}");
            }
        }

        private void VesselSpdControl_EditorKeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                SendKeys.Send("{TAB}");
            }
        }

        private void VesselFOControl_EditorKeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                SendKeys.Send("{TAB}");
            }
        }

        private void VesselDOControl_EditorKeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                SendKeys.Send("{TAB}");
            }
        }

        private void CargoGridControl_EditorKeyDown(object sender, KeyEventArgs e)
        {
            int FocusedColumnIndex = CargoView.FocusedColumn.AbsoluteIndex;
            int FocusedRowIndex = CargoView.FocusedRowHandle;
            if (e.KeyCode == Keys.Enter)
            {
                if (FocusedColumnIndex < CargoView.Columns.Count - 1)
                {
                    SendKeys.Send("{TAB}");
                }
                else if (FocusedRowIndex < CargoView.RowCount - 1 && FocusedColumnIndex == CargoView.Columns.Count - 1)
                {
                    SendKeys.Send("{TAB}");
                }
                else if (CargoView.RowCount < 20)
                {
                    sEData.Cargo.Rows.Add();
                    sEData.Cargo.Rows[FocusedRowIndex + 1]["Index"] = FocusedRowIndex + 2;
                    //MessageBox.Show((string)sEData.Cargo.Rows[2][4]);
                    CargoPanel.Height += 31;
                    CargoGridControl.Height += 31;
                    voyagepanel.Height += 31;
                    SendKeys.Send("{TAB}");
                }
                else
                {
                    SendKeys.Send("{TAB}");
                }
            }
        }

        private void PortRotationGridControl_EditorKeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && !(PortRotationView.FocusedColumn.AbsoluteIndex == 2))
            {
                if (PortRotationView.FocusedColumn.AbsoluteIndex == 14)
                {
                    PortRotationView.FocusedColumn = PortRotationView.Columns[8];
                    PortRotationView.MoveNext();
                }
                else {
                    SendKeys.Send("{TAB}");
                }
            }
        }

        private void ResultOEGridControl_EditorKeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                SendKeys.Send("{TAB}");
            }
        }

        private void ResultBEGridControl_EditorKeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                SendKeys.Send("{TAB}");
            }
        }

        private void ResultCBGridControl_EditorKeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (ResultCBView.FocusedRowHandle == 1)
                {
                }
                else {
                    SendKeys.Send("{TAB}");
                }
            }
        }

        private void ResultOEView_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            int[] FocusOutRow = { 0 };
            if (FocusOutRow.Contains(e.FocusedRowHandle))
            {
                ResultOEView.Columns[3].OptionsColumn.AllowFocus = false;
            }
            else {
                ResultOEView.Columns[3].OptionsColumn.AllowFocus = true;
            }
        }

        private void ResultBEView_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            int[] FocusOutRow = { 1, 2, 4, 5 };
            if (FocusOutRow.Contains(e.FocusedRowHandle))
            {
                ResultBEView.Columns[2].OptionsColumn.AllowFocus = false;
                ResultBEView.Columns[4].OptionsColumn.AllowFocus = false;
            }
            else {
                ResultBEView.Columns[2].OptionsColumn.AllowFocus = true;
                ResultBEView.Columns[4].OptionsColumn.AllowFocus = true;
            }
        }

        private void ResultCBView_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            int[] FocusOutRow = { 2, 3 };
            if (FocusOutRow.Contains(e.FocusedRowHandle))
            {
                ResultCBView.Columns[1].OptionsColumn.AllowFocus = false;
            }
            else {
                ResultCBView.Columns[1].OptionsColumn.AllowFocus = true;
            }
        }

        private void PortRotationView_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            int MarginIndex = PortRotationView.RowCount - 1;
            // Port Attribute Focus
            PortRotationView.Columns[2].OptionsColumn.AllowFocus = true;
            int BallastCheck = -1;
            string PortValue = sEData.PortR.Rows[e.FocusedRowHandle]["Name"].ToString();

            if (PortValue == "")
            {
                for (int j = 3; j < 17; j++)
                {
                    PortRotationView.Columns[j].OptionsColumn.AllowFocus = false;
                }
            }
            else
            {
                for (int j = 3; j < 17; j++)
                {
                    PortRotationView.Columns[j].OptionsColumn.AllowFocus = true;
                }
                PortRotationView.Columns["Sea"].OptionsColumn.AllowFocus = false;
            }
            #region Ballast에 의한 Distance exception 처리
            for (int i = 0; i < MarginIndex; i++)
            {
                PortValue = sEData.PortR.Rows[i]["Name"].ToString();
                if (PortValue != "")
                {
                    BallastCheck += 1;
                }
                if (BallastCheck == 0)
                {
                    BallastCheck = i;
                    break;
                }
            }
            if (e.FocusedRowHandle == BallastCheck)
            {
                for (int j = 3; j < 9; j++)
                {
                    PortRotationView.Columns[j].OptionsColumn.AllowFocus = false;
                }
            }
            #endregion

            // Time focus
            for (int i = 16; i < 18; i++)
            {
                PortRotationView.Columns[i].OptionsColumn.AllowFocus = false;
            }

            if (e.FocusedRowHandle == BallastCheck)
            {
                object TypeValue = sEData.PortR.Rows[BallastCheck]["Type"];
                if (TypeValue.Equals("Ballast"))
                {
                    PortRotationView.Columns["Depature"].OptionsColumn.AllowFocus = true;
                }
                else
                {
                    PortRotationView.Columns["Arrival"].OptionsColumn.AllowFocus = true;
                }
            }

            // Ballast Margin Focus
            // Type Unfocusing
           
            string hasForienKey = sEData.PortR.Rows[e.FocusedRowHandle]["CargoPK"].ToString();
            if (hasForienKey == "")
            {
                PortRotationView.Columns[1].OptionsColumn.AllowFocus = true;
            }
            if ((e.FocusedRowHandle == 0) || (e.FocusedRowHandle == MarginIndex))
            {
                PortRotationView.Columns[1].OptionsColumn.AllowFocus = false;
            }
            // Margin Attribute Focusing
            if (sEData.PortR.Rows[e.FocusedRowHandle]["Type"].ToString() == "Margin")
            {
                PortRotationView.Columns[8].OptionsColumn.AllowFocus = true; //Sea
                PortRotationView.Columns[2].OptionsColumn.AllowFocus = false;//Name
                if (getIdle() > 0)
                {
                    PortRotationView.Columns["Idle"].OptionsColumn.AllowFocus = true;
                }
                if (getWork() > 0)
                {
                    PortRotationView.Columns["Work"].OptionsColumn.AllowFocus = true;
                }
            }

        }

        private void ResultOEView_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if ((e.RowHandle == 0 || e.RowHandle == 5) && e.Column.AbsoluteIndex == 3)
            {
                e.Cache.FillRectangle(Color.FromArgb(255, 255, 192), e.Bounds);
                e.Appearance.DrawString(e.Cache, e.DisplayText, e.Bounds);
                e.Handled = true;
            }
        }

        private void ResultBEView_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if ((e.RowHandle == 1 || e.RowHandle == 2 || e.RowHandle == 4 || e.RowHandle == 5) && (e.Column.AbsoluteIndex == 2 || e.Column.AbsoluteIndex == 4))
            {
                e.Cache.FillRectangle(Color.FromArgb(255, 255, 192), e.Bounds);
                e.Appearance.DrawString(e.Cache, e.DisplayText, e.Bounds);
                e.Handled = true;
            }
        }

        private void ResultCBView_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if ((e.RowHandle == 3 || e.RowHandle == 2) && e.Column.AbsoluteIndex == 1)
            { 
               
                e.Cache.FillRectangle(Color.FromArgb(255, 255, 192), e.Bounds);
                e.Appearance.DrawString(e.Cache, e.DisplayText, e.Bounds);
                e.Handled = true;
            }
            if (e.RowHandle == 1 && e.Column.AbsoluteIndex == 1)
            {
                e.Appearance.DrawString(e.Cache, string.Format("{0:#0.000}%",e.DisplayText) , e.Bounds);
                e.Handled = true;
            }
        }

        private void PortRotationView_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            int MarginIndex = PortRotationView.RowCount - 1;
            string ColumnName = e.Column.Name;
            // Port Attribute Color

            int BallastCheck = -1;
            string PortType = sEData.PortR.Rows[e.RowHandle]["Type"].ToString();
            string PortValue = sEData.PortR.Rows[e.RowHandle]["Name"].ToString();
            if (PortType != "")
            {
                if (ColumnName.Equals("PortRType"))//type
                {
                    e.Cache.FillRectangle(Color.FromArgb(232, 234, 249), e.Bounds);
                    e.Appearance.DrawString(e.Cache, e.DisplayText, e.Bounds);
                    e.Handled = true;
                }
            }
            if (PortValue == "")
            {
                if (!ColumnName.Equals("PortRType") && !ColumnName.Equals("PortRName") && !ColumnName.Equals("PortRIndex")) //PortName 이 공란일 경우
                {
                    e.Cache.FillRectangle(Color.FromArgb(232, 234, 249), e.Bounds);
                    e.Appearance.DrawString(e.Cache, e.DisplayText, e.Bounds);
                    e.Handled = true;
                }
            }
            else //Port 네임이 있을 경우
            {
                //Portname color by port type 
                if (ColumnName.Equals("PortRName"))
                {
                    if (PortType == "Loading")
                    {
                        e.Cache.FillRectangle(Color.FromArgb(220, 220, 226), e.Bounds);
                        e.Appearance.DrawString(e.Cache, e.DisplayText, e.Bounds);
                        e.Handled = true;
                    }
                    if (PortType == "Dischg.")
                    {
                        e.Cache.FillRectangle(Color.FromArgb(254, 184, 141), e.Bounds);
                        e.Appearance.DrawString(e.Cache, e.DisplayText, e.Bounds);
                        e.Handled = true;
                    }
                }
                if (ColumnName.Equals("PortRDistance"))
                {
                    e.Cache.FillRectangle(Color.FromArgb(255, 255, 192), e.Bounds);
                    e.Appearance.DrawString(e.Cache, e.DisplayText, e.Bounds);
                    e.Handled = true;
                }
                if (ColumnName.Equals("PortRSeca"))
                {
                    e.Cache.FillRectangle(Color.FromArgb(197, 222, 207), e.Bounds);
                    e.Appearance.DrawString(e.Cache, e.DisplayText, e.Bounds);
                    e.Handled = true;
                }
                if (ColumnName.Equals("PortRSea"))
                {
                    e.Cache.FillRectangle(Color.FromArgb(255, 255, 192), e.Bounds);
                    e.Appearance.DrawString(e.Cache, e.DisplayText, e.Bounds);
                    e.Handled = true;
                }
                if (ColumnName.Equals("PortRIdle"))
                {
                    e.Cache.FillRectangle(Color.FromArgb(255, 255, 192), e.Bounds);
                    e.Appearance.DrawString(e.Cache, e.DisplayText, e.Bounds);
                    e.Handled = true;
                }
                if (ColumnName.Equals("PortRWork"))
                {
                    e.Cache.FillRectangle(Color.FromArgb(255, 255, 192), e.Bounds);
                    e.Appearance.DrawString(e.Cache, e.DisplayText, e.Bounds);
                    e.Handled = true;
                }
                if (ColumnName.Equals("PortRDem"))
                {
                    e.Cache.FillRectangle(Color.FromArgb(255, 255, 192), e.Bounds);
                    e.Appearance.DrawString(e.Cache, e.DisplayText, e.Bounds);
                    e.Handled = true;
                }
                if (ColumnName.Equals("PortRDes"))
                {
                    e.Cache.FillRectangle(Color.FromArgb(255, 255, 192), e.Bounds);
                    e.Appearance.DrawString(e.Cache, e.DisplayText, e.Bounds);
                    e.Handled = true;
                }

            }// 

            #region Ballast에 의한 Distance exception 처리
            for (int i = 0; i < MarginIndex; i++)
            {
                PortValue = sEData.PortR.Rows[i]["Name"].ToString();
                if (PortValue != "")
                {
                    BallastCheck += 1;
                }
                if (BallastCheck == 0)
                {
                    BallastCheck = i;
                    break;
                }
            }
            if (e.RowHandle == BallastCheck)
            {
                if (ColumnName.Equals("PortRDistance")|| ColumnName.Equals("PortRSeca")|| ColumnName.Equals("PortRWF")|| ColumnName.Equals("PortRSpd")|| ColumnName.Equals("PortRSea"))
                {
                    e.Cache.FillRectangle(Color.FromArgb(232, 234, 249), e.Bounds);
                    e.Appearance.DrawString(e.Cache, e.DisplayText, e.Bounds);
                    e.Handled = true;
                }
            }
            #endregion

            // Ballast Margin Color

            // Margin Attribute color
            if (sEData.PortR.Rows[e.RowHandle]["Type"].ToString() == "Margin")
            {
                if (ColumnName.Equals("PortRSea"))
                {
                    e.Cache.FillRectangle(Color.White, e.Bounds);
                    e.Appearance.DrawString(e.Cache, e.DisplayText, e.Bounds);
                    e.Handled = true;
                }
                if (getIdle() > 0 && ColumnName.Equals("PortRIdle"))
                {
                    e.Cache.FillRectangle(Color.White, e.Bounds);
                    e.Appearance.DrawString(e.Cache, e.DisplayText, e.Bounds);
                    e.Handled = true;
                }
                if (getWork() > 0 && ColumnName.Equals("PortRWork"))
                {
                    e.Cache.FillRectangle(Color.White, e.Bounds);
                    e.Appearance.DrawString(e.Cache, e.DisplayText, e.Bounds);
                    e.Handled = true;
                }
                if (ColumnName.Equals("PortRName"))
                {
                    e.Cache.FillRectangle(Color.FromArgb(232, 234, 249), e.Bounds);
                    e.Appearance.DrawString(e.Cache, e.DisplayText, e.Bounds);
                    e.Handled = true;
                }
            }

        }

        private void CargoView_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            string DischargingPort = sEData.Cargo.Rows[e.RowHandle]["DischargingPort"].ToString();
            if (DischargingPort == ""&& (e.Column.AbsoluteIndex == 4 || e.Column.AbsoluteIndex == 5))
            {
                e.Cache.FillRectangle(Color.White, e.Bounds);
                e.Appearance.DrawString(e.Cache, e.DisplayText, e.Bounds);
                e.Handled = true;
            }
            else
            {
                if (e.Column.AbsoluteIndex == 4)
                {
                    e.Cache.FillRectangle(Color.FromArgb(220, 220, 226), e.Bounds);
                    e.Appearance.DrawString(e.Cache, e.DisplayText, e.Bounds);
                    e.Handled = true;
                }
                if (e.Column.AbsoluteIndex == 5)
                {
                    e.Cache.FillRectangle(Color.FromArgb(254, 184, 141), e.Bounds);
                    e.Appearance.DrawString(e.Cache, e.DisplayText, e.Bounds);
                    e.Handled = true;
                }
            }
        }

        private void CargoView_CustomDrawFooterCell(object sender, DevExpress.XtraGrid.Views.Grid.FooterCellCustomDrawEventArgs e)
        {
            e.Cache.FillRectangle(SystemColors.ButtonFace, e.Bounds);
            e.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            StringFormat format = new StringFormat();
            format.LineAlignment = StringAlignment.Far;
            format.Alignment = StringAlignment.Far;


            if (e.Column.AbsoluteIndex == 8) // Frt
            {

                Appearance.DrawString(e.Cache, string.Format("{0:#,##0.000}",FrtSummary), e.Bounds, e.Appearance.Font, format);
                e.Handled = true;
            }
            else if (e.Column.AbsoluteIndex == 13) // FrtTax
            {
                Appearance.DrawString(e.Cache, string.Format("{0:##0.000}%", FrtTaxSummary), e.Bounds, e.Appearance.Font, format);
                e.Handled = true;
            }
            else if (e.Column.AbsoluteIndex == 11) // A Comm
            {
                Appearance.DrawString(e.Cache, string.Format("{0:##0.000}%", AcommSummary), e.Bounds, e.Appearance.Font, format);
                e.Handled = true;
            }
            else if (e.Column.AbsoluteIndex == 12) // Brkg
            {
                Appearance.DrawString(e.Cache, string.Format("{0:##0.000}%", BrkgSummary), e.Bounds, e.Appearance.Font, format);
                e.Handled = true;
            }
            else
            {
                Appearance.DrawString(e.Cache, e.Info.DisplayText, e.Bounds, e.Appearance.Font, format);
                e.Handled = true;
            }
        }

        private void PortRotationView_CustomDrawFooterCell(object sender, DevExpress.XtraGrid.Views.Grid.FooterCellCustomDrawEventArgs e)
        {
            e.Cache.FillRectangle(SystemColors.ButtonFace, e.Bounds);
            e.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            StringFormat format = new StringFormat();
            format.LineAlignment = StringAlignment.Far;
            format.Alignment = StringAlignment.Far;
            
            Font font = e.Appearance.Font;            

            FontStyle fs = e.Appearance.Font.Style;
            
            if (e.Column.Name == "PortRName")
            {
                fs |= FontStyle.Bold;
                font = new Font(e.Appearance.Font, fs);
                e.Appearance.Font = new Font(e.Appearance.Font, fs);
                Appearance.DrawString(e.Cache, e.Info.DisplayText, e.Bounds, font, format);
                e.Handled = true;
            }
            if (e.Column.Name == "PortRArrival")
            {
                fs |= FontStyle.Bold;
                font = new Font(e.Appearance.Font, fs);
                e.Appearance.Font = new Font(e.Appearance.Font, fs);
                if (TimeMin.ToOADate()>0)
                {
                    Appearance.DrawString(e.Cache, TimeMin.ToString("yyyy-MM-dd HH:mm"), e.Bounds, font, format);
                }
                else
                {
                    Appearance.DrawString(e.Cache, e.Info.DisplayText, e.Bounds, font, format);
                }
                e.Handled = true;
            }
            if (e.Column.Name == "PortRDepature")
            {
                fs |= FontStyle.Bold;
                font = new Font(e.Appearance.Font, fs);
                e.Appearance.Font = new Font(e.Appearance.Font, fs);
                Appearance.DrawString(e.Cache, e.Info.DisplayText, e.Bounds, font, format);
                e.Handled = true;
            }
            else if (e.Column.Name == "PortRDistance")
            {
                fs |= FontStyle.Bold;
                font = new Font(e.Appearance.Font, fs);       
                e.Appearance.Font = new Font(e.Appearance.Font, fs);
                Appearance.DrawString(e.Cache, e.Info.DisplayText, e.Bounds, font, Brushes.Red, format);
                e.Handled = true;
            }
            else if (e.Column.Name == "PortRSeca")
            {
                fs |= FontStyle.Bold;
                font = new Font(e.Appearance.Font, fs);
                e.Appearance.Font = new Font(e.Appearance.Font, fs);
                Appearance.DrawString(e.Cache, e.Info.DisplayText, e.Bounds, font, Brushes.Green, format);
                e.Handled = true;
            }
            else
            {
                Appearance.DrawString(e.Cache, e.Info.DisplayText, e.Bounds, font, format);
                e.Handled = true;
            }
        }

        private void CargoView_CustomRowCellEdit(object sender, DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventArgs e)
        {
            int MarginIndex = CargoView.RowCount - 1;
            string LoadingValue, DischargingValue;
            for (int i = 0; i < MarginIndex; i++)
            {
                LoadingValue = sEData.Cargo.Rows[i]["LoadingPort"].ToString();
                DischargingValue = sEData.Cargo.Rows[i]["DischargingPort"].ToString();

                if ((LoadingValue == "" ) && (e.Column.Name == "LoadingPort"))
                {
                    e.RepositoryItem = CargoEditBtn;
                }
                if ((DischargingValue == "") && (e.Column.Name =="DischargingPort"))
                {
                    e.RepositoryItem = CargoEditBtn;
                }
            }

        }

        private void PortRotationView_CustomRowCellEdit(object sender, DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventArgs e)
        {//Time Edit Set
            int BallastCheck = -1;
            int MarginIndex = PortRotationView.RowCount - 1;
            string PortValue;
            //BallastCheck
            for (int i = 0; i < MarginIndex; i++)
            {
                PortValue = sEData.PortR.Rows[i]["Name"].ToString();
                if (PortValue != "")
                {
                    BallastCheck += 1;
                }
                if (BallastCheck == 0)
                {
                    BallastCheck = i;
                    break;
                }
            }
            if (e.RowHandle == BallastCheck)
            {
                PortValue = sEData.PortR.Rows[BallastCheck]["Type"].ToString();
                if (PortValue == "Ballast")
                {
                    if (e.Column.Name == "PortRDepature")
                    {
                        e.RepositoryItem = DateEdit;
                    }
                }
                else
                {
                    if (e.Column.Name == "PortRArrival")
                    {
                        e.RepositoryItem = DateEdit;
                    }
                }
            }
            // PortName Detail, L/D Rate Crane PortCharge Edit Set
            PortValue = sEData.PortR.Rows[e.RowHandle]["Name"].ToString();
            if (PortValue == "")
            {
                if (e.Column.Name == "PortRName" && sEData.PortR.Rows[e.RowHandle]["Type"].ToString()!= "Margin")
                {
                    e.RepositoryItem = PortREditBtn;
                }
            }
            else
            {
                if (e.Column.Name == "PortRNameDetail")
                {
                    e.RepositoryItem = CraneBtn;
                }
                if (e.Column.Name == "PortRLDRate" || e.Column.Name == "PortRPortCharge")
                {
                    e.RepositoryItem = ZoomBtn;
                }
                if (e.Column.Name == "PortRCrane")
                {
                    e.RepositoryItem = NameDetailBtn;
                }
            }
        }

        private void EditBtn_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            int row = PortRotationView.FocusedRowHandle;           
            string result = Prompt.ShowDialog("PortName을 입력해주세요.", "PortName 입력");
            PortRotationView.SetRowCellValue(row, PortRotationView.FocusedColumn, result);
            PortRotationView.MoveNext();
        }        

        private void CargoEditBtn_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            int row = CargoView.FocusedRowHandle;
            string result = Prompt.ShowDialog("PortName을 입력해주세요.", "PortName 입력");
            CargoView.SetRowCellValue(row, CargoView.FocusedColumn, result);
            SendKeys.Send("{TAB}");
        }

        private void VesselFOView_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            viewModel = new ViewModel.voyageViewModel(sEData);
            viewModel.VesselFusselChanged();
        }

        private void VesselDOView_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            viewModel = new ViewModel.voyageViewModel(sEData);
            viewModel.VesselFusselChanged();
        }

        private void CargoView_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            viewModel = new ViewModel.voyageViewModel(sEData);
            viewModel.CargoChanged();
            CargoValidate(e.RowHandle);
            FrtSummary = (double)sEData.SummaryValues.Rows[0]["Frt"];
            AcommSummary = (double)sEData.SummaryValues.Rows[0]["Acomm"];
            BrkgSummary = (double)sEData.SummaryValues.Rows[0]["Brkg"];
            FrtTaxSummary = (double)sEData.SummaryValues.Rows[0]["FrtTax"];
        }

        private void PortRotationView_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            viewModel = new ViewModel.voyageViewModel(sEData);
            viewModel.PortRotationChanged();
            DurationLabel.Text = viewModel.DurationChanged();
            if (sEData.SummaryValues.Rows[0]["TimeMin"].ToString() !="")
            {
                TimeMin = (DateTime)sEData.SummaryValues.Rows[0]["TimeMin"];
            }
            PortRValidate(e.RowHandle);
        }

        private void ResultOEView_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            viewModel = new ViewModel.voyageViewModel(sEData);
            viewModel.OEChanged();
        }

        private void ResultBEView_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            viewModel = new ViewModel.voyageViewModel(sEData);
            viewModel.BEChanged();
        }

        private void ResultCBView_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            viewModel = new ViewModel.voyageViewModel(sEData);
            viewModel.ResultChanged();
            ResultValidate();
        }

        private void VesselSpdView_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        { 
            viewModel = new ViewModel.voyageViewModel(sEData);
            viewModel.VesselSpeedChanged();
        }

        private void CargoAppend_Click(object sender, EventArgs e)
        {
            if (sEData.Cargo.Count > 19)
                return;
            sEData.Cargo.Rows.Add();
            sEData.Cargo.Rows[sEData.Cargo.Count - 1]["Index"] = sEData.Cargo.Count;
            CargoGridControl.Height += 31;
            CargoPanel.Height += 31;
        }

        private void CargoInsert_Click(object sender, EventArgs e)
        {
            if (sEData.Cargo.Count > 19)
                return;
            DataRow dataRow = sEData.Cargo.NewRow();
            dataRow["Index"] = 99;
            sEData.Cargo.Rows.InsertAt(dataRow, CargoView.FocusedRowHandle);
            SetCargoIndex();
            CargoGridControl.Height += 31;
            CargoPanel.Height += 31;
        }       

        private void CargoClear_Click(object sender, EventArgs e)
        {
            sEData.Cargo.Clear();
            sEData.Cargo.Rows.Add();
            sEData.Cargo.Rows.Add();
            SetCargoIndex();
            sEData.PortRInit();
            CargoGridControl.Height = 136;
            CargoPanel.Height = 225;
        }

        private void CargoRemove_Click(object sender, EventArgs e)
        {
            int CargoPK;
            if (sEData.Cargo.Rows[CargoView.FocusedRowHandle]["CargoPK"].ToString() == "")
            {
                CargoPK = 0;
            }
            else
            {
                CargoPK = (int)sEData.Cargo.Rows[CargoView.FocusedRowHandle]["CargoPK"];
            }            
            sEData.Cargo.Rows.RemoveAt(CargoView.FocusedRowHandle);
            PortRRemoveByCargo(CargoPK);
            CargoGridControl.Height -= 31;
            CargoPanel.Height -= 31;
            if (sEData.Cargo.Count < 2)
            {
                CargoAppend_Click(sender,e);
            }
            SetCargoIndex();
        }
        private void PortRClear_Click(object sender, EventArgs e)
        {
            sEData.PortRInit();
            for (int i = 0; i < sEData.Cargo.Count; i++)
            {
                sEData.Cargo.Rows[i]["LoadingPort"] = "";
                sEData.Cargo.Rows[i]["DischargingPort"] = "";
            }
            PortRotationGridControl.Height = 260;
            PortRPanel.Height = 332;
        }

        private void PortRAppend_Click(object sender, EventArgs e)
        {
            if (sEData.PortR.Rows.Count > 19)
                return;
            DataRow dataRow = sEData.PortR.NewRow();
            sEData.PortR.Rows.InsertAt(dataRow, sEData.PortR.Rows.Count - 2);
            SetPortIndex();
            PortRotationGridControl.Height += 31;
            PortRPanel.Height += 31;
        }

        private void PortRInsert_Click(object sender, EventArgs e)
        {
            if (sEData.PortR.Rows.Count > 19)
                return;
            DataRow dataRow = sEData.PortR.NewRow();
            if (PortRotationView.FocusedRowHandle >0 )
            {
                sEData.PortR.Rows.InsertAt(dataRow, PortRotationView.FocusedRowHandle);
            }
            else
            {
                sEData.PortR.Rows.InsertAt(dataRow,1);
            }
           
            SetPortIndex();
            PortRotationGridControl.Height += 31;
            PortRPanel.Height += 31;
        }

        private void PortRRemove_Click(object sender, EventArgs e)
        {
            int rowindex = PortRotationView.FocusedRowHandle;
            viewModel.PortRemoveEvent(rowindex);
            if (rowindex == PortRotationView.RowCount -1 || rowindex == 0)
            {
                for (int i = 3; i < 16; i++)
                {
                    sEData.PortR.Rows[rowindex][i] = DBNull.Value;
                }                
            }
            else
            {
                sEData.PortR.Rows.RemoveAt(rowindex);
                PortRotationGridControl.Height -= 31;
                PortRPanel.Height -= 31;
            }            
            // Cargo Port 삭제해주기
            if (sEData.PortR.Rows[PortRotationView.FocusedRowHandle]["CargoPK"].ToString() != "")
            {
                string port = sEData.PortR.Rows[PortRotationView.FocusedRowHandle]["Name"].ToString();
                int pk = (int)sEData.PortR.Rows[PortRotationView.FocusedRowHandle]["CargoPK"];
                CargoRemoveByPortR(port, pk);
            }
            // distance 삭제해주기
            
            if (sEData.PortR.Rows.Count < 7)
            {
                PortRAppend_Click(sender, e);
            }
        }
        private Double getWork()
        {
            Double Work = 0;
            for (int i = 0; i < PortRotationView.RowCount - 1; i++)
            {
                string WorkValue = sEData.PortR.Rows[i]["Work"].ToString();
                if (WorkValue != "")
                {
                    Work += Convert.ToDouble(WorkValue);
                }
            }
            return Work;
        }
        private Double getIdle()
        {
            Double Idle = 0;
            for (int i = 0; i < PortRotationView.RowCount - 1; i++)
            {
                string IdleValue = sEData.PortR.Rows[i]["Idle"].ToString();
                if (IdleValue != "")
                {
                    Idle += Convert.ToDouble(IdleValue);
                }
            }
            return Idle;
        }
        private void CargoValidate(int rowindex)
    {
        SEData.CargoDataTable Cargo = sEData.Cargo;

        //Discharging port validation
        if ((Cargo.Rows[rowindex]["DischargingPort"].ToString() != "") && (Cargo.Rows[rowindex]["LoadingPort"].ToString() == ""))
        {
            bool LoadingPortExist = false;
            int prevLoadingport = 0;
            for (int i = rowindex; i > 0; i--)
            {
                if (Cargo.Rows[i]["LoadingPort"].ToString() != "")
                {
                    prevLoadingport = i;
                    LoadingPortExist = true;
                    break;
                }
            }
            if (LoadingPortExist)
            {
                Cargo.Rows[rowindex]["LoadingPort"] = Cargo.Rows[prevLoadingport]["LoadingPort"];
            }
            else
            {
                MessageBox.Show("LoadingPort를 먼저 입력해주세요");
                Cargo.Rows[rowindex]["DischargingPort"] = "";
            }
        }

        // Percent value validation
        for (int i = 0; i < Cargo.Rows.Count; i++)
        {
            if ( Cargo.Rows[i]["Acomm"].ToString() != "" && Convert.ToDouble(Cargo.Rows[i]["Acomm"]) > 99)
            {
                Cargo.Rows[i]["Acomm"] = 99;
            }
            if ( Cargo.Rows[i]["Brkg"].ToString() != "" && Convert.ToDouble(Cargo.Rows[i]["Brkg"]) > 99)
            {
                Cargo.Rows[i]["Brkg"] = 99;
            }
            if (Cargo.Rows[i]["FrtTax"].ToString() != "" && Convert.ToDouble(Cargo.Rows[i]["FrtTax"]) > 99 )
            {
                Cargo.Rows[i]["FrtTax"] = 99;
            }
        }
    }
        private void PortRValidate(int rowindex)
        {
            SEData.PortRDataTable portr = sEData.PortR;
            int lastindex = portr.Rows.Count - 1;
            // value vaildation
            // 99 이상 입력 안되는 value
            for (int i = 0; i < portr.Rows.Count; i++)
            {
                if (portr.Rows[i]["WF"].ToString() != "" && Convert.ToDouble(portr.Rows[i]["WF"]) > 99)
                {
                    portr.Rows[i]["WF"] = 99;
                }
                if (portr.Rows[i]["Spd"].ToString() != "" && Convert.ToDouble(portr.Rows[i]["Spd"]) > 99)
                {
                    portr.Rows[i]["Spd"] = 99;
                }
                if (portr.Rows[i]["Idle"].ToString() != "" && Convert.ToDouble(portr.Rows[i]["Idle"]) > 99)
                {
                    portr.Rows[i]["Idle"] = 99;
                }
                if (portr.Rows[i]["Work"].ToString() != "" && Convert.ToDouble(portr.Rows[i]["Work"]) > 99)
                {
                    portr.Rows[i]["Work"] = 99;
                }
            }
            if (portr.Rows[lastindex]["Sea"].ToString() != "" && Convert.ToDouble(portr.Rows[lastindex]["Sea"]) > 99)
            {
                portr.Rows[lastindex]["Sea"] = 99;
            }
            // First port Distance delete
            for (int i = 0; i < portr.Rows.Count; i++)
            {
                object dbnull = System.DBNull.Value;
                if (portr.Rows[i]["Name"].ToString()!="")
                {
                    portr.Rows[i]["Distance"] = dbnull;
                    portr.Rows[i]["Seca"] = dbnull;
                    portr.Rows[i]["Wf"] = dbnull;
                    portr.Rows[i]["Spd"] = dbnull;
                    portr.Rows[i]["Sea"] = dbnull;
                    break;
                }
            }
            //Seca , Distance Validate
            for (int i = 0; i < portr.Rows.Count; i++)
            {
                object distancetemp = portr.Rows[i]["Distance"];
                object secatemp = portr.Rows[i]["Seca"];

                if (distancetemp.ToString() == "" && secatemp.ToString() != "")
                {
                    MessageBox.Show("Seca는 Distance를 넘을 수 없습니다.");
                    secatemp = "";
                }
                else
                {
                    if(secatemp.ToString() != "" && (double)distancetemp < (double)secatemp)
                    {
                        MessageBox.Show("Seca는 Distance를 넘을 수 없습니다.");
                        portr.Rows[i]["Distance"] = secatemp;
                    }
                }
            }
            SendKeys.Send("{ENTER}");
        }
        private void ResultValidate()
        {
            if(sEData.ResultCB.Rows[1]["Value"].ToString() != "" && Convert.ToDouble(sEData.ResultCB.Rows[1]["Value"]) > 99)
            {
                sEData.ResultCB.Rows[1]["Value"] = 99;
            }
        }
        private void SetCargoIndex()
        {
            for (int i = 0; i < sEData.Cargo.Count; i++)
            {
                sEData.Cargo.Rows[i]["Index"] = i+1;
            }
        }

        private void SetPortIndex()
        {
            for (int i = 0; i < sEData.PortR.Count; i++)
            {
                sEData.PortR.Rows[i]["Index"] = i + 1;
            }
        }
        private void CargoRemoveByPortR(string Port,int PK)
        {
            for (int i = 0; i < sEData.PortR.Count; i++)
            {
                if (sEData.Cargo.Rows[i]["LoadingPort"].ToString() == Port && (int)sEData.Cargo.Rows[i]["CargoPK"] == PK) 
                {
                    sEData.Cargo.Rows[i]["LoadingPort"] = "";
                }
                if (sEData.Cargo.Rows[i]["DischargingPort"].ToString() == Port && (int)sEData.Cargo.Rows[i]["CargoPK"] == PK)
                {
                    sEData.Cargo.Rows[i]["DischargingPort"] = "";
                }
            }
            SetCargoIndex();
        }
        private void PortRRemoveByCargo(int PK)
        {
            for (int i = 0; i < sEData.PortR.Count; i++)
            {
                if (sEData.PortR.Rows[i]["CargoPK"].ToString() !="" && (int)sEData.PortR.Rows[i]["CargoPK"] == PK)
                {
                    sEData.PortR.Rows.RemoveAt(i);
                }
            }
            SetPortIndex();
        }

    }
    
    public static class Prompt
    {
        public static string ShowDialog(string text, string caption)
        {
            Form prompt = new Form()
            {
                Width = 300,
                Height = 150,
                FormBorderStyle = FormBorderStyle.FixedDialog,
                Text = caption,
                StartPosition = FormStartPosition.CenterScreen
            };
            Label textLabel = new Label() { Left = 20, Top = 20, Text = text, Width = 200 };
            TextBox textBox = new TextBox() { Left = 20, Top = 50, Width = 200 };
            Button confirmation = new Button() { Text = "Ok", Left = 230, Width = 30, Top = 50, DialogResult = DialogResult.OK };
            confirmation.Click += (sender, e) => { prompt.Close(); };
            prompt.Controls.Add(textBox);
            prompt.Controls.Add(confirmation);
            prompt.Controls.Add(textLabel);
            prompt.AcceptButton = confirmation;

            return prompt.ShowDialog() == DialogResult.OK ? textBox.Text : "";
        }
    }

}
