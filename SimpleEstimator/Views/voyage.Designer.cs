﻿namespace SimpleEstimator.Views
{
    partial class voyage
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions1 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions2 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(voyage));
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject5 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject6 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject7 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject8 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions3 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject9 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject10 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject11 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject12 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions4 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject13 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject14 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject15 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject16 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions5 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject17 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject18 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject19 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject20 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions6 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject21 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject22 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject23 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject24 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions7 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject25 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject26 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject27 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject28 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions8 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject29 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject30 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject31 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject32 = new DevExpress.Utils.SerializableAppearanceObject();
            this.voyagepanel = new System.Windows.Forms.Panel();
            this.ResultPanel = new System.Windows.Forms.Panel();
            this.ResutlGridControl = new DevExpress.XtraGrid.GridControl();
            this.resultBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sEDataBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.ResultView = new DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView();
            this.gridBand53 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.ResultType = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand54 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.ResultValue = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.ResultCBGridControl = new DevExpress.XtraGrid.GridControl();
            this.resultCBBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.ResultCBView = new DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView();
            this.gridBand51 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.ReultCBType = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand52 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.ReultCBValue = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.ResultBEGridControl = new DevExpress.XtraGrid.GridControl();
            this.resultBEBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.ResultBEView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.ResultBEType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ResultBEFO = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ResultBEFOValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ResultBEDO = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ResultBEDOValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ResultOEGridControl = new DevExpress.XtraGrid.GridControl();
            this.resultOEBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.ResultOEView = new DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView();
            this.gridBand43 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.ResultOEType1 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand44 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.ResultOEValue1 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand45 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.ResultOEType2 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand46 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.ResultOEValue2 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.BELabel = new System.Windows.Forms.Label();
            this.ResultLabel = new System.Windows.Forms.Label();
            this.OPLabel = new System.Windows.Forms.Label();
            this.PortRPanel = new System.Windows.Forms.Panel();
            this.PortRRemove = new DevExpress.XtraEditors.SimpleButton();
            this.PortRInsert = new DevExpress.XtraEditors.SimpleButton();
            this.PortRAppend = new DevExpress.XtraEditors.SimpleButton();
            this.PortRClear = new DevExpress.XtraEditors.SimpleButton();
            this.DurationPanel = new System.Windows.Forms.Panel();
            this.DurationLabel = new System.Windows.Forms.Label();
            this.PortRotationGridControl = new DevExpress.XtraGrid.GridControl();
            this.portRBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.PortRotationView = new DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView();
            this.gridBand30 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.PortRIndex = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand31 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.PortRType = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.PortTypeComboBox = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.gridBand32 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.PortRName = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.PortRCrane = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand33 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.PortRDistance = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.PortRSeca = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand34 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.PortRWF = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand35 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.PortRSpd = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand36 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.PortRSea = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand37 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.PortRLDRate = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand38 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.PortRIdle = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.PortRWork = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.PortRNameDetail = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand39 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.PortRDem = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand40 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.PortRDes = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand41 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.PortRPortCharge = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand42 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.PortRArrival = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.NonBtnDateEdit = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.gridBand60 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.PortRDepature = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.DateEdit = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.PortRLabel = new System.Windows.Forms.Label();
            this.CargoPanel = new System.Windows.Forms.Panel();
            this.CargoRemove = new DevExpress.XtraEditors.SimpleButton();
            this.CargoInsert = new DevExpress.XtraEditors.SimpleButton();
            this.CargoAppend = new DevExpress.XtraEditors.SimpleButton();
            this.CargoClear = new DevExpress.XtraEditors.SimpleButton();
            this.CargoGridControl = new DevExpress.XtraGrid.GridControl();
            this.cargoBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.CargoView = new DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView();
            this.gridBand17 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.CargoIndex = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand18 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.CargoAccount = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand19 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.CargoHistory = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.CargoHistoryBtn = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.CargoName = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand20 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.LoadingPort = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand21 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.DischargingPort = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand22 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.Quantity = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.QuantityTerm = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.QuantityTermComboBox = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.gridBand23 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.Frt = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand24 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.Term = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.TermComboBox = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.gridBand25 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.Revenue = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand26 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.AComm = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand27 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.Brkg = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand28 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.FrtTax = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand29 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.LinerTerm = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.CargoLabel = new System.Windows.Forms.Label();
            this.VesselPanel = new System.Windows.Forms.Panel();
            this.FusselPanel = new System.Windows.Forms.Panel();
            this.VesselDOControl = new DevExpress.XtraGrid.GridControl();
            this.vPDOBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.VesselDOView = new DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView();
            this.gridBand8 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.LSDO = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand13 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.DOType = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.DoTypeComboBox = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.gridBand15 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.DOSea = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand16 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.DOIdle = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand59 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.DOWork = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.VesselFOControl = new DevExpress.XtraGrid.GridControl();
            this.vPFOBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.VesselFOView = new DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView();
            this.gridBand9 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.LSFO = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand10 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.FOType = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.repositoryItemComboBox1 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.gridBand11 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.FOBallast = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand12 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.FOLaden = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand1 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.FOIdle = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand7 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.FOWork = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.SpdPanel = new System.Windows.Forms.Panel();
            this.VesselSpdControl = new DevExpress.XtraGrid.GridControl();
            this.vPSpdBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.VesselSpdView = new DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView();
            this.gridBand5 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.BallastSpd = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand6 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.LadenSpd = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.MVPanel = new System.Windows.Forms.Panel();
            this.VesselMVControl = new DevExpress.XtraGrid.GridControl();
            this.vPVesselBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.VesselMVView = new DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView();
            this.gridBand112 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.VesselMV = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand2 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.VesselType = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand3 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.VesselDWT = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand4 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.VesselDraft = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.VesselLabel = new System.Windows.Forms.Label();
            this.persistentRepository1 = new DevExpress.XtraEditors.Repository.PersistentRepository(this.components);
            this.NameDetailBtn = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.ZoomBtn = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.CraneBtn = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.PortREditBtn = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.CargoEditBtn = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.voyagepanel.SuspendLayout();
            this.ResultPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ResutlGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.resultBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sEDataBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ResultView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ResultCBGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.resultCBBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ResultCBView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ResultBEGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.resultBEBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ResultBEView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ResultOEGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.resultOEBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ResultOEView)).BeginInit();
            this.PortRPanel.SuspendLayout();
            this.DurationPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PortRotationGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.portRBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PortRotationView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PortTypeComboBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NonBtnDateEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NonBtnDateEdit.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateEdit.CalendarTimeProperties)).BeginInit();
            this.CargoPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CargoGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cargoBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CargoView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CargoHistoryBtn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.QuantityTermComboBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TermComboBox)).BeginInit();
            this.VesselPanel.SuspendLayout();
            this.FusselPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.VesselDOControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vPDOBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.VesselDOView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DoTypeComboBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.VesselFOControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vPFOBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.VesselFOView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox1)).BeginInit();
            this.SpdPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.VesselSpdControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vPSpdBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.VesselSpdView)).BeginInit();
            this.MVPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.VesselMVControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vPVesselBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.VesselMVView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NameDetailBtn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ZoomBtn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CraneBtn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PortREditBtn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CargoEditBtn)).BeginInit();
            this.SuspendLayout();
            // 
            // voyagepanel
            // 
            this.voyagepanel.Controls.Add(this.ResultPanel);
            this.voyagepanel.Controls.Add(this.PortRPanel);
            this.voyagepanel.Controls.Add(this.CargoPanel);
            this.voyagepanel.Controls.Add(this.VesselPanel);
            this.voyagepanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.voyagepanel.Location = new System.Drawing.Point(0, 0);
            this.voyagepanel.Name = "voyagepanel";
            this.voyagepanel.Size = new System.Drawing.Size(1678, 1002);
            this.voyagepanel.TabIndex = 0;
            // 
            // ResultPanel
            // 
            this.ResultPanel.Controls.Add(this.ResutlGridControl);
            this.ResultPanel.Controls.Add(this.ResultCBGridControl);
            this.ResultPanel.Controls.Add(this.ResultBEGridControl);
            this.ResultPanel.Controls.Add(this.ResultOEGridControl);
            this.ResultPanel.Controls.Add(this.BELabel);
            this.ResultPanel.Controls.Add(this.ResultLabel);
            this.ResultPanel.Controls.Add(this.OPLabel);
            this.ResultPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.ResultPanel.Location = new System.Drawing.Point(0, 752);
            this.ResultPanel.Name = "ResultPanel";
            this.ResultPanel.Size = new System.Drawing.Size(1678, 247);
            this.ResultPanel.TabIndex = 2;
            // 
            // ResutlGridControl
            // 
            this.ResutlGridControl.DataSource = this.resultBindingSource;
            this.ResutlGridControl.Location = new System.Drawing.Point(1354, 39);
            this.ResutlGridControl.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat;
            this.ResutlGridControl.LookAndFeel.UseDefaultLookAndFeel = false;
            this.ResutlGridControl.MainView = this.ResultView;
            this.ResutlGridControl.Name = "ResutlGridControl";
            this.ResutlGridControl.Size = new System.Drawing.Size(284, 190);
            this.ResutlGridControl.TabIndex = 10;
            this.ResutlGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.ResultView});
            // 
            // resultBindingSource
            // 
            this.resultBindingSource.DataMember = "Result";
            this.resultBindingSource.DataSource = this.sEDataBindingSource;
            // 
            // sEDataBindingSource
            // 
            this.sEDataBindingSource.DataSource = typeof(Model.DataSet.SEData);
            this.sEDataBindingSource.Position = 0;
            // 
            // ResultView
            // 
            this.ResultView.BandPanelRowHeight = 30;
            this.ResultView.Bands.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand53,
            this.gridBand54});
            this.ResultView.Columns.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn[] {
            this.ResultType,
            this.ResultValue});
            this.ResultView.GridControl = this.ResutlGridControl;
            this.ResultView.HorzScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Never;
            this.ResultView.Name = "ResultView";
            this.ResultView.OptionsBehavior.AlignGroupSummaryInGroupRow = DevExpress.Utils.DefaultBoolean.False;
            this.ResultView.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.False;
            this.ResultView.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.False;
            this.ResultView.OptionsBehavior.AllowFixedGroups = DevExpress.Utils.DefaultBoolean.False;
            this.ResultView.OptionsBehavior.AllowPartialGroups = DevExpress.Utils.DefaultBoolean.False;
            this.ResultView.OptionsBehavior.AllowPartialRedrawOnScrolling = false;
            this.ResultView.OptionsBehavior.AllowPixelScrolling = DevExpress.Utils.DefaultBoolean.False;
            this.ResultView.OptionsCustomization.AllowBandMoving = false;
            this.ResultView.OptionsCustomization.AllowBandResizing = false;
            this.ResultView.OptionsCustomization.AllowColumnMoving = false;
            this.ResultView.OptionsCustomization.AllowColumnResizing = false;
            this.ResultView.OptionsCustomization.AllowGroup = false;
            this.ResultView.OptionsCustomization.AllowQuickHideColumns = false;
            this.ResultView.OptionsCustomization.AllowSort = false;
            this.ResultView.OptionsCustomization.ShowBandsInCustomizationForm = false;
            this.ResultView.OptionsSelection.InvertSelection = true;
            this.ResultView.OptionsSelection.UseIndicatorForSelection = false;
            this.ResultView.OptionsView.AllowHtmlDrawGroups = false;
            this.ResultView.OptionsView.ShowBands = false;
            this.ResultView.OptionsView.ShowColumnHeaders = false;
            this.ResultView.OptionsView.ShowGroupExpandCollapseButtons = false;
            this.ResultView.OptionsView.ShowGroupPanel = false;
            this.ResultView.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.True;
            this.ResultView.OptionsView.ShowIndicator = false;
            this.ResultView.RowHeight = 30;
            this.ResultView.VertScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Never;
            // 
            // gridBand53
            // 
            this.gridBand53.AppearanceHeader.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.gridBand53.AppearanceHeader.BackColor2 = System.Drawing.Color.Silver;
            this.gridBand53.AppearanceHeader.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.gridBand53.AppearanceHeader.Options.UseBackColor = true;
            this.gridBand53.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand53.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand53.Caption = "gridBand1";
            this.gridBand53.Columns.Add(this.ResultType);
            this.gridBand53.Name = "gridBand53";
            this.gridBand53.OptionsBand.AllowHotTrack = false;
            this.gridBand53.OptionsBand.AllowMove = false;
            this.gridBand53.OptionsBand.AllowPress = false;
            this.gridBand53.OptionsBand.AllowSize = false;
            this.gridBand53.OptionsBand.ShowInCustomizationForm = false;
            this.gridBand53.VisibleIndex = 0;
            this.gridBand53.Width = 110;
            // 
            // ResultType
            // 
            this.ResultType.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(170)))), ((int)(((byte)(170)))));
            this.ResultType.AppearanceCell.Options.UseBackColor = true;
            this.ResultType.FieldName = "Type";
            this.ResultType.Name = "ResultType";
            this.ResultType.OptionsColumn.AllowEdit = false;
            this.ResultType.OptionsColumn.AllowFocus = false;
            this.ResultType.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
            this.ResultType.OptionsColumn.AllowIncrementalSearch = false;
            this.ResultType.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.ResultType.OptionsColumn.AllowMove = false;
            this.ResultType.OptionsColumn.AllowShowHide = false;
            this.ResultType.OptionsColumn.AllowSize = false;
            this.ResultType.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.ResultType.OptionsColumn.ImmediateUpdateRowPosition = DevExpress.Utils.DefaultBoolean.False;
            this.ResultType.OptionsColumn.Printable = DevExpress.Utils.DefaultBoolean.False;
            this.ResultType.OptionsColumn.ReadOnly = true;
            this.ResultType.OptionsColumn.ShowCaption = false;
            this.ResultType.OptionsColumn.ShowInCustomizationForm = false;
            this.ResultType.OptionsColumn.ShowInExpressionEditor = false;
            this.ResultType.OptionsColumn.TabStop = false;
            this.ResultType.Visible = true;
            this.ResultType.Width = 110;
            // 
            // gridBand54
            // 
            this.gridBand54.AppearanceHeader.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.gridBand54.AppearanceHeader.BackColor2 = System.Drawing.Color.Silver;
            this.gridBand54.AppearanceHeader.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.gridBand54.AppearanceHeader.Options.UseBackColor = true;
            this.gridBand54.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand54.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand54.Caption = "gridBand2";
            this.gridBand54.Columns.Add(this.ResultValue);
            this.gridBand54.Name = "gridBand54";
            this.gridBand54.OptionsBand.AllowHotTrack = false;
            this.gridBand54.OptionsBand.AllowMove = false;
            this.gridBand54.OptionsBand.AllowPress = false;
            this.gridBand54.OptionsBand.AllowSize = false;
            this.gridBand54.OptionsBand.ShowInCustomizationForm = false;
            this.gridBand54.VisibleIndex = 1;
            this.gridBand54.Width = 170;
            // 
            // ResultValue
            // 
            this.ResultValue.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.ResultValue.AppearanceCell.Options.UseBackColor = true;
            this.ResultValue.AppearanceCell.Options.UseTextOptions = true;
            this.ResultValue.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.ResultValue.DisplayFormat.FormatString = "{0:#,##0.000}";
            this.ResultValue.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.ResultValue.FieldName = "Value";
            this.ResultValue.Name = "ResultValue";
            this.ResultValue.OptionsColumn.AllowEdit = false;
            this.ResultValue.OptionsColumn.AllowFocus = false;
            this.ResultValue.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
            this.ResultValue.OptionsColumn.AllowIncrementalSearch = false;
            this.ResultValue.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.ResultValue.OptionsColumn.AllowMove = false;
            this.ResultValue.OptionsColumn.AllowShowHide = false;
            this.ResultValue.OptionsColumn.AllowSize = false;
            this.ResultValue.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.ResultValue.OptionsColumn.ImmediateUpdateRowPosition = DevExpress.Utils.DefaultBoolean.False;
            this.ResultValue.OptionsColumn.Printable = DevExpress.Utils.DefaultBoolean.False;
            this.ResultValue.OptionsColumn.ReadOnly = true;
            this.ResultValue.OptionsColumn.ShowCaption = false;
            this.ResultValue.OptionsColumn.ShowInCustomizationForm = false;
            this.ResultValue.OptionsColumn.ShowInExpressionEditor = false;
            this.ResultValue.OptionsColumn.TabStop = false;
            this.ResultValue.Visible = true;
            this.ResultValue.Width = 170;
            // 
            // ResultCBGridControl
            // 
            this.ResultCBGridControl.DataSource = this.resultCBBindingSource;
            this.ResultCBGridControl.Location = new System.Drawing.Point(1073, 39);
            this.ResultCBGridControl.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat;
            this.ResultCBGridControl.LookAndFeel.UseDefaultLookAndFeel = false;
            this.ResultCBGridControl.MainView = this.ResultCBView;
            this.ResultCBGridControl.Name = "ResultCBGridControl";
            this.ResultCBGridControl.Size = new System.Drawing.Size(284, 128);
            this.ResultCBGridControl.TabIndex = 9;
            this.ResultCBGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.ResultCBView});
            this.ResultCBGridControl.EditorKeyDown += new System.Windows.Forms.KeyEventHandler(this.ResultCBGridControl_EditorKeyDown);
            // 
            // resultCBBindingSource
            // 
            this.resultCBBindingSource.DataMember = "ResultCB";
            this.resultCBBindingSource.DataSource = this.sEDataBindingSource;
            // 
            // ResultCBView
            // 
            this.ResultCBView.Appearance.FocusedCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(120)))), ((int)(((byte)(177)))), ((int)(((byte)(145)))));
            this.ResultCBView.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.ResultCBView.Appearance.FocusedCell.Options.UseBackColor = true;
            this.ResultCBView.Appearance.FocusedCell.Options.UseForeColor = true;
            this.ResultCBView.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(120)))), ((int)(((byte)(177)))), ((int)(((byte)(145)))));
            this.ResultCBView.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.ResultCBView.Appearance.FocusedRow.Options.UseBackColor = true;
            this.ResultCBView.Appearance.FocusedRow.Options.UseForeColor = true;
            this.ResultCBView.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.Transparent;
            this.ResultCBView.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.ResultCBView.BandPanelRowHeight = 30;
            this.ResultCBView.Bands.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand51,
            this.gridBand52});
            this.ResultCBView.Columns.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn[] {
            this.ReultCBType,
            this.ReultCBValue});
            this.ResultCBView.GridControl = this.ResultCBGridControl;
            this.ResultCBView.HorzScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Never;
            this.ResultCBView.Name = "ResultCBView";
            this.ResultCBView.OptionsBehavior.AlignGroupSummaryInGroupRow = DevExpress.Utils.DefaultBoolean.False;
            this.ResultCBView.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.False;
            this.ResultCBView.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.False;
            this.ResultCBView.OptionsBehavior.AllowFixedGroups = DevExpress.Utils.DefaultBoolean.False;
            this.ResultCBView.OptionsBehavior.AllowPartialGroups = DevExpress.Utils.DefaultBoolean.False;
            this.ResultCBView.OptionsBehavior.AllowPartialRedrawOnScrolling = false;
            this.ResultCBView.OptionsBehavior.AllowPixelScrolling = DevExpress.Utils.DefaultBoolean.False;
            this.ResultCBView.OptionsCustomization.AllowBandMoving = false;
            this.ResultCBView.OptionsCustomization.AllowBandResizing = false;
            this.ResultCBView.OptionsCustomization.AllowColumnMoving = false;
            this.ResultCBView.OptionsCustomization.AllowColumnResizing = false;
            this.ResultCBView.OptionsCustomization.AllowGroup = false;
            this.ResultCBView.OptionsCustomization.AllowQuickHideColumns = false;
            this.ResultCBView.OptionsCustomization.AllowSort = false;
            this.ResultCBView.OptionsCustomization.ShowBandsInCustomizationForm = false;
            this.ResultCBView.OptionsSelection.InvertSelection = true;
            this.ResultCBView.OptionsSelection.UseIndicatorForSelection = false;
            this.ResultCBView.OptionsView.AllowHtmlDrawGroups = false;
            this.ResultCBView.OptionsView.ShowBands = false;
            this.ResultCBView.OptionsView.ShowColumnHeaders = false;
            this.ResultCBView.OptionsView.ShowGroupExpandCollapseButtons = false;
            this.ResultCBView.OptionsView.ShowGroupPanel = false;
            this.ResultCBView.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.True;
            this.ResultCBView.OptionsView.ShowIndicator = false;
            this.ResultCBView.RowHeight = 30;
            this.ResultCBView.VertScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Never;
            this.ResultCBView.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.ResultCBView_CustomDrawCell);
            this.ResultCBView.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.ResultCBView_FocusedRowChanged);
            this.ResultCBView.CellValueChanged += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.ResultCBView_CellValueChanged);
            this.ResultCBView.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ResultCBView_KeyDown);
            // 
            // gridBand51
            // 
            this.gridBand51.AppearanceHeader.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.gridBand51.AppearanceHeader.BackColor2 = System.Drawing.Color.Silver;
            this.gridBand51.AppearanceHeader.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.gridBand51.AppearanceHeader.Options.UseBackColor = true;
            this.gridBand51.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand51.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand51.Caption = "gridBand1";
            this.gridBand51.Columns.Add(this.ReultCBType);
            this.gridBand51.Name = "gridBand51";
            this.gridBand51.OptionsBand.AllowHotTrack = false;
            this.gridBand51.OptionsBand.AllowMove = false;
            this.gridBand51.OptionsBand.AllowPress = false;
            this.gridBand51.OptionsBand.AllowSize = false;
            this.gridBand51.OptionsBand.ShowInCustomizationForm = false;
            this.gridBand51.VisibleIndex = 0;
            this.gridBand51.Width = 110;
            // 
            // ReultCBType
            // 
            this.ReultCBType.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(170)))), ((int)(((byte)(170)))));
            this.ReultCBType.AppearanceCell.Options.UseBackColor = true;
            this.ReultCBType.FieldName = "Type";
            this.ReultCBType.Name = "ReultCBType";
            this.ReultCBType.OptionsColumn.AllowEdit = false;
            this.ReultCBType.OptionsColumn.AllowFocus = false;
            this.ReultCBType.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
            this.ReultCBType.OptionsColumn.AllowIncrementalSearch = false;
            this.ReultCBType.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.ReultCBType.OptionsColumn.AllowMove = false;
            this.ReultCBType.OptionsColumn.AllowShowHide = false;
            this.ReultCBType.OptionsColumn.AllowSize = false;
            this.ReultCBType.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.ReultCBType.OptionsColumn.ImmediateUpdateRowPosition = DevExpress.Utils.DefaultBoolean.False;
            this.ReultCBType.OptionsColumn.Printable = DevExpress.Utils.DefaultBoolean.False;
            this.ReultCBType.OptionsColumn.ReadOnly = true;
            this.ReultCBType.OptionsColumn.ShowCaption = false;
            this.ReultCBType.OptionsColumn.ShowInCustomizationForm = false;
            this.ReultCBType.OptionsColumn.ShowInExpressionEditor = false;
            this.ReultCBType.OptionsColumn.TabStop = false;
            this.ReultCBType.Visible = true;
            this.ReultCBType.Width = 110;
            // 
            // gridBand52
            // 
            this.gridBand52.AppearanceHeader.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.gridBand52.AppearanceHeader.BackColor2 = System.Drawing.Color.Silver;
            this.gridBand52.AppearanceHeader.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.gridBand52.AppearanceHeader.Options.UseBackColor = true;
            this.gridBand52.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand52.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand52.Caption = "gridBand2";
            this.gridBand52.Columns.Add(this.ReultCBValue);
            this.gridBand52.Name = "gridBand52";
            this.gridBand52.OptionsBand.AllowHotTrack = false;
            this.gridBand52.OptionsBand.AllowMove = false;
            this.gridBand52.OptionsBand.AllowPress = false;
            this.gridBand52.OptionsBand.AllowSize = false;
            this.gridBand52.OptionsBand.ShowInCustomizationForm = false;
            this.gridBand52.VisibleIndex = 1;
            this.gridBand52.Width = 170;
            // 
            // ReultCBValue
            // 
            this.ReultCBValue.AppearanceCell.Options.UseTextOptions = true;
            this.ReultCBValue.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.ReultCBValue.DisplayFormat.FormatString = "{0:#,##0.000}";
            this.ReultCBValue.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.ReultCBValue.FieldName = "Value";
            this.ReultCBValue.Name = "ReultCBValue";
            this.ReultCBValue.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
            this.ReultCBValue.OptionsColumn.AllowIncrementalSearch = false;
            this.ReultCBValue.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.ReultCBValue.OptionsColumn.AllowMove = false;
            this.ReultCBValue.OptionsColumn.AllowShowHide = false;
            this.ReultCBValue.OptionsColumn.AllowSize = false;
            this.ReultCBValue.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.ReultCBValue.OptionsColumn.ImmediateUpdateRowPosition = DevExpress.Utils.DefaultBoolean.False;
            this.ReultCBValue.OptionsColumn.Printable = DevExpress.Utils.DefaultBoolean.False;
            this.ReultCBValue.OptionsColumn.ShowCaption = false;
            this.ReultCBValue.OptionsColumn.ShowInCustomizationForm = false;
            this.ReultCBValue.OptionsColumn.ShowInExpressionEditor = false;
            this.ReultCBValue.Visible = true;
            this.ReultCBValue.Width = 170;
            // 
            // ResultBEGridControl
            // 
            this.ResultBEGridControl.DataSource = this.resultBEBindingSource;
            this.ResultBEGridControl.Location = new System.Drawing.Point(557, 39);
            this.ResultBEGridControl.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat;
            this.ResultBEGridControl.LookAndFeel.UseDefaultLookAndFeel = false;
            this.ResultBEGridControl.MainView = this.ResultBEView;
            this.ResultBEGridControl.Name = "ResultBEGridControl";
            this.ResultBEGridControl.Size = new System.Drawing.Size(495, 190);
            this.ResultBEGridControl.TabIndex = 8;
            this.ResultBEGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.ResultBEView});
            this.ResultBEGridControl.EditorKeyDown += new System.Windows.Forms.KeyEventHandler(this.ResultBEGridControl_EditorKeyDown);
            // 
            // resultBEBindingSource
            // 
            this.resultBEBindingSource.DataMember = "ResultBE";
            this.resultBEBindingSource.DataSource = this.sEDataBindingSource;
            // 
            // ResultBEView
            // 
            this.ResultBEView.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(120)))), ((int)(((byte)(177)))), ((int)(((byte)(145)))));
            this.ResultBEView.Appearance.FocusedRow.Options.UseBackColor = true;
            this.ResultBEView.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.Transparent;
            this.ResultBEView.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.ResultBEView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.ResultBEType,
            this.ResultBEFO,
            this.ResultBEFOValue,
            this.ResultBEDO,
            this.ResultBEDOValue});
            this.ResultBEView.GridControl = this.ResultBEGridControl;
            this.ResultBEView.HorzScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Never;
            this.ResultBEView.Name = "ResultBEView";
            this.ResultBEView.OptionsBehavior.AlignGroupSummaryInGroupRow = DevExpress.Utils.DefaultBoolean.False;
            this.ResultBEView.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.False;
            this.ResultBEView.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.False;
            this.ResultBEView.OptionsBehavior.AllowFixedGroups = DevExpress.Utils.DefaultBoolean.False;
            this.ResultBEView.OptionsBehavior.AllowPartialGroups = DevExpress.Utils.DefaultBoolean.False;
            this.ResultBEView.OptionsBehavior.AllowPartialRedrawOnScrolling = false;
            this.ResultBEView.OptionsBehavior.AllowPixelScrolling = DevExpress.Utils.DefaultBoolean.False;
            this.ResultBEView.OptionsCustomization.AllowColumnMoving = false;
            this.ResultBEView.OptionsCustomization.AllowColumnResizing = false;
            this.ResultBEView.OptionsCustomization.AllowGroup = false;
            this.ResultBEView.OptionsCustomization.AllowQuickHideColumns = false;
            this.ResultBEView.OptionsCustomization.AllowSort = false;
            this.ResultBEView.OptionsSelection.InvertSelection = true;
            this.ResultBEView.OptionsSelection.UseIndicatorForSelection = false;
            this.ResultBEView.OptionsView.AllowCellMerge = true;
            this.ResultBEView.OptionsView.ColumnAutoWidth = false;
            this.ResultBEView.OptionsView.ShowColumnHeaders = false;
            this.ResultBEView.OptionsView.ShowGroupExpandCollapseButtons = false;
            this.ResultBEView.OptionsView.ShowGroupPanel = false;
            this.ResultBEView.OptionsView.ShowIndicator = false;
            this.ResultBEView.RowHeight = 30;
            this.ResultBEView.VertScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Never;
            this.ResultBEView.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.ResultBEView_CustomDrawCell);
            this.ResultBEView.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.ResultBEView_FocusedRowChanged);
            this.ResultBEView.CellValueChanged += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.ResultBEView_CellValueChanged);
            this.ResultBEView.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ResultBEView_KeyDown);
            // 
            // ResultBEType
            // 
            this.ResultBEType.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(170)))), ((int)(((byte)(170)))));
            this.ResultBEType.AppearanceCell.Options.UseBackColor = true;
            this.ResultBEType.FieldName = "Type";
            this.ResultBEType.Name = "ResultBEType";
            this.ResultBEType.OptionsColumn.AllowEdit = false;
            this.ResultBEType.OptionsColumn.AllowFocus = false;
            this.ResultBEType.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
            this.ResultBEType.OptionsColumn.AllowIncrementalSearch = false;
            this.ResultBEType.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.ResultBEType.OptionsColumn.AllowMove = false;
            this.ResultBEType.OptionsColumn.AllowShowHide = false;
            this.ResultBEType.OptionsColumn.AllowSize = false;
            this.ResultBEType.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.ResultBEType.OptionsColumn.ImmediateUpdateRowPosition = DevExpress.Utils.DefaultBoolean.False;
            this.ResultBEType.OptionsColumn.Printable = DevExpress.Utils.DefaultBoolean.False;
            this.ResultBEType.OptionsColumn.ReadOnly = true;
            this.ResultBEType.OptionsColumn.ShowCaption = false;
            this.ResultBEType.OptionsColumn.ShowInCustomizationForm = false;
            this.ResultBEType.OptionsColumn.ShowInExpressionEditor = false;
            this.ResultBEType.OptionsColumn.TabStop = false;
            this.ResultBEType.Visible = true;
            this.ResultBEType.VisibleIndex = 0;
            this.ResultBEType.Width = 110;
            // 
            // ResultBEFO
            // 
            this.ResultBEFO.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(170)))), ((int)(((byte)(170)))));
            this.ResultBEFO.AppearanceCell.Options.UseBackColor = true;
            this.ResultBEFO.AppearanceCell.Options.UseTextOptions = true;
            this.ResultBEFO.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ResultBEFO.FieldName = "TypeFO";
            this.ResultBEFO.Name = "ResultBEFO";
            this.ResultBEFO.OptionsColumn.AllowEdit = false;
            this.ResultBEFO.OptionsColumn.AllowFocus = false;
            this.ResultBEFO.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
            this.ResultBEFO.OptionsColumn.AllowIncrementalSearch = false;
            this.ResultBEFO.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.True;
            this.ResultBEFO.OptionsColumn.AllowMove = false;
            this.ResultBEFO.OptionsColumn.AllowShowHide = false;
            this.ResultBEFO.OptionsColumn.AllowSize = false;
            this.ResultBEFO.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.ResultBEFO.OptionsColumn.ImmediateUpdateRowPosition = DevExpress.Utils.DefaultBoolean.False;
            this.ResultBEFO.OptionsColumn.Printable = DevExpress.Utils.DefaultBoolean.False;
            this.ResultBEFO.OptionsColumn.ReadOnly = true;
            this.ResultBEFO.OptionsColumn.ShowCaption = false;
            this.ResultBEFO.OptionsColumn.ShowInCustomizationForm = false;
            this.ResultBEFO.OptionsColumn.ShowInExpressionEditor = false;
            this.ResultBEFO.OptionsColumn.TabStop = false;
            this.ResultBEFO.Visible = true;
            this.ResultBEFO.VisibleIndex = 1;
            this.ResultBEFO.Width = 50;
            // 
            // ResultBEFOValue
            // 
            this.ResultBEFOValue.AppearanceCell.Options.UseTextOptions = true;
            this.ResultBEFOValue.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.ResultBEFOValue.DisplayFormat.FormatString = "{0:#,##0.000}";
            this.ResultBEFOValue.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.ResultBEFOValue.FieldName = "ValueFO";
            this.ResultBEFOValue.Name = "ResultBEFOValue";
            this.ResultBEFOValue.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
            this.ResultBEFOValue.OptionsColumn.AllowIncrementalSearch = false;
            this.ResultBEFOValue.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.ResultBEFOValue.OptionsColumn.AllowMove = false;
            this.ResultBEFOValue.OptionsColumn.AllowShowHide = false;
            this.ResultBEFOValue.OptionsColumn.AllowSize = false;
            this.ResultBEFOValue.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.ResultBEFOValue.OptionsColumn.ImmediateUpdateRowPosition = DevExpress.Utils.DefaultBoolean.False;
            this.ResultBEFOValue.OptionsColumn.Printable = DevExpress.Utils.DefaultBoolean.False;
            this.ResultBEFOValue.OptionsColumn.ShowCaption = false;
            this.ResultBEFOValue.OptionsColumn.ShowInCustomizationForm = false;
            this.ResultBEFOValue.OptionsColumn.ShowInExpressionEditor = false;
            this.ResultBEFOValue.Visible = true;
            this.ResultBEFOValue.VisibleIndex = 2;
            this.ResultBEFOValue.Width = 140;
            // 
            // ResultBEDO
            // 
            this.ResultBEDO.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(170)))), ((int)(((byte)(170)))));
            this.ResultBEDO.AppearanceCell.Options.UseBackColor = true;
            this.ResultBEDO.AppearanceCell.Options.UseTextOptions = true;
            this.ResultBEDO.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ResultBEDO.FieldName = "TypeDO";
            this.ResultBEDO.Name = "ResultBEDO";
            this.ResultBEDO.OptionsColumn.AllowEdit = false;
            this.ResultBEDO.OptionsColumn.AllowFocus = false;
            this.ResultBEDO.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
            this.ResultBEDO.OptionsColumn.AllowIncrementalSearch = false;
            this.ResultBEDO.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.True;
            this.ResultBEDO.OptionsColumn.AllowMove = false;
            this.ResultBEDO.OptionsColumn.AllowShowHide = false;
            this.ResultBEDO.OptionsColumn.AllowSize = false;
            this.ResultBEDO.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.ResultBEDO.OptionsColumn.ImmediateUpdateRowPosition = DevExpress.Utils.DefaultBoolean.False;
            this.ResultBEDO.OptionsColumn.Printable = DevExpress.Utils.DefaultBoolean.False;
            this.ResultBEDO.OptionsColumn.ReadOnly = true;
            this.ResultBEDO.OptionsColumn.ShowCaption = false;
            this.ResultBEDO.OptionsColumn.ShowInCustomizationForm = false;
            this.ResultBEDO.OptionsColumn.ShowInExpressionEditor = false;
            this.ResultBEDO.OptionsColumn.TabStop = false;
            this.ResultBEDO.Visible = true;
            this.ResultBEDO.VisibleIndex = 3;
            this.ResultBEDO.Width = 50;
            // 
            // ResultBEDOValue
            // 
            this.ResultBEDOValue.AppearanceCell.Options.UseTextOptions = true;
            this.ResultBEDOValue.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.ResultBEDOValue.DisplayFormat.FormatString = "{0:#,##0.000}";
            this.ResultBEDOValue.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.ResultBEDOValue.FieldName = "ValueDO";
            this.ResultBEDOValue.Name = "ResultBEDOValue";
            this.ResultBEDOValue.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
            this.ResultBEDOValue.OptionsColumn.AllowIncrementalSearch = false;
            this.ResultBEDOValue.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.ResultBEDOValue.OptionsColumn.AllowMove = false;
            this.ResultBEDOValue.OptionsColumn.AllowShowHide = false;
            this.ResultBEDOValue.OptionsColumn.AllowSize = false;
            this.ResultBEDOValue.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.ResultBEDOValue.OptionsColumn.ImmediateUpdateRowPosition = DevExpress.Utils.DefaultBoolean.False;
            this.ResultBEDOValue.OptionsColumn.Printable = DevExpress.Utils.DefaultBoolean.False;
            this.ResultBEDOValue.OptionsColumn.ShowCaption = false;
            this.ResultBEDOValue.OptionsColumn.ShowInCustomizationForm = false;
            this.ResultBEDOValue.OptionsColumn.ShowInExpressionEditor = false;
            this.ResultBEDOValue.Visible = true;
            this.ResultBEDOValue.VisibleIndex = 4;
            this.ResultBEDOValue.Width = 140;
            // 
            // ResultOEGridControl
            // 
            this.ResultOEGridControl.DataSource = this.resultOEBindingSource;
            this.ResultOEGridControl.Location = new System.Drawing.Point(11, 39);
            this.ResultOEGridControl.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat;
            this.ResultOEGridControl.LookAndFeel.UseDefaultLookAndFeel = false;
            this.ResultOEGridControl.MainView = this.ResultOEView;
            this.ResultOEGridControl.Name = "ResultOEGridControl";
            this.ResultOEGridControl.Size = new System.Drawing.Size(524, 190);
            this.ResultOEGridControl.TabIndex = 4;
            this.ResultOEGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.ResultOEView});
            this.ResultOEGridControl.EditorKeyDown += new System.Windows.Forms.KeyEventHandler(this.ResultOEGridControl_EditorKeyDown);
            // 
            // resultOEBindingSource
            // 
            this.resultOEBindingSource.DataMember = "ResultOE";
            this.resultOEBindingSource.DataSource = this.sEDataBindingSource;
            // 
            // ResultOEView
            // 
            this.ResultOEView.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(120)))), ((int)(((byte)(177)))), ((int)(((byte)(145)))));
            this.ResultOEView.Appearance.FocusedRow.Options.UseBackColor = true;
            this.ResultOEView.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.Transparent;
            this.ResultOEView.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.ResultOEView.BandPanelRowHeight = 30;
            this.ResultOEView.Bands.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand43,
            this.gridBand44,
            this.gridBand45,
            this.gridBand46});
            this.ResultOEView.Columns.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn[] {
            this.ResultOEType1,
            this.ResultOEValue1,
            this.ResultOEType2,
            this.ResultOEValue2});
            this.ResultOEView.GridControl = this.ResultOEGridControl;
            this.ResultOEView.HorzScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Never;
            this.ResultOEView.Name = "ResultOEView";
            this.ResultOEView.OptionsBehavior.AlignGroupSummaryInGroupRow = DevExpress.Utils.DefaultBoolean.False;
            this.ResultOEView.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.False;
            this.ResultOEView.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.False;
            this.ResultOEView.OptionsBehavior.AllowFixedGroups = DevExpress.Utils.DefaultBoolean.False;
            this.ResultOEView.OptionsBehavior.AllowPartialGroups = DevExpress.Utils.DefaultBoolean.False;
            this.ResultOEView.OptionsBehavior.AllowPartialRedrawOnScrolling = false;
            this.ResultOEView.OptionsBehavior.AllowPixelScrolling = DevExpress.Utils.DefaultBoolean.False;
            this.ResultOEView.OptionsCustomization.AllowBandMoving = false;
            this.ResultOEView.OptionsCustomization.AllowBandResizing = false;
            this.ResultOEView.OptionsCustomization.AllowColumnMoving = false;
            this.ResultOEView.OptionsCustomization.AllowColumnResizing = false;
            this.ResultOEView.OptionsCustomization.AllowGroup = false;
            this.ResultOEView.OptionsCustomization.AllowQuickHideColumns = false;
            this.ResultOEView.OptionsCustomization.AllowSort = false;
            this.ResultOEView.OptionsCustomization.ShowBandsInCustomizationForm = false;
            this.ResultOEView.OptionsSelection.InvertSelection = true;
            this.ResultOEView.OptionsSelection.UseIndicatorForSelection = false;
            this.ResultOEView.OptionsView.AllowHtmlDrawGroups = false;
            this.ResultOEView.OptionsView.ShowBands = false;
            this.ResultOEView.OptionsView.ShowColumnHeaders = false;
            this.ResultOEView.OptionsView.ShowGroupExpandCollapseButtons = false;
            this.ResultOEView.OptionsView.ShowGroupPanel = false;
            this.ResultOEView.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.True;
            this.ResultOEView.OptionsView.ShowIndicator = false;
            this.ResultOEView.RowHeight = 30;
            this.ResultOEView.VertScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Never;
            this.ResultOEView.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.ResultOEView_CustomDrawCell);
            this.ResultOEView.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.ResultOEView_FocusedRowChanged);
            this.ResultOEView.CellValueChanged += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.ResultOEView_CellValueChanged);
            this.ResultOEView.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ResultOEView_KeyDown);
            // 
            // gridBand43
            // 
            this.gridBand43.AppearanceHeader.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.gridBand43.AppearanceHeader.BackColor2 = System.Drawing.Color.Silver;
            this.gridBand43.AppearanceHeader.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.gridBand43.AppearanceHeader.Options.UseBackColor = true;
            this.gridBand43.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand43.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand43.Caption = "gridBand1";
            this.gridBand43.Columns.Add(this.ResultOEType1);
            this.gridBand43.Name = "gridBand43";
            this.gridBand43.OptionsBand.AllowHotTrack = false;
            this.gridBand43.OptionsBand.AllowMove = false;
            this.gridBand43.OptionsBand.AllowPress = false;
            this.gridBand43.OptionsBand.AllowSize = false;
            this.gridBand43.OptionsBand.ShowInCustomizationForm = false;
            this.gridBand43.VisibleIndex = 0;
            this.gridBand43.Width = 110;
            // 
            // ResultOEType1
            // 
            this.ResultOEType1.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(170)))), ((int)(((byte)(170)))));
            this.ResultOEType1.AppearanceCell.Options.UseBackColor = true;
            this.ResultOEType1.FieldName = "Type1";
            this.ResultOEType1.Name = "ResultOEType1";
            this.ResultOEType1.OptionsColumn.AllowEdit = false;
            this.ResultOEType1.OptionsColumn.AllowFocus = false;
            this.ResultOEType1.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
            this.ResultOEType1.OptionsColumn.AllowIncrementalSearch = false;
            this.ResultOEType1.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.ResultOEType1.OptionsColumn.AllowMove = false;
            this.ResultOEType1.OptionsColumn.AllowShowHide = false;
            this.ResultOEType1.OptionsColumn.AllowSize = false;
            this.ResultOEType1.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.ResultOEType1.OptionsColumn.ImmediateUpdateRowPosition = DevExpress.Utils.DefaultBoolean.False;
            this.ResultOEType1.OptionsColumn.Printable = DevExpress.Utils.DefaultBoolean.False;
            this.ResultOEType1.OptionsColumn.ReadOnly = true;
            this.ResultOEType1.OptionsColumn.ShowCaption = false;
            this.ResultOEType1.OptionsColumn.ShowInCustomizationForm = false;
            this.ResultOEType1.OptionsColumn.ShowInExpressionEditor = false;
            this.ResultOEType1.OptionsColumn.TabStop = false;
            this.ResultOEType1.Visible = true;
            this.ResultOEType1.Width = 110;
            // 
            // gridBand44
            // 
            this.gridBand44.AppearanceHeader.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.gridBand44.AppearanceHeader.BackColor2 = System.Drawing.Color.Silver;
            this.gridBand44.AppearanceHeader.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.gridBand44.AppearanceHeader.Options.UseBackColor = true;
            this.gridBand44.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand44.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand44.Caption = "gridBand2";
            this.gridBand44.Columns.Add(this.ResultOEValue1);
            this.gridBand44.Name = "gridBand44";
            this.gridBand44.OptionsBand.AllowHotTrack = false;
            this.gridBand44.OptionsBand.AllowMove = false;
            this.gridBand44.OptionsBand.AllowPress = false;
            this.gridBand44.OptionsBand.AllowSize = false;
            this.gridBand44.OptionsBand.ShowInCustomizationForm = false;
            this.gridBand44.VisibleIndex = 1;
            this.gridBand44.Width = 150;
            // 
            // ResultOEValue1
            // 
            this.ResultOEValue1.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.ResultOEValue1.AppearanceCell.Options.UseBackColor = true;
            this.ResultOEValue1.AppearanceCell.Options.UseTextOptions = true;
            this.ResultOEValue1.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.ResultOEValue1.DisplayFormat.FormatString = "{0:#,##0.000}";
            this.ResultOEValue1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.ResultOEValue1.FieldName = "Value1";
            this.ResultOEValue1.Name = "ResultOEValue1";
            this.ResultOEValue1.OptionsColumn.AllowEdit = false;
            this.ResultOEValue1.OptionsColumn.AllowFocus = false;
            this.ResultOEValue1.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
            this.ResultOEValue1.OptionsColumn.AllowIncrementalSearch = false;
            this.ResultOEValue1.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.ResultOEValue1.OptionsColumn.AllowMove = false;
            this.ResultOEValue1.OptionsColumn.AllowShowHide = false;
            this.ResultOEValue1.OptionsColumn.AllowSize = false;
            this.ResultOEValue1.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.ResultOEValue1.OptionsColumn.ImmediateUpdateRowPosition = DevExpress.Utils.DefaultBoolean.False;
            this.ResultOEValue1.OptionsColumn.Printable = DevExpress.Utils.DefaultBoolean.False;
            this.ResultOEValue1.OptionsColumn.ShowCaption = false;
            this.ResultOEValue1.OptionsColumn.ShowInCustomizationForm = false;
            this.ResultOEValue1.OptionsColumn.ShowInExpressionEditor = false;
            this.ResultOEValue1.OptionsColumn.TabStop = false;
            this.ResultOEValue1.Visible = true;
            this.ResultOEValue1.Width = 150;
            // 
            // gridBand45
            // 
            this.gridBand45.AppearanceHeader.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.gridBand45.AppearanceHeader.BackColor2 = System.Drawing.Color.Silver;
            this.gridBand45.AppearanceHeader.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.gridBand45.AppearanceHeader.Options.UseBackColor = true;
            this.gridBand45.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand45.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand45.Caption = "gridBand3";
            this.gridBand45.Columns.Add(this.ResultOEType2);
            this.gridBand45.Name = "gridBand45";
            this.gridBand45.OptionsBand.AllowHotTrack = false;
            this.gridBand45.OptionsBand.AllowMove = false;
            this.gridBand45.OptionsBand.AllowPress = false;
            this.gridBand45.OptionsBand.AllowSize = false;
            this.gridBand45.OptionsBand.ShowInCustomizationForm = false;
            this.gridBand45.VisibleIndex = 2;
            this.gridBand45.Width = 110;
            // 
            // ResultOEType2
            // 
            this.ResultOEType2.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(170)))), ((int)(((byte)(170)))));
            this.ResultOEType2.AppearanceCell.Options.UseBackColor = true;
            this.ResultOEType2.FieldName = "Type2";
            this.ResultOEType2.Name = "ResultOEType2";
            this.ResultOEType2.OptionsColumn.AllowEdit = false;
            this.ResultOEType2.OptionsColumn.AllowFocus = false;
            this.ResultOEType2.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
            this.ResultOEType2.OptionsColumn.AllowIncrementalSearch = false;
            this.ResultOEType2.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.ResultOEType2.OptionsColumn.AllowMove = false;
            this.ResultOEType2.OptionsColumn.AllowShowHide = false;
            this.ResultOEType2.OptionsColumn.AllowSize = false;
            this.ResultOEType2.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.ResultOEType2.OptionsColumn.ImmediateUpdateRowPosition = DevExpress.Utils.DefaultBoolean.False;
            this.ResultOEType2.OptionsColumn.Printable = DevExpress.Utils.DefaultBoolean.False;
            this.ResultOEType2.OptionsColumn.ReadOnly = true;
            this.ResultOEType2.OptionsColumn.ShowCaption = false;
            this.ResultOEType2.OptionsColumn.ShowInCustomizationForm = false;
            this.ResultOEType2.OptionsColumn.ShowInExpressionEditor = false;
            this.ResultOEType2.OptionsColumn.TabStop = false;
            this.ResultOEType2.Visible = true;
            this.ResultOEType2.Width = 110;
            // 
            // gridBand46
            // 
            this.gridBand46.AppearanceHeader.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.gridBand46.AppearanceHeader.BackColor2 = System.Drawing.Color.Silver;
            this.gridBand46.AppearanceHeader.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.gridBand46.AppearanceHeader.Options.UseBackColor = true;
            this.gridBand46.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand46.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand46.Caption = "gridBand4";
            this.gridBand46.Columns.Add(this.ResultOEValue2);
            this.gridBand46.Name = "gridBand46";
            this.gridBand46.OptionsBand.AllowHotTrack = false;
            this.gridBand46.OptionsBand.AllowMove = false;
            this.gridBand46.OptionsBand.AllowPress = false;
            this.gridBand46.OptionsBand.AllowSize = false;
            this.gridBand46.OptionsBand.ShowInCustomizationForm = false;
            this.gridBand46.VisibleIndex = 3;
            this.gridBand46.Width = 150;
            // 
            // ResultOEValue2
            // 
            this.ResultOEValue2.AppearanceCell.Options.UseTextOptions = true;
            this.ResultOEValue2.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.ResultOEValue2.DisplayFormat.FormatString = "{0:#,##0.000}";
            this.ResultOEValue2.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.ResultOEValue2.FieldName = "Value2";
            this.ResultOEValue2.Name = "ResultOEValue2";
            this.ResultOEValue2.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
            this.ResultOEValue2.OptionsColumn.AllowIncrementalSearch = false;
            this.ResultOEValue2.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.ResultOEValue2.OptionsColumn.AllowMove = false;
            this.ResultOEValue2.OptionsColumn.AllowShowHide = false;
            this.ResultOEValue2.OptionsColumn.AllowSize = false;
            this.ResultOEValue2.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.ResultOEValue2.OptionsColumn.ImmediateUpdateRowPosition = DevExpress.Utils.DefaultBoolean.False;
            this.ResultOEValue2.OptionsColumn.Printable = DevExpress.Utils.DefaultBoolean.False;
            this.ResultOEValue2.OptionsColumn.ShowCaption = false;
            this.ResultOEValue2.OptionsColumn.ShowInCustomizationForm = false;
            this.ResultOEValue2.OptionsColumn.ShowInExpressionEditor = false;
            this.ResultOEValue2.Visible = true;
            this.ResultOEValue2.Width = 150;
            // 
            // BELabel
            // 
            this.BELabel.AutoSize = true;
            this.BELabel.Font = new System.Drawing.Font("Leelawadee UI", 20F, System.Drawing.FontStyle.Bold);
            this.BELabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(130)))), ((int)(((byte)(28)))), ((int)(((byte)(51)))));
            this.BELabel.Location = new System.Drawing.Point(551, -1);
            this.BELabel.Name = "BELabel";
            this.BELabel.Size = new System.Drawing.Size(167, 37);
            this.BELabel.TabIndex = 3;
            this.BELabel.Text = "Bunker Exp.";
            // 
            // ResultLabel
            // 
            this.ResultLabel.AutoSize = true;
            this.ResultLabel.Font = new System.Drawing.Font("Leelawadee UI", 20F, System.Drawing.FontStyle.Bold);
            this.ResultLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(130)))), ((int)(((byte)(28)))), ((int)(((byte)(51)))));
            this.ResultLabel.Location = new System.Drawing.Point(1068, -1);
            this.ResultLabel.Name = "ResultLabel";
            this.ResultLabel.Size = new System.Drawing.Size(97, 37);
            this.ResultLabel.TabIndex = 2;
            this.ResultLabel.Text = "Result";
            // 
            // OPLabel
            // 
            this.OPLabel.AutoSize = true;
            this.OPLabel.Font = new System.Drawing.Font("Leelawadee UI", 20F, System.Drawing.FontStyle.Bold);
            this.OPLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(130)))), ((int)(((byte)(28)))), ((int)(((byte)(51)))));
            this.OPLabel.Location = new System.Drawing.Point(4, -1);
            this.OPLabel.Name = "OPLabel";
            this.OPLabel.Size = new System.Drawing.Size(250, 37);
            this.OPLabel.TabIndex = 1;
            this.OPLabel.Text = "Operaton Expense";
            // 
            // PortRPanel
            // 
            this.PortRPanel.Controls.Add(this.PortRRemove);
            this.PortRPanel.Controls.Add(this.PortRInsert);
            this.PortRPanel.Controls.Add(this.PortRAppend);
            this.PortRPanel.Controls.Add(this.PortRClear);
            this.PortRPanel.Controls.Add(this.DurationPanel);
            this.PortRPanel.Controls.Add(this.PortRotationGridControl);
            this.PortRPanel.Controls.Add(this.PortRLabel);
            this.PortRPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.PortRPanel.Location = new System.Drawing.Point(0, 420);
            this.PortRPanel.Name = "PortRPanel";
            this.PortRPanel.Size = new System.Drawing.Size(1678, 332);
            this.PortRPanel.TabIndex = 0;
            // 
            // PortRRemove
            // 
            this.PortRRemove.Location = new System.Drawing.Point(1576, 11);
            this.PortRRemove.Name = "PortRRemove";
            this.PortRRemove.Size = new System.Drawing.Size(75, 23);
            this.PortRRemove.TabIndex = 11;
            this.PortRRemove.Text = "Remove";
            this.PortRRemove.Click += new System.EventHandler(this.PortRRemove_Click);
            // 
            // PortRInsert
            // 
            this.PortRInsert.Location = new System.Drawing.Point(1495, 11);
            this.PortRInsert.Name = "PortRInsert";
            this.PortRInsert.Size = new System.Drawing.Size(75, 23);
            this.PortRInsert.TabIndex = 10;
            this.PortRInsert.Text = "Insert";
            this.PortRInsert.Click += new System.EventHandler(this.PortRInsert_Click);
            // 
            // PortRAppend
            // 
            this.PortRAppend.Location = new System.Drawing.Point(1414, 11);
            this.PortRAppend.Name = "PortRAppend";
            this.PortRAppend.Size = new System.Drawing.Size(75, 23);
            this.PortRAppend.TabIndex = 9;
            this.PortRAppend.Text = "Append";
            this.PortRAppend.Click += new System.EventHandler(this.PortRAppend_Click);
            // 
            // PortRClear
            // 
            this.PortRClear.Location = new System.Drawing.Point(1333, 11);
            this.PortRClear.Name = "PortRClear";
            this.PortRClear.Size = new System.Drawing.Size(75, 23);
            this.PortRClear.TabIndex = 8;
            this.PortRClear.Text = "Clear";
            this.PortRClear.Click += new System.EventHandler(this.PortRClear_Click);
            // 
            // DurationPanel
            // 
            this.DurationPanel.CausesValidation = false;
            this.DurationPanel.Controls.Add(this.DurationLabel);
            this.DurationPanel.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Bold);
            this.DurationPanel.Location = new System.Drawing.Point(198, 3);
            this.DurationPanel.Name = "DurationPanel";
            this.DurationPanel.Size = new System.Drawing.Size(1129, 35);
            this.DurationPanel.TabIndex = 5;
            // 
            // DurationLabel
            // 
            this.DurationLabel.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.DurationLabel.AutoSize = true;
            this.DurationLabel.Location = new System.Drawing.Point(4, 8);
            this.DurationLabel.Name = "DurationLabel";
            this.DurationLabel.Size = new System.Drawing.Size(128, 18);
            this.DurationLabel.TabIndex = 0;
            this.DurationLabel.Text = "Total Duration : ";
            // 
            // PortRotationGridControl
            // 
            this.PortRotationGridControl.DataSource = this.portRBindingSource;
            this.PortRotationGridControl.Location = new System.Drawing.Point(11, 40);
            this.PortRotationGridControl.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat;
            this.PortRotationGridControl.LookAndFeel.UseDefaultLookAndFeel = false;
            this.PortRotationGridControl.MainView = this.PortRotationView;
            this.PortRotationGridControl.Name = "PortRotationGridControl";
            this.PortRotationGridControl.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.DateEdit,
            this.NonBtnDateEdit,
            this.PortTypeComboBox});
            this.PortRotationGridControl.Size = new System.Drawing.Size(1640, 260);
            this.PortRotationGridControl.TabIndex = 4;
            this.PortRotationGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.PortRotationView});
            this.PortRotationGridControl.EditorKeyDown += new System.Windows.Forms.KeyEventHandler(this.PortRotationGridControl_EditorKeyDown);
            // 
            // portRBindingSource
            // 
            this.portRBindingSource.DataMember = "PortR";
            this.portRBindingSource.DataSource = this.sEDataBindingSource;
            // 
            // PortRotationView
            // 
            this.PortRotationView.Appearance.FocusedCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(120)))), ((int)(((byte)(177)))), ((int)(((byte)(145)))));
            this.PortRotationView.Appearance.FocusedCell.Options.UseBackColor = true;
            this.PortRotationView.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(120)))), ((int)(((byte)(177)))), ((int)(((byte)(145)))));
            this.PortRotationView.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.PortRotationView.Appearance.FocusedRow.Options.UseBackColor = true;
            this.PortRotationView.Appearance.FocusedRow.Options.UseForeColor = true;
            this.PortRotationView.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.Transparent;
            this.PortRotationView.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.PortRotationView.BandPanelRowHeight = 30;
            this.PortRotationView.Bands.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand30,
            this.gridBand31,
            this.gridBand32,
            this.gridBand33,
            this.gridBand34,
            this.gridBand35,
            this.gridBand36,
            this.gridBand37,
            this.gridBand38,
            this.gridBand39,
            this.gridBand40,
            this.gridBand41,
            this.gridBand42,
            this.gridBand60});
            this.PortRotationView.Columns.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn[] {
            this.PortRIndex,
            this.PortRType,
            this.PortRName,
            this.PortRNameDetail,
            this.PortRDistance,
            this.PortRSeca,
            this.PortRWF,
            this.PortRSpd,
            this.PortRSea,
            this.PortRLDRate,
            this.PortRIdle,
            this.PortRWork,
            this.PortRCrane,
            this.PortRDem,
            this.PortRDes,
            this.PortRPortCharge,
            this.PortRArrival,
            this.PortRDepature});
            this.PortRotationView.FooterPanelHeight = 40;
            this.PortRotationView.GridControl = this.PortRotationGridControl;
            this.PortRotationView.HorzScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Never;
            this.PortRotationView.Name = "PortRotationView";
            this.PortRotationView.OptionsBehavior.AlignGroupSummaryInGroupRow = DevExpress.Utils.DefaultBoolean.False;
            this.PortRotationView.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.False;
            this.PortRotationView.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.False;
            this.PortRotationView.OptionsBehavior.AllowFixedGroups = DevExpress.Utils.DefaultBoolean.False;
            this.PortRotationView.OptionsBehavior.AllowPartialGroups = DevExpress.Utils.DefaultBoolean.False;
            this.PortRotationView.OptionsBehavior.AllowPartialRedrawOnScrolling = false;
            this.PortRotationView.OptionsBehavior.AllowPixelScrolling = DevExpress.Utils.DefaultBoolean.False;
            this.PortRotationView.OptionsCustomization.AllowBandMoving = false;
            this.PortRotationView.OptionsCustomization.AllowBandResizing = false;
            this.PortRotationView.OptionsCustomization.AllowColumnMoving = false;
            this.PortRotationView.OptionsCustomization.AllowColumnResizing = false;
            this.PortRotationView.OptionsCustomization.AllowGroup = false;
            this.PortRotationView.OptionsCustomization.AllowQuickHideColumns = false;
            this.PortRotationView.OptionsCustomization.AllowSort = false;
            this.PortRotationView.OptionsCustomization.ShowBandsInCustomizationForm = false;
            this.PortRotationView.OptionsSelection.InvertSelection = true;
            this.PortRotationView.OptionsSelection.UseIndicatorForSelection = false;
            this.PortRotationView.OptionsView.AllowHtmlDrawGroups = false;
            this.PortRotationView.OptionsView.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways;
            this.PortRotationView.OptionsView.ShowColumnHeaders = false;
            this.PortRotationView.OptionsView.ShowFooter = true;
            this.PortRotationView.OptionsView.ShowGroupExpandCollapseButtons = false;
            this.PortRotationView.OptionsView.ShowGroupPanel = false;
            this.PortRotationView.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.True;
            this.PortRotationView.OptionsView.ShowIndicator = false;
            this.PortRotationView.RowHeight = 30;
            this.PortRotationView.VertScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Never;
            this.PortRotationView.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.PortRotationView_CustomDrawCell);
            this.PortRotationView.CustomDrawFooterCell += new DevExpress.XtraGrid.Views.Grid.FooterCellCustomDrawEventHandler(this.PortRotationView_CustomDrawFooterCell);
            this.PortRotationView.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.PortRotationView_CustomRowCellEdit);
            this.PortRotationView.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.PortRotationView_FocusedRowChanged);
            this.PortRotationView.CellValueChanged += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.PortRotationView_CellValueChanged);
            this.PortRotationView.KeyDown += new System.Windows.Forms.KeyEventHandler(this.PortRotationView_KeyDown);
            // 
            // gridBand30
            // 
            this.gridBand30.AppearanceHeader.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.gridBand30.AppearanceHeader.BackColor2 = System.Drawing.Color.Silver;
            this.gridBand30.AppearanceHeader.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.gridBand30.AppearanceHeader.Options.UseBackColor = true;
            this.gridBand30.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand30.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand30.Columns.Add(this.PortRIndex);
            this.gridBand30.Name = "gridBand30";
            this.gridBand30.OptionsBand.AllowHotTrack = false;
            this.gridBand30.OptionsBand.AllowMove = false;
            this.gridBand30.OptionsBand.AllowPress = false;
            this.gridBand30.OptionsBand.AllowSize = false;
            this.gridBand30.OptionsBand.ShowInCustomizationForm = false;
            this.gridBand30.VisibleIndex = 0;
            this.gridBand30.Width = 25;
            // 
            // PortRIndex
            // 
            this.PortRIndex.AppearanceCell.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.PortRIndex.AppearanceCell.Options.UseBackColor = true;
            this.PortRIndex.AppearanceCell.Options.UseTextOptions = true;
            this.PortRIndex.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.PortRIndex.FieldName = "Index";
            this.PortRIndex.Name = "PortRIndex";
            this.PortRIndex.OptionsColumn.AllowEdit = false;
            this.PortRIndex.OptionsColumn.AllowFocus = false;
            this.PortRIndex.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
            this.PortRIndex.OptionsColumn.AllowIncrementalSearch = false;
            this.PortRIndex.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.PortRIndex.OptionsColumn.AllowMove = false;
            this.PortRIndex.OptionsColumn.AllowShowHide = false;
            this.PortRIndex.OptionsColumn.AllowSize = false;
            this.PortRIndex.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.PortRIndex.OptionsColumn.ImmediateUpdateRowPosition = DevExpress.Utils.DefaultBoolean.False;
            this.PortRIndex.OptionsColumn.Printable = DevExpress.Utils.DefaultBoolean.False;
            this.PortRIndex.OptionsColumn.ShowCaption = false;
            this.PortRIndex.OptionsColumn.ShowInCustomizationForm = false;
            this.PortRIndex.OptionsColumn.ShowInExpressionEditor = false;
            this.PortRIndex.OptionsColumn.TabStop = false;
            this.PortRIndex.Visible = true;
            this.PortRIndex.Width = 25;
            // 
            // gridBand31
            // 
            this.gridBand31.AppearanceHeader.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.gridBand31.AppearanceHeader.BackColor2 = System.Drawing.Color.Silver;
            this.gridBand31.AppearanceHeader.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.gridBand31.AppearanceHeader.Options.UseBackColor = true;
            this.gridBand31.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand31.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand31.Caption = "Type";
            this.gridBand31.Columns.Add(this.PortRType);
            this.gridBand31.Name = "gridBand31";
            this.gridBand31.OptionsBand.AllowHotTrack = false;
            this.gridBand31.OptionsBand.AllowMove = false;
            this.gridBand31.OptionsBand.AllowPress = false;
            this.gridBand31.OptionsBand.AllowSize = false;
            this.gridBand31.OptionsBand.ShowInCustomizationForm = false;
            this.gridBand31.VisibleIndex = 1;
            // 
            // PortRType
            // 
            this.PortRType.ColumnEdit = this.PortTypeComboBox;
            this.PortRType.FieldName = "Type";
            this.PortRType.Name = "PortRType";
            this.PortRType.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowOnlyInEditor;
            this.PortRType.Visible = true;
            this.PortRType.Width = 70;
            // 
            // PortTypeComboBox
            // 
            this.PortTypeComboBox.AutoHeight = false;
            this.PortTypeComboBox.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.PortTypeComboBox.Items.AddRange(new object[] {
            "Ballast",
            "Loading",
            "Dischg.",
            "Bunker",
            "Canal",
            "Passing"});
            this.PortTypeComboBox.Name = "PortTypeComboBox";
            // 
            // gridBand32
            // 
            this.gridBand32.AppearanceHeader.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.gridBand32.AppearanceHeader.BackColor2 = System.Drawing.Color.Silver;
            this.gridBand32.AppearanceHeader.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.gridBand32.AppearanceHeader.Options.UseBackColor = true;
            this.gridBand32.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand32.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand32.Caption = "Port Name or Coordinates";
            this.gridBand32.Columns.Add(this.PortRName);
            this.gridBand32.Columns.Add(this.PortRCrane);
            this.gridBand32.Name = "gridBand32";
            this.gridBand32.OptionsBand.AllowHotTrack = false;
            this.gridBand32.OptionsBand.AllowMove = false;
            this.gridBand32.OptionsBand.AllowPress = false;
            this.gridBand32.OptionsBand.AllowSize = false;
            this.gridBand32.OptionsBand.ShowInCustomizationForm = false;
            this.gridBand32.VisibleIndex = 2;
            this.gridBand32.Width = 245;
            // 
            // PortRName
            // 
            this.PortRName.FieldName = "Name";
            this.PortRName.Name = "PortRName";
            this.PortRName.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
            this.PortRName.OptionsColumn.AllowIncrementalSearch = false;
            this.PortRName.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.PortRName.OptionsColumn.AllowMove = false;
            this.PortRName.OptionsColumn.AllowShowHide = false;
            this.PortRName.OptionsColumn.AllowSize = false;
            this.PortRName.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.PortRName.OptionsColumn.ImmediateUpdateRowPosition = DevExpress.Utils.DefaultBoolean.False;
            this.PortRName.OptionsColumn.Printable = DevExpress.Utils.DefaultBoolean.False;
            this.PortRName.OptionsColumn.ShowCaption = false;
            this.PortRName.OptionsColumn.ShowInCustomizationForm = false;
            this.PortRName.OptionsColumn.ShowInExpressionEditor = false;
            this.PortRName.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways;
            this.PortRName.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Custom, "Name", "Total")});
            this.PortRName.Visible = true;
            this.PortRName.Width = 216;
            // 
            // PortRCrane
            // 
            this.PortRCrane.Name = "PortRCrane";
            this.PortRCrane.Visible = true;
            this.PortRCrane.Width = 29;
            // 
            // gridBand33
            // 
            this.gridBand33.AppearanceHeader.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.gridBand33.AppearanceHeader.BackColor2 = System.Drawing.Color.Silver;
            this.gridBand33.AppearanceHeader.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.gridBand33.AppearanceHeader.Options.UseBackColor = true;
            this.gridBand33.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand33.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand33.Caption = "Distance / (S)ECA";
            this.gridBand33.Columns.Add(this.PortRDistance);
            this.gridBand33.Columns.Add(this.PortRSeca);
            this.gridBand33.Name = "gridBand33";
            this.gridBand33.OptionsBand.AllowHotTrack = false;
            this.gridBand33.OptionsBand.AllowMove = false;
            this.gridBand33.OptionsBand.AllowPress = false;
            this.gridBand33.OptionsBand.AllowSize = false;
            this.gridBand33.OptionsBand.ShowInCustomizationForm = false;
            this.gridBand33.VisibleIndex = 3;
            this.gridBand33.Width = 200;
            // 
            // PortRDistance
            // 
            this.PortRDistance.DisplayFormat.FormatString = "{0:#,##0}";
            this.PortRDistance.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.PortRDistance.FieldName = "Distance";
            this.PortRDistance.Name = "PortRDistance";
            this.PortRDistance.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Distance", "{0:#,##0}")});
            this.PortRDistance.Visible = true;
            this.PortRDistance.Width = 98;
            // 
            // PortRSeca
            // 
            this.PortRSeca.DisplayFormat.FormatString = "{0:#,##0}";
            this.PortRSeca.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.PortRSeca.FieldName = "Seca";
            this.PortRSeca.Name = "PortRSeca";
            this.PortRSeca.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Seca", "{0:#,##0}")});
            this.PortRSeca.Visible = true;
            this.PortRSeca.Width = 102;
            // 
            // gridBand34
            // 
            this.gridBand34.AppearanceHeader.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.gridBand34.AppearanceHeader.BackColor2 = System.Drawing.Color.Silver;
            this.gridBand34.AppearanceHeader.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.gridBand34.AppearanceHeader.Options.UseBackColor = true;
            this.gridBand34.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand34.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand34.Caption = "W.F";
            this.gridBand34.Columns.Add(this.PortRWF);
            this.gridBand34.Name = "gridBand34";
            this.gridBand34.OptionsBand.AllowHotTrack = false;
            this.gridBand34.OptionsBand.AllowMove = false;
            this.gridBand34.OptionsBand.AllowPress = false;
            this.gridBand34.OptionsBand.AllowSize = false;
            this.gridBand34.OptionsBand.ShowInCustomizationForm = false;
            this.gridBand34.VisibleIndex = 4;
            // 
            // PortRWF
            // 
            this.PortRWF.DisplayFormat.FormatString = "{0:#0.0}%";
            this.PortRWF.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.PortRWF.FieldName = "WF";
            this.PortRWF.Name = "PortRWF";
            this.PortRWF.Visible = true;
            this.PortRWF.Width = 70;
            // 
            // gridBand35
            // 
            this.gridBand35.AppearanceHeader.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.gridBand35.AppearanceHeader.BackColor2 = System.Drawing.Color.Silver;
            this.gridBand35.AppearanceHeader.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.gridBand35.AppearanceHeader.Options.UseBackColor = true;
            this.gridBand35.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand35.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand35.Caption = "Spd";
            this.gridBand35.Columns.Add(this.PortRSpd);
            this.gridBand35.Name = "gridBand35";
            this.gridBand35.OptionsBand.AllowHotTrack = false;
            this.gridBand35.OptionsBand.AllowMove = false;
            this.gridBand35.OptionsBand.AllowPress = false;
            this.gridBand35.OptionsBand.AllowSize = false;
            this.gridBand35.OptionsBand.ShowInCustomizationForm = false;
            this.gridBand35.VisibleIndex = 5;
            this.gridBand35.Width = 80;
            // 
            // PortRSpd
            // 
            this.PortRSpd.DisplayFormat.FormatString = "{0:#0.00}";
            this.PortRSpd.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.PortRSpd.FieldName = "Spd";
            this.PortRSpd.Name = "PortRSpd";
            this.PortRSpd.Visible = true;
            this.PortRSpd.Width = 80;
            // 
            // gridBand36
            // 
            this.gridBand36.AppearanceHeader.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.gridBand36.AppearanceHeader.BackColor2 = System.Drawing.Color.Silver;
            this.gridBand36.AppearanceHeader.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.gridBand36.AppearanceHeader.Options.UseBackColor = true;
            this.gridBand36.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand36.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand36.Caption = "Sea";
            this.gridBand36.Columns.Add(this.PortRSea);
            this.gridBand36.Name = "gridBand36";
            this.gridBand36.OptionsBand.AllowHotTrack = false;
            this.gridBand36.OptionsBand.AllowMove = false;
            this.gridBand36.OptionsBand.AllowPress = false;
            this.gridBand36.OptionsBand.AllowSize = false;
            this.gridBand36.OptionsBand.ShowInCustomizationForm = false;
            this.gridBand36.VisibleIndex = 6;
            this.gridBand36.Width = 90;
            // 
            // PortRSea
            // 
            this.PortRSea.DisplayFormat.FormatString = "{0:#,##0.00}";
            this.PortRSea.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.PortRSea.FieldName = "Sea";
            this.PortRSea.Name = "PortRSea";
            this.PortRSea.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Sea", "{0:#,##0.00}")});
            this.PortRSea.Visible = true;
            this.PortRSea.Width = 90;
            // 
            // gridBand37
            // 
            this.gridBand37.AppearanceHeader.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.gridBand37.AppearanceHeader.BackColor2 = System.Drawing.Color.Silver;
            this.gridBand37.AppearanceHeader.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.gridBand37.AppearanceHeader.Options.UseBackColor = true;
            this.gridBand37.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand37.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand37.Caption = "L/D Rate";
            this.gridBand37.Columns.Add(this.PortRLDRate);
            this.gridBand37.Name = "gridBand37";
            this.gridBand37.OptionsBand.AllowHotTrack = false;
            this.gridBand37.OptionsBand.AllowMove = false;
            this.gridBand37.OptionsBand.AllowPress = false;
            this.gridBand37.OptionsBand.AllowSize = false;
            this.gridBand37.OptionsBand.ShowInCustomizationForm = false;
            this.gridBand37.VisibleIndex = 7;
            this.gridBand37.Width = 90;
            // 
            // PortRLDRate
            // 
            this.PortRLDRate.DisplayFormat.FormatString = "{0:#,##0.000}";
            this.PortRLDRate.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.PortRLDRate.FieldName = "L/DRate";
            this.PortRLDRate.Name = "PortRLDRate";
            this.PortRLDRate.Visible = true;
            this.PortRLDRate.Width = 90;
            // 
            // gridBand38
            // 
            this.gridBand38.AppearanceHeader.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.gridBand38.AppearanceHeader.BackColor2 = System.Drawing.Color.Silver;
            this.gridBand38.AppearanceHeader.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.gridBand38.AppearanceHeader.Options.UseBackColor = true;
            this.gridBand38.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand38.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand38.Caption = "Port ( I / W )";
            this.gridBand38.Columns.Add(this.PortRIdle);
            this.gridBand38.Columns.Add(this.PortRWork);
            this.gridBand38.Columns.Add(this.PortRNameDetail);
            this.gridBand38.Name = "gridBand38";
            this.gridBand38.OptionsBand.AllowHotTrack = false;
            this.gridBand38.OptionsBand.AllowMove = false;
            this.gridBand38.OptionsBand.AllowPress = false;
            this.gridBand38.OptionsBand.AllowSize = false;
            this.gridBand38.OptionsBand.ShowInCustomizationForm = false;
            this.gridBand38.VisibleIndex = 8;
            this.gridBand38.Width = 200;
            // 
            // PortRIdle
            // 
            this.PortRIdle.DisplayFormat.FormatString = "{0:#,##0.00}";
            this.PortRIdle.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.PortRIdle.FieldName = "Idle";
            this.PortRIdle.Name = "PortRIdle";
            this.PortRIdle.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Idle", "{0:#,##0.000}")});
            this.PortRIdle.Visible = true;
            this.PortRIdle.Width = 90;
            // 
            // PortRWork
            // 
            this.PortRWork.DisplayFormat.FormatString = "{0:#,##0.00}";
            this.PortRWork.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.PortRWork.FieldName = "Work";
            this.PortRWork.Name = "PortRWork";
            this.PortRWork.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Work", "{0:#,##0.000}")});
            this.PortRWork.Visible = true;
            this.PortRWork.Width = 81;
            // 
            // PortRNameDetail
            // 
            this.PortRNameDetail.Name = "PortRNameDetail";
            this.PortRNameDetail.Visible = true;
            this.PortRNameDetail.Width = 29;
            // 
            // gridBand39
            // 
            this.gridBand39.AppearanceHeader.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.gridBand39.AppearanceHeader.BackColor2 = System.Drawing.Color.Silver;
            this.gridBand39.AppearanceHeader.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.gridBand39.AppearanceHeader.Options.UseBackColor = true;
            this.gridBand39.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand39.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand39.Caption = "Dem";
            this.gridBand39.Columns.Add(this.PortRDem);
            this.gridBand39.Name = "gridBand39";
            this.gridBand39.OptionsBand.AllowHotTrack = false;
            this.gridBand39.OptionsBand.AllowMove = false;
            this.gridBand39.OptionsBand.AllowPress = false;
            this.gridBand39.OptionsBand.AllowSize = false;
            this.gridBand39.OptionsBand.ShowInCustomizationForm = false;
            this.gridBand39.VisibleIndex = 9;
            this.gridBand39.Width = 100;
            // 
            // PortRDem
            // 
            this.PortRDem.DisplayFormat.FormatString = "{0:#,##0.000}";
            this.PortRDem.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.PortRDem.FieldName = "Dem";
            this.PortRDem.Name = "PortRDem";
            this.PortRDem.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Dem", "{0:#,##0.000}")});
            this.PortRDem.Visible = true;
            this.PortRDem.Width = 100;
            // 
            // gridBand40
            // 
            this.gridBand40.AppearanceHeader.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.gridBand40.AppearanceHeader.BackColor2 = System.Drawing.Color.Silver;
            this.gridBand40.AppearanceHeader.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.gridBand40.AppearanceHeader.Options.UseBackColor = true;
            this.gridBand40.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand40.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand40.Caption = "Des";
            this.gridBand40.Columns.Add(this.PortRDes);
            this.gridBand40.Name = "gridBand40";
            this.gridBand40.OptionsBand.AllowHotTrack = false;
            this.gridBand40.OptionsBand.AllowMove = false;
            this.gridBand40.OptionsBand.AllowPress = false;
            this.gridBand40.OptionsBand.AllowSize = false;
            this.gridBand40.OptionsBand.ShowInCustomizationForm = false;
            this.gridBand40.VisibleIndex = 10;
            this.gridBand40.Width = 100;
            // 
            // PortRDes
            // 
            this.PortRDes.DisplayFormat.FormatString = "{0:#,##0.000}";
            this.PortRDes.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.PortRDes.FieldName = "Des";
            this.PortRDes.Name = "PortRDes";
            this.PortRDes.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Des", "{0:#,##0.000}")});
            this.PortRDes.Visible = true;
            this.PortRDes.Width = 100;
            // 
            // gridBand41
            // 
            this.gridBand41.AppearanceHeader.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.gridBand41.AppearanceHeader.BackColor2 = System.Drawing.Color.Silver;
            this.gridBand41.AppearanceHeader.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.gridBand41.AppearanceHeader.Options.UseBackColor = true;
            this.gridBand41.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand41.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand41.Caption = "Port Charge";
            this.gridBand41.Columns.Add(this.PortRPortCharge);
            this.gridBand41.Name = "gridBand41";
            this.gridBand41.OptionsBand.AllowHotTrack = false;
            this.gridBand41.OptionsBand.AllowMove = false;
            this.gridBand41.OptionsBand.AllowPress = false;
            this.gridBand41.OptionsBand.AllowSize = false;
            this.gridBand41.OptionsBand.ShowInCustomizationForm = false;
            this.gridBand41.VisibleIndex = 11;
            this.gridBand41.Width = 120;
            // 
            // PortRPortCharge
            // 
            this.PortRPortCharge.DisplayFormat.FormatString = "{0:#,##0.000}";
            this.PortRPortCharge.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.PortRPortCharge.FieldName = "PortCharge";
            this.PortRPortCharge.Name = "PortRPortCharge";
            this.PortRPortCharge.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
            this.PortRPortCharge.OptionsColumn.AllowIncrementalSearch = false;
            this.PortRPortCharge.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.PortRPortCharge.OptionsColumn.AllowMove = false;
            this.PortRPortCharge.OptionsColumn.AllowShowHide = false;
            this.PortRPortCharge.OptionsColumn.AllowSize = false;
            this.PortRPortCharge.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.PortRPortCharge.OptionsColumn.ShowCaption = false;
            this.PortRPortCharge.OptionsColumn.ShowInCustomizationForm = false;
            this.PortRPortCharge.OptionsColumn.ShowInExpressionEditor = false;
            this.PortRPortCharge.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "PortCharge", "{0:#,##0.000}")});
            this.PortRPortCharge.Visible = true;
            this.PortRPortCharge.Width = 120;
            // 
            // gridBand42
            // 
            this.gridBand42.AppearanceHeader.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.gridBand42.AppearanceHeader.BackColor2 = System.Drawing.Color.Silver;
            this.gridBand42.AppearanceHeader.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.gridBand42.AppearanceHeader.Options.UseBackColor = true;
            this.gridBand42.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand42.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand42.Caption = "Arrival";
            this.gridBand42.Columns.Add(this.PortRArrival);
            this.gridBand42.Name = "gridBand42";
            this.gridBand42.OptionsBand.AllowHotTrack = false;
            this.gridBand42.OptionsBand.AllowMove = false;
            this.gridBand42.OptionsBand.AllowPress = false;
            this.gridBand42.OptionsBand.AllowSize = false;
            this.gridBand42.OptionsBand.ShowInCustomizationForm = false;
            this.gridBand42.VisibleIndex = 12;
            this.gridBand42.Width = 123;
            // 
            // PortRArrival
            // 
            this.PortRArrival.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(234)))), ((int)(((byte)(249)))));
            this.PortRArrival.AppearanceCell.Options.UseBackColor = true;
            this.PortRArrival.ColumnEdit = this.NonBtnDateEdit;
            this.PortRArrival.DisplayFormat.FormatString = "{0:yyyy-MM-dd HH:mm}";
            this.PortRArrival.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.PortRArrival.FieldName = "Arrival";
            this.PortRArrival.Name = "PortRArrival";
            this.PortRArrival.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
            this.PortRArrival.OptionsColumn.AllowIncrementalSearch = false;
            this.PortRArrival.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.PortRArrival.OptionsColumn.AllowMove = false;
            this.PortRArrival.OptionsColumn.AllowShowHide = false;
            this.PortRArrival.OptionsColumn.AllowSize = false;
            this.PortRArrival.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.PortRArrival.OptionsColumn.ImmediateUpdateRowPosition = DevExpress.Utils.DefaultBoolean.False;
            this.PortRArrival.OptionsColumn.ShowCaption = false;
            this.PortRArrival.OptionsColumn.ShowInCustomizationForm = false;
            this.PortRArrival.OptionsColumn.ShowInExpressionEditor = false;
            this.PortRArrival.OptionsColumn.TabStop = false;
            this.PortRArrival.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways;
            this.PortRArrival.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Min, "Arrival", "{0:yyyy-MM-dd HH:mm}")});
            this.PortRArrival.Visible = true;
            this.PortRArrival.Width = 123;
            // 
            // NonBtnDateEdit
            // 
            this.NonBtnDateEdit.AutoHeight = false;
            this.NonBtnDateEdit.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", 0, true, false, false, editorButtonImageOptions1, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, serializableAppearanceObject2, serializableAppearanceObject3, serializableAppearanceObject4, "", null, null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.NonBtnDateEdit.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.NonBtnDateEdit.Name = "NonBtnDateEdit";
            // 
            // gridBand60
            // 
            this.gridBand60.AppearanceHeader.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.gridBand60.AppearanceHeader.BackColor2 = System.Drawing.Color.Silver;
            this.gridBand60.AppearanceHeader.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.gridBand60.AppearanceHeader.Options.UseBackColor = true;
            this.gridBand60.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand60.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand60.Caption = "Depature";
            this.gridBand60.Columns.Add(this.PortRDepature);
            this.gridBand60.Name = "gridBand60";
            this.gridBand60.VisibleIndex = 13;
            this.gridBand60.Width = 123;
            // 
            // PortRDepature
            // 
            this.PortRDepature.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(234)))), ((int)(((byte)(249)))));
            this.PortRDepature.AppearanceCell.Options.UseBackColor = true;
            this.PortRDepature.ColumnEdit = this.NonBtnDateEdit;
            this.PortRDepature.DisplayFormat.FormatString = "{0:yyyy-MM-dd HH:mm}";
            this.PortRDepature.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.PortRDepature.FieldName = "Depature";
            this.PortRDepature.Name = "PortRDepature";
            this.PortRDepature.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
            this.PortRDepature.OptionsColumn.AllowIncrementalSearch = false;
            this.PortRDepature.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.PortRDepature.OptionsColumn.AllowMove = false;
            this.PortRDepature.OptionsColumn.AllowShowHide = false;
            this.PortRDepature.OptionsColumn.AllowSize = false;
            this.PortRDepature.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.PortRDepature.OptionsColumn.ImmediateUpdateRowPosition = DevExpress.Utils.DefaultBoolean.False;
            this.PortRDepature.OptionsColumn.Printable = DevExpress.Utils.DefaultBoolean.False;
            this.PortRDepature.OptionsColumn.ShowCaption = false;
            this.PortRDepature.OptionsColumn.ShowInCustomizationForm = false;
            this.PortRDepature.OptionsColumn.ShowInExpressionEditor = false;
            this.PortRDepature.OptionsColumn.TabStop = false;
            this.PortRDepature.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways;
            this.PortRDepature.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Max, "Depature", "{0:yyyy-MM-dd HH:mm}")});
            this.PortRDepature.Visible = true;
            this.PortRDepature.Width = 123;
            // 
            // DateEdit
            // 
            this.DateEdit.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.DateEdit.Appearance.Options.UseBackColor = true;
            this.DateEdit.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            editorButtonImageOptions2.Image = ((System.Drawing.Image)(resources.GetObject("editorButtonImageOptions2.Image")));
            serializableAppearanceObject5.BackColor = System.Drawing.Color.Transparent;
            serializableAppearanceObject5.Options.UseBackColor = true;
            this.DateEdit.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, editorButtonImageOptions2, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject5, serializableAppearanceObject6, serializableAppearanceObject7, serializableAppearanceObject8, "", null, null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.DateEdit.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.DateEdit.CalendarTimeEditing = DevExpress.Utils.DefaultBoolean.True;
            this.DateEdit.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DateEdit.EditFormat.FormatString = "{0:yyyy-MM-dd HH:mm}";
            this.DateEdit.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DateEdit.Mask.EditMask = "yyyy-MM-dd HH:mm";
            this.DateEdit.Name = "DateEdit";
            // 
            // PortRLabel
            // 
            this.PortRLabel.AutoSize = true;
            this.PortRLabel.Dock = System.Windows.Forms.DockStyle.Top;
            this.PortRLabel.Font = new System.Drawing.Font("Leelawadee UI", 20F, System.Drawing.FontStyle.Bold);
            this.PortRLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(130)))), ((int)(((byte)(28)))), ((int)(((byte)(51)))));
            this.PortRLabel.Location = new System.Drawing.Point(0, 0);
            this.PortRLabel.Name = "PortRLabel";
            this.PortRLabel.Size = new System.Drawing.Size(193, 37);
            this.PortRLabel.TabIndex = 1;
            this.PortRLabel.Text = "Port Rotation";
            // 
            // CargoPanel
            // 
            this.CargoPanel.Controls.Add(this.CargoRemove);
            this.CargoPanel.Controls.Add(this.CargoInsert);
            this.CargoPanel.Controls.Add(this.CargoAppend);
            this.CargoPanel.Controls.Add(this.CargoClear);
            this.CargoPanel.Controls.Add(this.CargoGridControl);
            this.CargoPanel.Controls.Add(this.CargoLabel);
            this.CargoPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.CargoPanel.Location = new System.Drawing.Point(0, 195);
            this.CargoPanel.Name = "CargoPanel";
            this.CargoPanel.Size = new System.Drawing.Size(1678, 225);
            this.CargoPanel.TabIndex = 1;
            // 
            // CargoRemove
            // 
            this.CargoRemove.Location = new System.Drawing.Point(1576, 14);
            this.CargoRemove.Name = "CargoRemove";
            this.CargoRemove.Size = new System.Drawing.Size(75, 23);
            this.CargoRemove.TabIndex = 11;
            this.CargoRemove.Text = "Remove";
            this.CargoRemove.Click += new System.EventHandler(this.CargoRemove_Click);
            // 
            // CargoInsert
            // 
            this.CargoInsert.Location = new System.Drawing.Point(1495, 14);
            this.CargoInsert.Name = "CargoInsert";
            this.CargoInsert.Size = new System.Drawing.Size(75, 23);
            this.CargoInsert.TabIndex = 10;
            this.CargoInsert.Text = "Insert";
            this.CargoInsert.Click += new System.EventHandler(this.CargoInsert_Click);
            // 
            // CargoAppend
            // 
            this.CargoAppend.Location = new System.Drawing.Point(1414, 14);
            this.CargoAppend.Name = "CargoAppend";
            this.CargoAppend.Size = new System.Drawing.Size(75, 23);
            this.CargoAppend.TabIndex = 9;
            this.CargoAppend.Text = "Append";
            this.CargoAppend.Click += new System.EventHandler(this.CargoAppend_Click);
            // 
            // CargoClear
            // 
            this.CargoClear.Location = new System.Drawing.Point(1333, 14);
            this.CargoClear.Name = "CargoClear";
            this.CargoClear.Size = new System.Drawing.Size(75, 23);
            this.CargoClear.TabIndex = 8;
            this.CargoClear.Text = "Clear";
            this.CargoClear.Click += new System.EventHandler(this.CargoClear_Click);
            // 
            // CargoGridControl
            // 
            this.CargoGridControl.DataSource = this.cargoBindingSource;
            this.CargoGridControl.Location = new System.Drawing.Point(11, 49);
            this.CargoGridControl.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat;
            this.CargoGridControl.LookAndFeel.UseDefaultLookAndFeel = false;
            this.CargoGridControl.MainView = this.CargoView;
            this.CargoGridControl.Name = "CargoGridControl";
            this.CargoGridControl.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.CargoHistoryBtn,
            this.QuantityTermComboBox,
            this.TermComboBox});
            this.CargoGridControl.Size = new System.Drawing.Size(1640, 136);
            this.CargoGridControl.TabIndex = 3;
            this.CargoGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.CargoView});
            this.CargoGridControl.EditorKeyDown += new System.Windows.Forms.KeyEventHandler(this.CargoGridControl_EditorKeyDown);
            // 
            // cargoBindingSource
            // 
            this.cargoBindingSource.DataMember = "Cargo";
            this.cargoBindingSource.DataSource = this.sEDataBindingSource;
            // 
            // CargoView
            // 
            this.CargoView.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(120)))), ((int)(((byte)(177)))), ((int)(((byte)(145)))));
            this.CargoView.Appearance.FocusedRow.Options.UseBackColor = true;
            this.CargoView.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.Transparent;
            this.CargoView.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.CargoView.BandPanelRowHeight = 30;
            this.CargoView.Bands.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand17,
            this.gridBand18,
            this.gridBand19,
            this.gridBand20,
            this.gridBand21,
            this.gridBand22,
            this.gridBand23,
            this.gridBand24,
            this.gridBand25,
            this.gridBand26,
            this.gridBand27,
            this.gridBand28,
            this.gridBand29});
            this.CargoView.Columns.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn[] {
            this.CargoIndex,
            this.CargoAccount,
            this.CargoHistory,
            this.CargoName,
            this.LoadingPort,
            this.DischargingPort,
            this.Quantity,
            this.QuantityTerm,
            this.Frt,
            this.Term,
            this.Revenue,
            this.AComm,
            this.Brkg,
            this.FrtTax,
            this.LinerTerm});
            this.CargoView.FooterPanelHeight = 40;
            this.CargoView.GridControl = this.CargoGridControl;
            this.CargoView.HorzScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Never;
            this.CargoView.Name = "CargoView";
            this.CargoView.OptionsBehavior.AlignGroupSummaryInGroupRow = DevExpress.Utils.DefaultBoolean.False;
            this.CargoView.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.False;
            this.CargoView.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.False;
            this.CargoView.OptionsBehavior.AllowFixedGroups = DevExpress.Utils.DefaultBoolean.False;
            this.CargoView.OptionsBehavior.AllowPartialGroups = DevExpress.Utils.DefaultBoolean.False;
            this.CargoView.OptionsBehavior.AllowPartialRedrawOnScrolling = false;
            this.CargoView.OptionsBehavior.AllowPixelScrolling = DevExpress.Utils.DefaultBoolean.False;
            this.CargoView.OptionsCustomization.AllowBandMoving = false;
            this.CargoView.OptionsCustomization.AllowBandResizing = false;
            this.CargoView.OptionsCustomization.AllowColumnMoving = false;
            this.CargoView.OptionsCustomization.AllowColumnResizing = false;
            this.CargoView.OptionsCustomization.AllowGroup = false;
            this.CargoView.OptionsCustomization.AllowQuickHideColumns = false;
            this.CargoView.OptionsCustomization.AllowSort = false;
            this.CargoView.OptionsCustomization.ShowBandsInCustomizationForm = false;
            this.CargoView.OptionsSelection.InvertSelection = true;
            this.CargoView.OptionsSelection.UseIndicatorForSelection = false;
            this.CargoView.OptionsView.AllowHtmlDrawGroups = false;
            this.CargoView.OptionsView.ShowColumnHeaders = false;
            this.CargoView.OptionsView.ShowFooter = true;
            this.CargoView.OptionsView.ShowGroupExpandCollapseButtons = false;
            this.CargoView.OptionsView.ShowGroupPanel = false;
            this.CargoView.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.True;
            this.CargoView.OptionsView.ShowIndicator = false;
            this.CargoView.RowHeight = 30;
            this.CargoView.VertScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Never;
            this.CargoView.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.CargoView_CustomDrawCell);
            this.CargoView.CustomDrawFooterCell += new DevExpress.XtraGrid.Views.Grid.FooterCellCustomDrawEventHandler(this.CargoView_CustomDrawFooterCell);
            this.CargoView.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.CargoView_CustomRowCellEdit);
            this.CargoView.CellValueChanged += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.CargoView_CellValueChanged);
            this.CargoView.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CargoView_KeyDown);
            // 
            // gridBand17
            // 
            this.gridBand17.AppearanceHeader.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.gridBand17.AppearanceHeader.BackColor2 = System.Drawing.Color.Silver;
            this.gridBand17.AppearanceHeader.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.gridBand17.AppearanceHeader.Options.UseBackColor = true;
            this.gridBand17.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand17.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand17.Columns.Add(this.CargoIndex);
            this.gridBand17.Name = "gridBand17";
            this.gridBand17.OptionsBand.AllowHotTrack = false;
            this.gridBand17.OptionsBand.AllowMove = false;
            this.gridBand17.OptionsBand.AllowPress = false;
            this.gridBand17.OptionsBand.AllowSize = false;
            this.gridBand17.OptionsBand.ShowInCustomizationForm = false;
            this.gridBand17.VisibleIndex = 0;
            this.gridBand17.Width = 25;
            // 
            // CargoIndex
            // 
            this.CargoIndex.AppearanceCell.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.CargoIndex.AppearanceCell.Options.UseBackColor = true;
            this.CargoIndex.AppearanceCell.Options.UseTextOptions = true;
            this.CargoIndex.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.CargoIndex.FieldName = "Index";
            this.CargoIndex.Name = "CargoIndex";
            this.CargoIndex.OptionsColumn.AllowEdit = false;
            this.CargoIndex.OptionsColumn.AllowFocus = false;
            this.CargoIndex.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
            this.CargoIndex.OptionsColumn.AllowIncrementalSearch = false;
            this.CargoIndex.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.CargoIndex.OptionsColumn.AllowMove = false;
            this.CargoIndex.OptionsColumn.AllowShowHide = false;
            this.CargoIndex.OptionsColumn.AllowSize = false;
            this.CargoIndex.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.CargoIndex.OptionsColumn.ImmediateUpdateRowPosition = DevExpress.Utils.DefaultBoolean.False;
            this.CargoIndex.OptionsColumn.Printable = DevExpress.Utils.DefaultBoolean.False;
            this.CargoIndex.OptionsColumn.ReadOnly = true;
            this.CargoIndex.OptionsColumn.ShowCaption = false;
            this.CargoIndex.OptionsColumn.ShowInCustomizationForm = false;
            this.CargoIndex.OptionsColumn.ShowInExpressionEditor = false;
            this.CargoIndex.OptionsColumn.TabStop = false;
            this.CargoIndex.Visible = true;
            this.CargoIndex.Width = 25;
            // 
            // gridBand18
            // 
            this.gridBand18.AppearanceHeader.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.gridBand18.AppearanceHeader.BackColor2 = System.Drawing.Color.Silver;
            this.gridBand18.AppearanceHeader.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.gridBand18.AppearanceHeader.Options.UseBackColor = true;
            this.gridBand18.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand18.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand18.Caption = "Account";
            this.gridBand18.Columns.Add(this.CargoAccount);
            this.gridBand18.Name = "gridBand18";
            this.gridBand18.OptionsBand.AllowHotTrack = false;
            this.gridBand18.OptionsBand.AllowMove = false;
            this.gridBand18.OptionsBand.AllowPress = false;
            this.gridBand18.OptionsBand.AllowSize = false;
            this.gridBand18.OptionsBand.ShowInCustomizationForm = false;
            this.gridBand18.VisibleIndex = 1;
            this.gridBand18.Width = 125;
            // 
            // CargoAccount
            // 
            this.CargoAccount.FieldName = "Account";
            this.CargoAccount.Name = "CargoAccount";
            this.CargoAccount.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
            this.CargoAccount.OptionsColumn.AllowIncrementalSearch = false;
            this.CargoAccount.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.CargoAccount.OptionsColumn.AllowMove = false;
            this.CargoAccount.OptionsColumn.AllowShowHide = false;
            this.CargoAccount.OptionsColumn.AllowSize = false;
            this.CargoAccount.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.CargoAccount.OptionsColumn.ImmediateUpdateRowPosition = DevExpress.Utils.DefaultBoolean.False;
            this.CargoAccount.OptionsColumn.Printable = DevExpress.Utils.DefaultBoolean.False;
            this.CargoAccount.OptionsColumn.ShowCaption = false;
            this.CargoAccount.OptionsColumn.ShowInCustomizationForm = false;
            this.CargoAccount.OptionsColumn.ShowInExpressionEditor = false;
            this.CargoAccount.Visible = true;
            this.CargoAccount.Width = 125;
            // 
            // gridBand19
            // 
            this.gridBand19.AppearanceHeader.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.gridBand19.AppearanceHeader.BackColor2 = System.Drawing.Color.Silver;
            this.gridBand19.AppearanceHeader.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.gridBand19.AppearanceHeader.Options.UseBackColor = true;
            this.gridBand19.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand19.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand19.Caption = "Cargo Name";
            this.gridBand19.Columns.Add(this.CargoHistory);
            this.gridBand19.Columns.Add(this.CargoName);
            this.gridBand19.Name = "gridBand19";
            this.gridBand19.OptionsBand.AllowHotTrack = false;
            this.gridBand19.OptionsBand.AllowMove = false;
            this.gridBand19.OptionsBand.AllowPress = false;
            this.gridBand19.OptionsBand.AllowSize = false;
            this.gridBand19.OptionsBand.ShowInCustomizationForm = false;
            this.gridBand19.VisibleIndex = 2;
            this.gridBand19.Width = 180;
            // 
            // CargoHistory
            // 
            this.CargoHistory.ColumnEdit = this.CargoHistoryBtn;
            this.CargoHistory.Name = "CargoHistory";
            this.CargoHistory.OptionsColumn.AllowEdit = false;
            this.CargoHistory.OptionsColumn.AllowFocus = false;
            this.CargoHistory.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
            this.CargoHistory.OptionsColumn.AllowIncrementalSearch = false;
            this.CargoHistory.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.CargoHistory.OptionsColumn.AllowMove = false;
            this.CargoHistory.OptionsColumn.AllowShowHide = false;
            this.CargoHistory.OptionsColumn.AllowSize = false;
            this.CargoHistory.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.CargoHistory.OptionsColumn.ImmediateUpdateRowPosition = DevExpress.Utils.DefaultBoolean.False;
            this.CargoHistory.OptionsColumn.Printable = DevExpress.Utils.DefaultBoolean.False;
            this.CargoHistory.OptionsColumn.ShowCaption = false;
            this.CargoHistory.OptionsColumn.ShowInCustomizationForm = false;
            this.CargoHistory.OptionsColumn.ShowInExpressionEditor = false;
            this.CargoHistory.OptionsColumn.TabStop = false;
            this.CargoHistory.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways;
            this.CargoHistory.Visible = true;
            this.CargoHistory.Width = 29;
            // 
            // CargoHistoryBtn
            // 
            this.CargoHistoryBtn.AllowFocused = false;
            this.CargoHistoryBtn.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            serializableAppearanceObject9.BackColor = System.Drawing.Color.Transparent;
            serializableAppearanceObject9.Options.UseBackColor = true;
            this.CargoHistoryBtn.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", 23, true, true, false, editorButtonImageOptions3, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject9, serializableAppearanceObject10, serializableAppearanceObject11, serializableAppearanceObject12, "", null, null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.CargoHistoryBtn.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.CargoHistoryBtn.Name = "CargoHistoryBtn";
            // 
            // CargoName
            // 
            this.CargoName.FieldName = "Name";
            this.CargoName.Name = "CargoName";
            this.CargoName.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
            this.CargoName.OptionsColumn.AllowIncrementalSearch = false;
            this.CargoName.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.CargoName.OptionsColumn.AllowMove = false;
            this.CargoName.OptionsColumn.AllowShowHide = false;
            this.CargoName.OptionsColumn.AllowSize = false;
            this.CargoName.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.CargoName.OptionsColumn.ImmediateUpdateRowPosition = DevExpress.Utils.DefaultBoolean.False;
            this.CargoName.OptionsColumn.Printable = DevExpress.Utils.DefaultBoolean.False;
            this.CargoName.OptionsColumn.ShowInCustomizationForm = false;
            this.CargoName.OptionsColumn.ShowInExpressionEditor = false;
            this.CargoName.OptionsColumn.TabStop = false;
            this.CargoName.Visible = true;
            this.CargoName.Width = 151;
            // 
            // gridBand20
            // 
            this.gridBand20.AppearanceHeader.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.gridBand20.AppearanceHeader.BackColor2 = System.Drawing.Color.Silver;
            this.gridBand20.AppearanceHeader.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.gridBand20.AppearanceHeader.Options.UseBackColor = true;
            this.gridBand20.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand20.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand20.Caption = "Loading Port";
            this.gridBand20.Columns.Add(this.LoadingPort);
            this.gridBand20.Name = "gridBand20";
            this.gridBand20.OptionsBand.AllowHotTrack = false;
            this.gridBand20.OptionsBand.AllowMove = false;
            this.gridBand20.OptionsBand.AllowPress = false;
            this.gridBand20.OptionsBand.AllowSize = false;
            this.gridBand20.OptionsBand.ShowInCustomizationForm = false;
            this.gridBand20.VisibleIndex = 3;
            this.gridBand20.Width = 170;
            // 
            // LoadingPort
            // 
            this.LoadingPort.FieldName = "LoadingPort";
            this.LoadingPort.Name = "LoadingPort";
            this.LoadingPort.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
            this.LoadingPort.OptionsColumn.AllowIncrementalSearch = false;
            this.LoadingPort.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.LoadingPort.OptionsColumn.AllowMove = false;
            this.LoadingPort.OptionsColumn.AllowShowHide = false;
            this.LoadingPort.OptionsColumn.AllowSize = false;
            this.LoadingPort.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.LoadingPort.OptionsColumn.ImmediateUpdateRowPosition = DevExpress.Utils.DefaultBoolean.False;
            this.LoadingPort.OptionsColumn.Printable = DevExpress.Utils.DefaultBoolean.False;
            this.LoadingPort.OptionsColumn.ShowCaption = false;
            this.LoadingPort.OptionsColumn.ShowInCustomizationForm = false;
            this.LoadingPort.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowOnlyInEditor;
            this.LoadingPort.Visible = true;
            this.LoadingPort.Width = 170;
            // 
            // gridBand21
            // 
            this.gridBand21.AppearanceHeader.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.gridBand21.AppearanceHeader.BackColor2 = System.Drawing.Color.Silver;
            this.gridBand21.AppearanceHeader.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.gridBand21.AppearanceHeader.Options.UseBackColor = true;
            this.gridBand21.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand21.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand21.Caption = "Discharging Port";
            this.gridBand21.Columns.Add(this.DischargingPort);
            this.gridBand21.Name = "gridBand21";
            this.gridBand21.OptionsBand.AllowHotTrack = false;
            this.gridBand21.OptionsBand.AllowMove = false;
            this.gridBand21.OptionsBand.AllowPress = false;
            this.gridBand21.OptionsBand.AllowSize = false;
            this.gridBand21.OptionsBand.ShowInCustomizationForm = false;
            this.gridBand21.VisibleIndex = 4;
            this.gridBand21.Width = 170;
            // 
            // DischargingPort
            // 
            this.DischargingPort.FieldName = "DischargingPort";
            this.DischargingPort.Name = "DischargingPort";
            this.DischargingPort.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
            this.DischargingPort.OptionsColumn.AllowIncrementalSearch = false;
            this.DischargingPort.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.DischargingPort.OptionsColumn.AllowMove = false;
            this.DischargingPort.OptionsColumn.AllowShowHide = false;
            this.DischargingPort.OptionsColumn.AllowSize = false;
            this.DischargingPort.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.DischargingPort.OptionsColumn.Printable = DevExpress.Utils.DefaultBoolean.False;
            this.DischargingPort.OptionsColumn.ShowCaption = false;
            this.DischargingPort.OptionsColumn.ShowInCustomizationForm = false;
            this.DischargingPort.OptionsColumn.ShowInExpressionEditor = false;
            this.DischargingPort.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways;
            this.DischargingPort.Visible = true;
            this.DischargingPort.Width = 170;
            // 
            // gridBand22
            // 
            this.gridBand22.AppearanceHeader.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.gridBand22.AppearanceHeader.BackColor2 = System.Drawing.Color.Silver;
            this.gridBand22.AppearanceHeader.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.gridBand22.AppearanceHeader.Options.UseBackColor = true;
            this.gridBand22.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand22.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand22.Caption = "Qunatity";
            this.gridBand22.Columns.Add(this.Quantity);
            this.gridBand22.Columns.Add(this.QuantityTerm);
            this.gridBand22.Name = "gridBand22";
            this.gridBand22.OptionsBand.AllowHotTrack = false;
            this.gridBand22.OptionsBand.AllowMove = false;
            this.gridBand22.OptionsBand.AllowPress = false;
            this.gridBand22.OptionsBand.AllowSize = false;
            this.gridBand22.OptionsBand.ShowInCustomizationForm = false;
            this.gridBand22.VisibleIndex = 5;
            this.gridBand22.Width = 190;
            // 
            // Quantity
            // 
            this.Quantity.AppearanceCell.Options.UseTextOptions = true;
            this.Quantity.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.Quantity.DisplayFormat.FormatString = "{0:#,##0.000}";
            this.Quantity.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.Quantity.FieldName = "Quantity";
            this.Quantity.Name = "Quantity";
            this.Quantity.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Quantity", "{0:#,##0.000}")});
            this.Quantity.Visible = true;
            this.Quantity.Width = 138;
            // 
            // QuantityTerm
            // 
            this.QuantityTerm.AppearanceCell.Options.UseTextOptions = true;
            this.QuantityTerm.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.QuantityTerm.ColumnEdit = this.QuantityTermComboBox;
            this.QuantityTerm.FieldName = "QuantityTerm";
            this.QuantityTerm.Name = "QuantityTerm";
            this.QuantityTerm.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowOnlyInEditor;
            this.QuantityTerm.Visible = true;
            this.QuantityTerm.Width = 52;
            // 
            // QuantityTermComboBox
            // 
            this.QuantityTermComboBox.AutoHeight = false;
            this.QuantityTermComboBox.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.QuantityTermComboBox.Items.AddRange(new object[] {
            "CBM",
            "EA",
            "KL",
            "LT",
            "MT",
            "RT",
            "TON",
            "UNIT",
            "Barrel"});
            this.QuantityTermComboBox.Name = "QuantityTermComboBox";
            // 
            // gridBand23
            // 
            this.gridBand23.AppearanceHeader.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.gridBand23.AppearanceHeader.BackColor2 = System.Drawing.Color.Silver;
            this.gridBand23.AppearanceHeader.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.gridBand23.AppearanceHeader.Options.UseBackColor = true;
            this.gridBand23.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand23.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand23.Caption = "Frt";
            this.gridBand23.Columns.Add(this.Frt);
            this.gridBand23.Name = "gridBand23";
            this.gridBand23.OptionsBand.AllowHotTrack = false;
            this.gridBand23.OptionsBand.AllowMove = false;
            this.gridBand23.OptionsBand.AllowPress = false;
            this.gridBand23.OptionsBand.AllowSize = false;
            this.gridBand23.OptionsBand.ShowInCustomizationForm = false;
            this.gridBand23.VisibleIndex = 6;
            this.gridBand23.Width = 100;
            // 
            // Frt
            // 
            this.Frt.AppearanceCell.Options.UseTextOptions = true;
            this.Frt.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.Frt.DisplayFormat.FormatString = "{0:#,##0.000}";
            this.Frt.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.Frt.FieldName = "Frt";
            this.Frt.Name = "Frt";
            this.Frt.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Custom, "Frt", "{0:#,##0.000}")});
            this.Frt.Visible = true;
            this.Frt.Width = 100;
            // 
            // gridBand24
            // 
            this.gridBand24.AppearanceHeader.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.gridBand24.AppearanceHeader.BackColor2 = System.Drawing.Color.Silver;
            this.gridBand24.AppearanceHeader.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.gridBand24.AppearanceHeader.Options.UseBackColor = true;
            this.gridBand24.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand24.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand24.Caption = "Term";
            this.gridBand24.Columns.Add(this.Term);
            this.gridBand24.Name = "gridBand24";
            this.gridBand24.OptionsBand.AllowHotTrack = false;
            this.gridBand24.OptionsBand.AllowMove = false;
            this.gridBand24.OptionsBand.AllowPress = false;
            this.gridBand24.OptionsBand.AllowSize = false;
            this.gridBand24.OptionsBand.ShowInCustomizationForm = false;
            this.gridBand24.VisibleIndex = 7;
            this.gridBand24.Width = 90;
            // 
            // Term
            // 
            this.Term.AppearanceCell.Options.UseTextOptions = true;
            this.Term.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Term.ColumnEdit = this.TermComboBox;
            this.Term.FieldName = "Term";
            this.Term.Name = "Term";
            this.Term.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowOnlyInEditor;
            this.Term.Visible = true;
            this.Term.Width = 90;
            // 
            // TermComboBox
            // 
            this.TermComboBox.AutoHeight = false;
            this.TermComboBox.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.TermComboBox.Items.AddRange(new object[] {
            "FIO",
            "FILO",
            "LIFO",
            "BTBT"});
            this.TermComboBox.Name = "TermComboBox";
            // 
            // gridBand25
            // 
            this.gridBand25.AppearanceHeader.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.gridBand25.AppearanceHeader.BackColor2 = System.Drawing.Color.Silver;
            this.gridBand25.AppearanceHeader.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.gridBand25.AppearanceHeader.Options.UseBackColor = true;
            this.gridBand25.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand25.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand25.Caption = "Revenue";
            this.gridBand25.Columns.Add(this.Revenue);
            this.gridBand25.Name = "gridBand25";
            this.gridBand25.OptionsBand.AllowHotTrack = false;
            this.gridBand25.OptionsBand.AllowMove = false;
            this.gridBand25.OptionsBand.AllowPress = false;
            this.gridBand25.OptionsBand.AllowSize = false;
            this.gridBand25.OptionsBand.ShowInCustomizationForm = false;
            this.gridBand25.VisibleIndex = 8;
            this.gridBand25.Width = 140;
            // 
            // Revenue
            // 
            this.Revenue.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.Revenue.AppearanceCell.Options.UseBackColor = true;
            this.Revenue.AppearanceCell.Options.UseTextOptions = true;
            this.Revenue.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.Revenue.DisplayFormat.FormatString = "{0:#,##0.000}";
            this.Revenue.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.Revenue.FieldName = "Revenue";
            this.Revenue.Name = "Revenue";
            this.Revenue.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Revenue", "{0:#,##0.000}")});
            this.Revenue.Visible = true;
            this.Revenue.Width = 140;
            // 
            // gridBand26
            // 
            this.gridBand26.AppearanceHeader.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.gridBand26.AppearanceHeader.BackColor2 = System.Drawing.Color.Silver;
            this.gridBand26.AppearanceHeader.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.gridBand26.AppearanceHeader.Options.UseBackColor = true;
            this.gridBand26.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand26.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand26.Caption = "A. Comm";
            this.gridBand26.Columns.Add(this.AComm);
            this.gridBand26.Name = "gridBand26";
            this.gridBand26.OptionsBand.AllowHotTrack = false;
            this.gridBand26.OptionsBand.AllowMove = false;
            this.gridBand26.OptionsBand.AllowPress = false;
            this.gridBand26.OptionsBand.AllowSize = false;
            this.gridBand26.OptionsBand.ShowInCustomizationForm = false;
            this.gridBand26.VisibleIndex = 9;
            this.gridBand26.Width = 100;
            // 
            // AComm
            // 
            this.AComm.AppearanceCell.Options.UseTextOptions = true;
            this.AComm.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.AComm.DisplayFormat.FormatString = "{0:##0.000}%";
            this.AComm.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.AComm.FieldName = "AComm";
            this.AComm.Name = "AComm";
            this.AComm.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Custom, "AComm", "{0:#0.000}%")});
            this.AComm.Visible = true;
            this.AComm.Width = 100;
            // 
            // gridBand27
            // 
            this.gridBand27.AppearanceHeader.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.gridBand27.AppearanceHeader.BackColor2 = System.Drawing.Color.Silver;
            this.gridBand27.AppearanceHeader.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.gridBand27.AppearanceHeader.Options.UseBackColor = true;
            this.gridBand27.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand27.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand27.Caption = "Brkg";
            this.gridBand27.Columns.Add(this.Brkg);
            this.gridBand27.Name = "gridBand27";
            this.gridBand27.OptionsBand.AllowHotTrack = false;
            this.gridBand27.OptionsBand.AllowMove = false;
            this.gridBand27.OptionsBand.AllowPress = false;
            this.gridBand27.OptionsBand.AllowSize = false;
            this.gridBand27.OptionsBand.ShowInCustomizationForm = false;
            this.gridBand27.VisibleIndex = 10;
            this.gridBand27.Width = 100;
            // 
            // Brkg
            // 
            this.Brkg.AppearanceCell.Options.UseTextOptions = true;
            this.Brkg.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.Brkg.DisplayFormat.FormatString = "{0:##0.000}%";
            this.Brkg.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.Brkg.FieldName = "Brkg";
            this.Brkg.Name = "Brkg";
            this.Brkg.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Custom, "Brkg", "{0:#0.000}%")});
            this.Brkg.Visible = true;
            this.Brkg.Width = 100;
            // 
            // gridBand28
            // 
            this.gridBand28.AppearanceHeader.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.gridBand28.AppearanceHeader.BackColor2 = System.Drawing.Color.Silver;
            this.gridBand28.AppearanceHeader.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.gridBand28.AppearanceHeader.Options.UseBackColor = true;
            this.gridBand28.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand28.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand28.Caption = "Frt Tax";
            this.gridBand28.Columns.Add(this.FrtTax);
            this.gridBand28.Name = "gridBand28";
            this.gridBand28.OptionsBand.AllowHotTrack = false;
            this.gridBand28.OptionsBand.AllowMove = false;
            this.gridBand28.OptionsBand.AllowPress = false;
            this.gridBand28.OptionsBand.AllowSize = false;
            this.gridBand28.OptionsBand.ShowInCustomizationForm = false;
            this.gridBand28.VisibleIndex = 11;
            this.gridBand28.Width = 100;
            // 
            // FrtTax
            // 
            this.FrtTax.AppearanceCell.Options.UseTextOptions = true;
            this.FrtTax.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.FrtTax.DisplayFormat.FormatString = "{0:##0.000}%";
            this.FrtTax.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.FrtTax.FieldName = "FrtTax";
            this.FrtTax.Name = "FrtTax";
            this.FrtTax.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Custom, "FrtTax", "{0:#0.000}%")});
            this.FrtTax.Visible = true;
            this.FrtTax.Width = 100;
            // 
            // gridBand29
            // 
            this.gridBand29.AppearanceHeader.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.gridBand29.AppearanceHeader.BackColor2 = System.Drawing.Color.Silver;
            this.gridBand29.AppearanceHeader.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.gridBand29.AppearanceHeader.Options.UseBackColor = true;
            this.gridBand29.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand29.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand29.Caption = "Liner Term";
            this.gridBand29.Columns.Add(this.LinerTerm);
            this.gridBand29.Name = "gridBand29";
            this.gridBand29.OptionsBand.AllowHotTrack = false;
            this.gridBand29.OptionsBand.AllowMove = false;
            this.gridBand29.OptionsBand.AllowPress = false;
            this.gridBand29.OptionsBand.AllowSize = false;
            this.gridBand29.OptionsBand.ShowInCustomizationForm = false;
            this.gridBand29.VisibleIndex = 12;
            this.gridBand29.Width = 145;
            // 
            // LinerTerm
            // 
            this.LinerTerm.AppearanceCell.Options.UseTextOptions = true;
            this.LinerTerm.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.LinerTerm.DisplayFormat.FormatString = "{0:#,##0.000}";
            this.LinerTerm.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.LinerTerm.FieldName = "LinerTerm";
            this.LinerTerm.Name = "LinerTerm";
            this.LinerTerm.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "LinerTerm", "{0:#,##0.000}")});
            this.LinerTerm.Visible = true;
            this.LinerTerm.Width = 145;
            // 
            // CargoLabel
            // 
            this.CargoLabel.AutoSize = true;
            this.CargoLabel.Dock = System.Windows.Forms.DockStyle.Top;
            this.CargoLabel.Font = new System.Drawing.Font("Leelawadee UI", 20F, System.Drawing.FontStyle.Bold);
            this.CargoLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(130)))), ((int)(((byte)(28)))), ((int)(((byte)(51)))));
            this.CargoLabel.Location = new System.Drawing.Point(0, 0);
            this.CargoLabel.Name = "CargoLabel";
            this.CargoLabel.Size = new System.Drawing.Size(94, 37);
            this.CargoLabel.TabIndex = 1;
            this.CargoLabel.Text = "Cargo";
            // 
            // VesselPanel
            // 
            this.VesselPanel.Controls.Add(this.FusselPanel);
            this.VesselPanel.Controls.Add(this.SpdPanel);
            this.VesselPanel.Controls.Add(this.MVPanel);
            this.VesselPanel.Controls.Add(this.VesselLabel);
            this.VesselPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.VesselPanel.Location = new System.Drawing.Point(0, 0);
            this.VesselPanel.Name = "VesselPanel";
            this.VesselPanel.Size = new System.Drawing.Size(1678, 195);
            this.VesselPanel.TabIndex = 0;
            // 
            // FusselPanel
            // 
            this.FusselPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.FusselPanel.Controls.Add(this.VesselDOControl);
            this.FusselPanel.Controls.Add(this.VesselFOControl);
            this.FusselPanel.Location = new System.Drawing.Point(768, 40);
            this.FusselPanel.Name = "FusselPanel";
            this.FusselPanel.Size = new System.Drawing.Size(888, 149);
            this.FusselPanel.TabIndex = 2;
            // 
            // VesselDOControl
            // 
            this.VesselDOControl.DataSource = this.vPDOBindingSource;
            this.VesselDOControl.Location = new System.Drawing.Point(495, 29);
            this.VesselDOControl.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat;
            this.VesselDOControl.LookAndFeel.UseDefaultLookAndFeel = false;
            this.VesselDOControl.MainView = this.VesselDOView;
            this.VesselDOControl.Name = "VesselDOControl";
            this.VesselDOControl.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.DoTypeComboBox});
            this.VesselDOControl.Size = new System.Drawing.Size(374, 96);
            this.VesselDOControl.TabIndex = 2;
            this.VesselDOControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.VesselDOView});
            this.VesselDOControl.EditorKeyDown += new System.Windows.Forms.KeyEventHandler(this.VesselDOControl_EditorKeyDown);
            // 
            // vPDOBindingSource
            // 
            this.vPDOBindingSource.DataMember = "VPDO";
            this.vPDOBindingSource.DataSource = this.sEDataBindingSource;
            // 
            // VesselDOView
            // 
            this.VesselDOView.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(120)))), ((int)(((byte)(177)))), ((int)(((byte)(145)))));
            this.VesselDOView.Appearance.FocusedRow.Options.UseBackColor = true;
            this.VesselDOView.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.Transparent;
            this.VesselDOView.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.VesselDOView.BandPanelRowHeight = 30;
            this.VesselDOView.Bands.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand8,
            this.gridBand13,
            this.gridBand15,
            this.gridBand16,
            this.gridBand59});
            this.VesselDOView.Columns.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn[] {
            this.LSDO,
            this.DOType,
            this.DOSea,
            this.DOIdle,
            this.DOWork});
            this.VesselDOView.GridControl = this.VesselDOControl;
            this.VesselDOView.HorzScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Never;
            this.VesselDOView.Name = "VesselDOView";
            this.VesselDOView.OptionsBehavior.AlignGroupSummaryInGroupRow = DevExpress.Utils.DefaultBoolean.False;
            this.VesselDOView.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.False;
            this.VesselDOView.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.False;
            this.VesselDOView.OptionsBehavior.AllowFixedGroups = DevExpress.Utils.DefaultBoolean.False;
            this.VesselDOView.OptionsBehavior.AllowPartialGroups = DevExpress.Utils.DefaultBoolean.False;
            this.VesselDOView.OptionsBehavior.AllowPartialRedrawOnScrolling = false;
            this.VesselDOView.OptionsBehavior.AllowPixelScrolling = DevExpress.Utils.DefaultBoolean.False;
            this.VesselDOView.OptionsCustomization.AllowBandMoving = false;
            this.VesselDOView.OptionsCustomization.AllowBandResizing = false;
            this.VesselDOView.OptionsCustomization.AllowColumnMoving = false;
            this.VesselDOView.OptionsCustomization.AllowColumnResizing = false;
            this.VesselDOView.OptionsCustomization.AllowGroup = false;
            this.VesselDOView.OptionsCustomization.AllowQuickHideColumns = false;
            this.VesselDOView.OptionsCustomization.AllowSort = false;
            this.VesselDOView.OptionsCustomization.ShowBandsInCustomizationForm = false;
            this.VesselDOView.OptionsSelection.InvertSelection = true;
            this.VesselDOView.OptionsSelection.UseIndicatorForSelection = false;
            this.VesselDOView.OptionsView.AllowHtmlDrawGroups = false;
            this.VesselDOView.OptionsView.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowOnlyInEditor;
            this.VesselDOView.OptionsView.ShowColumnHeaders = false;
            this.VesselDOView.OptionsView.ShowGroupExpandCollapseButtons = false;
            this.VesselDOView.OptionsView.ShowGroupPanel = false;
            this.VesselDOView.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.True;
            this.VesselDOView.OptionsView.ShowIndicator = false;
            this.VesselDOView.RowHeight = 30;
            this.VesselDOView.VertScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Never;
            this.VesselDOView.CellValueChanged += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.VesselDOView_CellValueChanged);
            this.VesselDOView.KeyDown += new System.Windows.Forms.KeyEventHandler(this.VesselDOView_KeyDown);
            // 
            // gridBand8
            // 
            this.gridBand8.AppearanceHeader.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.gridBand8.AppearanceHeader.BackColor2 = System.Drawing.Color.Silver;
            this.gridBand8.AppearanceHeader.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.gridBand8.AppearanceHeader.Options.UseBackColor = true;
            this.gridBand8.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand8.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand8.Columns.Add(this.LSDO);
            this.gridBand8.Name = "gridBand8";
            this.gridBand8.OptionsBand.AllowHotTrack = false;
            this.gridBand8.OptionsBand.AllowMove = false;
            this.gridBand8.OptionsBand.AllowPress = false;
            this.gridBand8.OptionsBand.AllowSize = false;
            this.gridBand8.OptionsBand.ShowInCustomizationForm = false;
            this.gridBand8.VisibleIndex = 0;
            this.gridBand8.Width = 50;
            // 
            // LSDO
            // 
            this.LSDO.AppearanceCell.BackColor = System.Drawing.Color.DarkGray;
            this.LSDO.AppearanceCell.Options.UseBackColor = true;
            this.LSDO.AppearanceCell.Options.UseTextOptions = true;
            this.LSDO.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.LSDO.FieldName = "LSDO";
            this.LSDO.Name = "LSDO";
            this.LSDO.OptionsColumn.AllowEdit = false;
            this.LSDO.OptionsColumn.AllowFocus = false;
            this.LSDO.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
            this.LSDO.OptionsColumn.AllowIncrementalSearch = false;
            this.LSDO.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.LSDO.OptionsColumn.AllowMove = false;
            this.LSDO.OptionsColumn.AllowShowHide = false;
            this.LSDO.OptionsColumn.AllowSize = false;
            this.LSDO.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.LSDO.OptionsColumn.ReadOnly = true;
            this.LSDO.OptionsColumn.ShowCaption = false;
            this.LSDO.OptionsColumn.ShowInCustomizationForm = false;
            this.LSDO.OptionsColumn.ShowInExpressionEditor = false;
            this.LSDO.OptionsColumn.TabStop = false;
            this.LSDO.Visible = true;
            this.LSDO.Width = 50;
            // 
            // gridBand13
            // 
            this.gridBand13.AppearanceHeader.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.gridBand13.AppearanceHeader.BackColor2 = System.Drawing.Color.Silver;
            this.gridBand13.AppearanceHeader.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.gridBand13.AppearanceHeader.Options.UseBackColor = true;
            this.gridBand13.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand13.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand13.Caption = "Type";
            this.gridBand13.Columns.Add(this.DOType);
            this.gridBand13.Name = "gridBand13";
            this.gridBand13.OptionsBand.AllowHotTrack = false;
            this.gridBand13.OptionsBand.AllowMove = false;
            this.gridBand13.OptionsBand.AllowPress = false;
            this.gridBand13.OptionsBand.AllowSize = false;
            this.gridBand13.OptionsBand.ShowInCustomizationForm = false;
            this.gridBand13.VisibleIndex = 1;
            this.gridBand13.Width = 80;
            // 
            // DOType
            // 
            this.DOType.ColumnEdit = this.DoTypeComboBox;
            this.DOType.DisplayFormat.FormatString = "{0:#,##0.00}";
            this.DOType.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.DOType.FieldName = "Type";
            this.DOType.Name = "DOType";
            this.DOType.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowOnlyInEditor;
            this.DOType.Visible = true;
            this.DOType.Width = 80;
            // 
            // DoTypeComboBox
            // 
            this.DoTypeComboBox.AutoHeight = false;
            this.DoTypeComboBox.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DoTypeComboBox.Items.AddRange(new object[] {
            "DMA",
            "DMB",
            "DMC",
            "DMX",
            "MGO"});
            this.DoTypeComboBox.Name = "DoTypeComboBox";
            // 
            // gridBand15
            // 
            this.gridBand15.AppearanceHeader.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.gridBand15.AppearanceHeader.BackColor2 = System.Drawing.Color.Silver;
            this.gridBand15.AppearanceHeader.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.gridBand15.AppearanceHeader.Options.UseBackColor = true;
            this.gridBand15.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand15.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand15.Caption = "Sea";
            this.gridBand15.Columns.Add(this.DOSea);
            this.gridBand15.Name = "gridBand15";
            this.gridBand15.OptionsBand.AllowHotTrack = false;
            this.gridBand15.OptionsBand.AllowMove = false;
            this.gridBand15.OptionsBand.AllowPress = false;
            this.gridBand15.OptionsBand.AllowSize = false;
            this.gridBand15.OptionsBand.ShowInCustomizationForm = false;
            this.gridBand15.VisibleIndex = 2;
            this.gridBand15.Width = 80;
            // 
            // DOSea
            // 
            this.DOSea.DisplayFormat.FormatString = "{0:#,##0.00}";
            this.DOSea.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.DOSea.FieldName = "Sea";
            this.DOSea.Name = "DOSea";
            this.DOSea.Visible = true;
            this.DOSea.Width = 80;
            // 
            // gridBand16
            // 
            this.gridBand16.AppearanceHeader.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.gridBand16.AppearanceHeader.BackColor2 = System.Drawing.Color.Silver;
            this.gridBand16.AppearanceHeader.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.gridBand16.AppearanceHeader.Options.UseBackColor = true;
            this.gridBand16.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand16.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand16.Caption = "Idle";
            this.gridBand16.Columns.Add(this.DOIdle);
            this.gridBand16.Name = "gridBand16";
            this.gridBand16.VisibleIndex = 3;
            this.gridBand16.Width = 80;
            // 
            // DOIdle
            // 
            this.DOIdle.DisplayFormat.FormatString = "{0:#,##0.00}";
            this.DOIdle.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.DOIdle.FieldName = "Idle";
            this.DOIdle.Name = "DOIdle";
            this.DOIdle.Visible = true;
            this.DOIdle.Width = 80;
            // 
            // gridBand59
            // 
            this.gridBand59.AppearanceHeader.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.gridBand59.AppearanceHeader.BackColor2 = System.Drawing.Color.Silver;
            this.gridBand59.AppearanceHeader.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.gridBand59.AppearanceHeader.Options.UseBackColor = true;
            this.gridBand59.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand59.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand59.Caption = "Work";
            this.gridBand59.Columns.Add(this.DOWork);
            this.gridBand59.Name = "gridBand59";
            this.gridBand59.VisibleIndex = 4;
            this.gridBand59.Width = 80;
            // 
            // DOWork
            // 
            this.DOWork.DisplayFormat.FormatString = "{0:#,##0.00}";
            this.DOWork.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.DOWork.FieldName = "Work";
            this.DOWork.Name = "DOWork";
            this.DOWork.Visible = true;
            this.DOWork.Width = 80;
            // 
            // VesselFOControl
            // 
            this.VesselFOControl.DataSource = this.vPFOBindingSource;
            this.VesselFOControl.Location = new System.Drawing.Point(22, 29);
            this.VesselFOControl.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat;
            this.VesselFOControl.LookAndFeel.UseDefaultLookAndFeel = false;
            this.VesselFOControl.MainView = this.VesselFOView;
            this.VesselFOControl.Name = "VesselFOControl";
            this.VesselFOControl.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemComboBox1});
            this.VesselFOControl.Size = new System.Drawing.Size(454, 96);
            this.VesselFOControl.TabIndex = 1;
            this.VesselFOControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.VesselFOView});
            this.VesselFOControl.EditorKeyDown += new System.Windows.Forms.KeyEventHandler(this.VesselFOControl_EditorKeyDown);
            // 
            // vPFOBindingSource
            // 
            this.vPFOBindingSource.DataMember = "VPFO";
            this.vPFOBindingSource.DataSource = this.sEDataBindingSource;
            // 
            // VesselFOView
            // 
            this.VesselFOView.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(120)))), ((int)(((byte)(177)))), ((int)(((byte)(145)))));
            this.VesselFOView.Appearance.FocusedRow.Options.UseBackColor = true;
            this.VesselFOView.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.Transparent;
            this.VesselFOView.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.VesselFOView.BandPanelRowHeight = 30;
            this.VesselFOView.Bands.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand9,
            this.gridBand10,
            this.gridBand11,
            this.gridBand12,
            this.gridBand1,
            this.gridBand7});
            this.VesselFOView.Columns.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn[] {
            this.LSFO,
            this.FOType,
            this.FOBallast,
            this.FOLaden,
            this.FOIdle,
            this.FOWork});
            this.VesselFOView.GridControl = this.VesselFOControl;
            this.VesselFOView.HorzScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Never;
            this.VesselFOView.Name = "VesselFOView";
            this.VesselFOView.OptionsBehavior.AlignGroupSummaryInGroupRow = DevExpress.Utils.DefaultBoolean.False;
            this.VesselFOView.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.False;
            this.VesselFOView.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.False;
            this.VesselFOView.OptionsBehavior.AllowFixedGroups = DevExpress.Utils.DefaultBoolean.False;
            this.VesselFOView.OptionsBehavior.AllowPartialGroups = DevExpress.Utils.DefaultBoolean.False;
            this.VesselFOView.OptionsBehavior.AllowPartialRedrawOnScrolling = false;
            this.VesselFOView.OptionsBehavior.AllowPixelScrolling = DevExpress.Utils.DefaultBoolean.False;
            this.VesselFOView.OptionsCustomization.AllowBandMoving = false;
            this.VesselFOView.OptionsCustomization.AllowBandResizing = false;
            this.VesselFOView.OptionsCustomization.AllowColumnMoving = false;
            this.VesselFOView.OptionsCustomization.AllowColumnResizing = false;
            this.VesselFOView.OptionsCustomization.AllowGroup = false;
            this.VesselFOView.OptionsCustomization.AllowQuickHideColumns = false;
            this.VesselFOView.OptionsCustomization.AllowSort = false;
            this.VesselFOView.OptionsCustomization.ShowBandsInCustomizationForm = false;
            this.VesselFOView.OptionsSelection.InvertSelection = true;
            this.VesselFOView.OptionsSelection.UseIndicatorForSelection = false;
            this.VesselFOView.OptionsView.AllowHtmlDrawGroups = false;
            this.VesselFOView.OptionsView.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowOnlyInEditor;
            this.VesselFOView.OptionsView.ShowColumnHeaders = false;
            this.VesselFOView.OptionsView.ShowGroupExpandCollapseButtons = false;
            this.VesselFOView.OptionsView.ShowGroupPanel = false;
            this.VesselFOView.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.True;
            this.VesselFOView.OptionsView.ShowIndicator = false;
            this.VesselFOView.RowHeight = 30;
            this.VesselFOView.VertScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Never;
            this.VesselFOView.CellValueChanged += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.VesselFOView_CellValueChanged);
            this.VesselFOView.KeyDown += new System.Windows.Forms.KeyEventHandler(this.VesselFOView_KeyDown);
            // 
            // gridBand9
            // 
            this.gridBand9.AppearanceHeader.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.gridBand9.AppearanceHeader.BackColor2 = System.Drawing.Color.Silver;
            this.gridBand9.AppearanceHeader.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.gridBand9.AppearanceHeader.Options.UseBackColor = true;
            this.gridBand9.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand9.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand9.Columns.Add(this.LSFO);
            this.gridBand9.Name = "gridBand9";
            this.gridBand9.OptionsBand.AllowHotTrack = false;
            this.gridBand9.OptionsBand.AllowMove = false;
            this.gridBand9.OptionsBand.AllowPress = false;
            this.gridBand9.OptionsBand.AllowSize = false;
            this.gridBand9.OptionsBand.ShowInCustomizationForm = false;
            this.gridBand9.VisibleIndex = 0;
            this.gridBand9.Width = 50;
            // 
            // LSFO
            // 
            this.LSFO.AppearanceCell.BackColor = System.Drawing.Color.DarkGray;
            this.LSFO.AppearanceCell.Options.UseBackColor = true;
            this.LSFO.AppearanceCell.Options.UseTextOptions = true;
            this.LSFO.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.LSFO.FieldName = "LSFO";
            this.LSFO.Name = "LSFO";
            this.LSFO.OptionsColumn.AllowEdit = false;
            this.LSFO.OptionsColumn.AllowFocus = false;
            this.LSFO.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
            this.LSFO.OptionsColumn.AllowIncrementalSearch = false;
            this.LSFO.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.LSFO.OptionsColumn.AllowMove = false;
            this.LSFO.OptionsColumn.AllowShowHide = false;
            this.LSFO.OptionsColumn.AllowSize = false;
            this.LSFO.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.LSFO.OptionsColumn.ReadOnly = true;
            this.LSFO.OptionsColumn.ShowCaption = false;
            this.LSFO.OptionsColumn.ShowInCustomizationForm = false;
            this.LSFO.OptionsColumn.ShowInExpressionEditor = false;
            this.LSFO.OptionsColumn.TabStop = false;
            this.LSFO.Visible = true;
            this.LSFO.Width = 50;
            // 
            // gridBand10
            // 
            this.gridBand10.AppearanceHeader.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.gridBand10.AppearanceHeader.BackColor2 = System.Drawing.Color.Silver;
            this.gridBand10.AppearanceHeader.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.gridBand10.AppearanceHeader.Options.UseBackColor = true;
            this.gridBand10.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand10.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand10.Caption = "Type";
            this.gridBand10.Columns.Add(this.FOType);
            this.gridBand10.Name = "gridBand10";
            this.gridBand10.OptionsBand.AllowHotTrack = false;
            this.gridBand10.OptionsBand.AllowMove = false;
            this.gridBand10.OptionsBand.AllowPress = false;
            this.gridBand10.OptionsBand.AllowSize = false;
            this.gridBand10.OptionsBand.ShowInCustomizationForm = false;
            this.gridBand10.VisibleIndex = 1;
            this.gridBand10.Width = 80;
            // 
            // FOType
            // 
            this.FOType.ColumnEdit = this.repositoryItemComboBox1;
            this.FOType.FieldName = "Type";
            this.FOType.Name = "FOType";
            this.FOType.Visible = true;
            this.FOType.Width = 80;
            // 
            // repositoryItemComboBox1
            // 
            this.repositoryItemComboBox1.AutoHeight = false;
            this.repositoryItemComboBox1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox1.Items.AddRange(new object[] {
            "380",
            "280",
            "180",
            "120",
            "80",
            "60",
            "40",
            "30"});
            this.repositoryItemComboBox1.Name = "repositoryItemComboBox1";
            // 
            // gridBand11
            // 
            this.gridBand11.AppearanceHeader.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.gridBand11.AppearanceHeader.BackColor2 = System.Drawing.Color.Silver;
            this.gridBand11.AppearanceHeader.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.gridBand11.AppearanceHeader.Options.UseBackColor = true;
            this.gridBand11.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand11.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand11.Caption = "Ballast";
            this.gridBand11.Columns.Add(this.FOBallast);
            this.gridBand11.Name = "gridBand11";
            this.gridBand11.OptionsBand.AllowHotTrack = false;
            this.gridBand11.OptionsBand.AllowMove = false;
            this.gridBand11.OptionsBand.AllowPress = false;
            this.gridBand11.OptionsBand.AllowSize = false;
            this.gridBand11.OptionsBand.ShowInCustomizationForm = false;
            this.gridBand11.VisibleIndex = 2;
            this.gridBand11.Width = 80;
            // 
            // FOBallast
            // 
            this.FOBallast.DisplayFormat.FormatString = "{0:#,##0.00}";
            this.FOBallast.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.FOBallast.FieldName = "Ballast";
            this.FOBallast.Name = "FOBallast";
            this.FOBallast.Visible = true;
            this.FOBallast.Width = 80;
            // 
            // gridBand12
            // 
            this.gridBand12.AppearanceHeader.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.gridBand12.AppearanceHeader.BackColor2 = System.Drawing.Color.Silver;
            this.gridBand12.AppearanceHeader.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.gridBand12.AppearanceHeader.Options.UseBackColor = true;
            this.gridBand12.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand12.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand12.Caption = "Laden";
            this.gridBand12.Columns.Add(this.FOLaden);
            this.gridBand12.Name = "gridBand12";
            this.gridBand12.OptionsBand.AllowHotTrack = false;
            this.gridBand12.OptionsBand.AllowMove = false;
            this.gridBand12.OptionsBand.AllowPress = false;
            this.gridBand12.OptionsBand.AllowSize = false;
            this.gridBand12.OptionsBand.ShowInCustomizationForm = false;
            this.gridBand12.VisibleIndex = 3;
            this.gridBand12.Width = 80;
            // 
            // FOLaden
            // 
            this.FOLaden.DisplayFormat.FormatString = "{0:#,##0.00}";
            this.FOLaden.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.FOLaden.FieldName = "Laden";
            this.FOLaden.Name = "FOLaden";
            this.FOLaden.Visible = true;
            this.FOLaden.Width = 80;
            // 
            // gridBand1
            // 
            this.gridBand1.AppearanceHeader.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.gridBand1.AppearanceHeader.BackColor2 = System.Drawing.Color.Silver;
            this.gridBand1.AppearanceHeader.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.gridBand1.AppearanceHeader.Options.UseBackColor = true;
            this.gridBand1.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand1.Caption = "Idle";
            this.gridBand1.Columns.Add(this.FOIdle);
            this.gridBand1.Name = "gridBand1";
            this.gridBand1.VisibleIndex = 4;
            this.gridBand1.Width = 80;
            // 
            // FOIdle
            // 
            this.FOIdle.DisplayFormat.FormatString = "{0:#,##0.00}";
            this.FOIdle.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.FOIdle.FieldName = "Idle";
            this.FOIdle.Name = "FOIdle";
            this.FOIdle.Visible = true;
            this.FOIdle.Width = 80;
            // 
            // gridBand7
            // 
            this.gridBand7.AppearanceHeader.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.gridBand7.AppearanceHeader.BackColor2 = System.Drawing.Color.Silver;
            this.gridBand7.AppearanceHeader.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.gridBand7.AppearanceHeader.Options.UseBackColor = true;
            this.gridBand7.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand7.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand7.Caption = "Work";
            this.gridBand7.Columns.Add(this.FOWork);
            this.gridBand7.Name = "gridBand7";
            this.gridBand7.VisibleIndex = 5;
            this.gridBand7.Width = 80;
            // 
            // FOWork
            // 
            this.FOWork.DisplayFormat.FormatString = "{0:#,##0.00}";
            this.FOWork.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.FOWork.FieldName = "Work";
            this.FOWork.Name = "FOWork";
            this.FOWork.Visible = true;
            this.FOWork.Width = 80;
            // 
            // SpdPanel
            // 
            this.SpdPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.SpdPanel.Controls.Add(this.VesselSpdControl);
            this.SpdPanel.Location = new System.Drawing.Point(506, 40);
            this.SpdPanel.Name = "SpdPanel";
            this.SpdPanel.Size = new System.Drawing.Size(256, 149);
            this.SpdPanel.TabIndex = 2;
            // 
            // VesselSpdControl
            // 
            this.VesselSpdControl.DataSource = this.vPSpdBindingSource;
            this.VesselSpdControl.Location = new System.Drawing.Point(69, 43);
            this.VesselSpdControl.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat;
            this.VesselSpdControl.LookAndFeel.UseDefaultLookAndFeel = false;
            this.VesselSpdControl.MainView = this.VesselSpdView;
            this.VesselSpdControl.Name = "VesselSpdControl";
            this.VesselSpdControl.Size = new System.Drawing.Size(164, 65);
            this.VesselSpdControl.TabIndex = 1;
            this.VesselSpdControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.VesselSpdView});
            this.VesselSpdControl.EditorKeyDown += new System.Windows.Forms.KeyEventHandler(this.VesselSpdControl_EditorKeyDown);
            // 
            // vPSpdBindingSource
            // 
            this.vPSpdBindingSource.DataMember = "VPSpd";
            this.vPSpdBindingSource.DataSource = this.sEDataBindingSource;
            // 
            // VesselSpdView
            // 
            this.VesselSpdView.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(120)))), ((int)(((byte)(177)))), ((int)(((byte)(145)))));
            this.VesselSpdView.Appearance.FocusedRow.Options.UseBackColor = true;
            this.VesselSpdView.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.Transparent;
            this.VesselSpdView.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.VesselSpdView.BandPanelRowHeight = 30;
            this.VesselSpdView.Bands.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand5,
            this.gridBand6});
            this.VesselSpdView.Columns.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn[] {
            this.BallastSpd,
            this.LadenSpd});
            this.VesselSpdView.GridControl = this.VesselSpdControl;
            this.VesselSpdView.HorzScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Never;
            this.VesselSpdView.Name = "VesselSpdView";
            this.VesselSpdView.OptionsBehavior.AlignGroupSummaryInGroupRow = DevExpress.Utils.DefaultBoolean.False;
            this.VesselSpdView.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.False;
            this.VesselSpdView.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.False;
            this.VesselSpdView.OptionsBehavior.AllowFixedGroups = DevExpress.Utils.DefaultBoolean.False;
            this.VesselSpdView.OptionsBehavior.AllowPartialGroups = DevExpress.Utils.DefaultBoolean.False;
            this.VesselSpdView.OptionsBehavior.AllowPartialRedrawOnScrolling = false;
            this.VesselSpdView.OptionsBehavior.AllowPixelScrolling = DevExpress.Utils.DefaultBoolean.False;
            this.VesselSpdView.OptionsCustomization.AllowBandMoving = false;
            this.VesselSpdView.OptionsCustomization.AllowBandResizing = false;
            this.VesselSpdView.OptionsCustomization.AllowColumnMoving = false;
            this.VesselSpdView.OptionsCustomization.AllowColumnResizing = false;
            this.VesselSpdView.OptionsCustomization.AllowGroup = false;
            this.VesselSpdView.OptionsCustomization.AllowQuickHideColumns = false;
            this.VesselSpdView.OptionsCustomization.AllowSort = false;
            this.VesselSpdView.OptionsCustomization.ShowBandsInCustomizationForm = false;
            this.VesselSpdView.OptionsSelection.InvertSelection = true;
            this.VesselSpdView.OptionsSelection.UseIndicatorForSelection = false;
            this.VesselSpdView.OptionsView.AllowHtmlDrawGroups = false;
            this.VesselSpdView.OptionsView.ShowColumnHeaders = false;
            this.VesselSpdView.OptionsView.ShowGroupExpandCollapseButtons = false;
            this.VesselSpdView.OptionsView.ShowGroupPanel = false;
            this.VesselSpdView.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.True;
            this.VesselSpdView.OptionsView.ShowIndicator = false;
            this.VesselSpdView.RowHeight = 30;
            this.VesselSpdView.VertScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Never;
            this.VesselSpdView.CellValueChanged += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.VesselSpdView_CellValueChanged);
            this.VesselSpdView.KeyDown += new System.Windows.Forms.KeyEventHandler(this.VesselSpdView_KeyDown);
            // 
            // gridBand5
            // 
            this.gridBand5.AppearanceHeader.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.gridBand5.AppearanceHeader.BackColor2 = System.Drawing.Color.Silver;
            this.gridBand5.AppearanceHeader.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.gridBand5.AppearanceHeader.Options.UseBackColor = true;
            this.gridBand5.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand5.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand5.Caption = "Ballast";
            this.gridBand5.Columns.Add(this.BallastSpd);
            this.gridBand5.Name = "gridBand5";
            this.gridBand5.OptionsBand.AllowHotTrack = false;
            this.gridBand5.OptionsBand.AllowMove = false;
            this.gridBand5.OptionsBand.AllowPress = false;
            this.gridBand5.OptionsBand.AllowSize = false;
            this.gridBand5.OptionsBand.ShowInCustomizationForm = false;
            this.gridBand5.VisibleIndex = 0;
            this.gridBand5.Width = 80;
            // 
            // BallastSpd
            // 
            this.BallastSpd.AppearanceCell.Options.UseTextOptions = true;
            this.BallastSpd.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.BallastSpd.DisplayFormat.FormatString = "{0:#0.00}";
            this.BallastSpd.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.BallastSpd.FieldName = "Ballast";
            this.BallastSpd.Name = "BallastSpd";
            this.BallastSpd.Visible = true;
            this.BallastSpd.Width = 80;
            // 
            // gridBand6
            // 
            this.gridBand6.AppearanceHeader.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.gridBand6.AppearanceHeader.BackColor2 = System.Drawing.Color.Silver;
            this.gridBand6.AppearanceHeader.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.gridBand6.AppearanceHeader.Options.UseBackColor = true;
            this.gridBand6.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand6.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand6.Caption = "Laden";
            this.gridBand6.Columns.Add(this.LadenSpd);
            this.gridBand6.Name = "gridBand6";
            this.gridBand6.OptionsBand.AllowHotTrack = false;
            this.gridBand6.OptionsBand.AllowMove = false;
            this.gridBand6.OptionsBand.AllowPress = false;
            this.gridBand6.OptionsBand.AllowSize = false;
            this.gridBand6.OptionsBand.ShowInCustomizationForm = false;
            this.gridBand6.VisibleIndex = 1;
            this.gridBand6.Width = 80;
            // 
            // LadenSpd
            // 
            this.LadenSpd.AppearanceCell.Options.UseTextOptions = true;
            this.LadenSpd.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.LadenSpd.DisplayFormat.FormatString = "{0:#0.00}";
            this.LadenSpd.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.LadenSpd.FieldName = "Laden";
            this.LadenSpd.Name = "LadenSpd";
            this.LadenSpd.Visible = true;
            this.LadenSpd.Width = 80;
            // 
            // MVPanel
            // 
            this.MVPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.MVPanel.Controls.Add(this.VesselMVControl);
            this.MVPanel.Location = new System.Drawing.Point(7, 40);
            this.MVPanel.Name = "MVPanel";
            this.MVPanel.Size = new System.Drawing.Size(481, 149);
            this.MVPanel.TabIndex = 1;
            // 
            // VesselMVControl
            // 
            this.VesselMVControl.DataSource = this.vPVesselBindingSource;
            this.VesselMVControl.Location = new System.Drawing.Point(29, 43);
            this.VesselMVControl.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat;
            this.VesselMVControl.LookAndFeel.UseDefaultLookAndFeel = false;
            this.VesselMVControl.MainView = this.VesselMVView;
            this.VesselMVControl.Name = "VesselMVControl";
            this.VesselMVControl.Size = new System.Drawing.Size(424, 65);
            this.VesselMVControl.TabIndex = 3;
            this.VesselMVControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.VesselMVView});
            this.VesselMVControl.EditorKeyDown += new System.Windows.Forms.KeyEventHandler(this.VesselMVControl_EditorKeyDown);
            // 
            // vPVesselBindingSource
            // 
            this.vPVesselBindingSource.DataMember = "VPVessel";
            this.vPVesselBindingSource.DataSource = this.sEDataBindingSource;
            // 
            // VesselMVView
            // 
            this.VesselMVView.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(120)))), ((int)(((byte)(177)))), ((int)(((byte)(145)))));
            this.VesselMVView.Appearance.FocusedRow.Options.UseBackColor = true;
            this.VesselMVView.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.Transparent;
            this.VesselMVView.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.VesselMVView.BandPanelRowHeight = 30;
            this.VesselMVView.Bands.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand112,
            this.gridBand2,
            this.gridBand3,
            this.gridBand4});
            this.VesselMVView.Columns.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn[] {
            this.VesselMV,
            this.VesselType,
            this.VesselDWT,
            this.VesselDraft});
            this.VesselMVView.GridControl = this.VesselMVControl;
            this.VesselMVView.HorzScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Never;
            this.VesselMVView.Name = "VesselMVView";
            this.VesselMVView.OptionsBehavior.AlignGroupSummaryInGroupRow = DevExpress.Utils.DefaultBoolean.False;
            this.VesselMVView.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.False;
            this.VesselMVView.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.False;
            this.VesselMVView.OptionsBehavior.AllowFixedGroups = DevExpress.Utils.DefaultBoolean.False;
            this.VesselMVView.OptionsBehavior.AllowPartialGroups = DevExpress.Utils.DefaultBoolean.False;
            this.VesselMVView.OptionsBehavior.AllowPartialRedrawOnScrolling = false;
            this.VesselMVView.OptionsBehavior.AllowPixelScrolling = DevExpress.Utils.DefaultBoolean.False;
            this.VesselMVView.OptionsCustomization.AllowBandMoving = false;
            this.VesselMVView.OptionsCustomization.AllowBandResizing = false;
            this.VesselMVView.OptionsCustomization.AllowColumnMoving = false;
            this.VesselMVView.OptionsCustomization.AllowColumnResizing = false;
            this.VesselMVView.OptionsCustomization.AllowGroup = false;
            this.VesselMVView.OptionsCustomization.AllowQuickHideColumns = false;
            this.VesselMVView.OptionsCustomization.AllowSort = false;
            this.VesselMVView.OptionsCustomization.ShowBandsInCustomizationForm = false;
            this.VesselMVView.OptionsSelection.InvertSelection = true;
            this.VesselMVView.OptionsSelection.UseIndicatorForSelection = false;
            this.VesselMVView.OptionsView.AllowHtmlDrawGroups = false;
            this.VesselMVView.OptionsView.ShowColumnHeaders = false;
            this.VesselMVView.OptionsView.ShowGroupExpandCollapseButtons = false;
            this.VesselMVView.OptionsView.ShowGroupPanel = false;
            this.VesselMVView.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.True;
            this.VesselMVView.OptionsView.ShowIndicator = false;
            this.VesselMVView.RowHeight = 30;
            this.VesselMVView.VertScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Never;
            this.VesselMVView.KeyDown += new System.Windows.Forms.KeyEventHandler(this.VesselMVView_KeyDown);
            // 
            // gridBand112
            // 
            this.gridBand112.AppearanceHeader.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.gridBand112.AppearanceHeader.BackColor2 = System.Drawing.Color.Silver;
            this.gridBand112.AppearanceHeader.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.gridBand112.AppearanceHeader.Options.UseBackColor = true;
            this.gridBand112.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand112.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand112.Caption = "MV";
            this.gridBand112.Columns.Add(this.VesselMV);
            this.gridBand112.Name = "gridBand112";
            this.gridBand112.OptionsBand.AllowHotTrack = false;
            this.gridBand112.OptionsBand.AllowMove = false;
            this.gridBand112.OptionsBand.AllowPress = false;
            this.gridBand112.OptionsBand.ShowInCustomizationForm = false;
            this.gridBand112.VisibleIndex = 0;
            this.gridBand112.Width = 180;
            // 
            // VesselMV
            // 
            this.VesselMV.FieldName = "MV";
            this.VesselMV.Name = "VesselMV";
            this.VesselMV.Visible = true;
            this.VesselMV.Width = 180;
            // 
            // gridBand2
            // 
            this.gridBand2.AppearanceHeader.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.gridBand2.AppearanceHeader.BackColor2 = System.Drawing.Color.Silver;
            this.gridBand2.AppearanceHeader.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.gridBand2.AppearanceHeader.Options.UseBackColor = true;
            this.gridBand2.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand2.Caption = "Type";
            this.gridBand2.Columns.Add(this.VesselType);
            this.gridBand2.Name = "gridBand2";
            this.gridBand2.OptionsBand.AllowHotTrack = false;
            this.gridBand2.OptionsBand.AllowMove = false;
            this.gridBand2.OptionsBand.AllowPress = false;
            this.gridBand2.OptionsBand.AllowSize = false;
            this.gridBand2.OptionsBand.ShowInCustomizationForm = false;
            this.gridBand2.VisibleIndex = 1;
            this.gridBand2.Width = 80;
            // 
            // VesselType
            // 
            this.VesselType.FieldName = "Type";
            this.VesselType.Name = "VesselType";
            this.VesselType.Visible = true;
            this.VesselType.Width = 80;
            // 
            // gridBand3
            // 
            this.gridBand3.AppearanceHeader.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.gridBand3.AppearanceHeader.BackColor2 = System.Drawing.Color.Silver;
            this.gridBand3.AppearanceHeader.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.gridBand3.AppearanceHeader.Options.UseBackColor = true;
            this.gridBand3.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand3.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand3.Caption = "DWT";
            this.gridBand3.Columns.Add(this.VesselDWT);
            this.gridBand3.Name = "gridBand3";
            this.gridBand3.OptionsBand.AllowHotTrack = false;
            this.gridBand3.OptionsBand.AllowMove = false;
            this.gridBand3.OptionsBand.AllowPress = false;
            this.gridBand3.OptionsBand.ShowInCustomizationForm = false;
            this.gridBand3.VisibleIndex = 2;
            this.gridBand3.Width = 80;
            // 
            // VesselDWT
            // 
            this.VesselDWT.AppearanceCell.Options.UseTextOptions = true;
            this.VesselDWT.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.VesselDWT.DisplayFormat.FormatString = "{0:#,##0}";
            this.VesselDWT.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.VesselDWT.FieldName = "DWT";
            this.VesselDWT.Name = "VesselDWT";
            this.VesselDWT.Visible = true;
            this.VesselDWT.Width = 80;
            // 
            // gridBand4
            // 
            this.gridBand4.AppearanceHeader.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.gridBand4.AppearanceHeader.BackColor2 = System.Drawing.Color.Silver;
            this.gridBand4.AppearanceHeader.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.gridBand4.AppearanceHeader.Options.UseBackColor = true;
            this.gridBand4.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand4.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand4.Caption = "Draft";
            this.gridBand4.Columns.Add(this.VesselDraft);
            this.gridBand4.Name = "gridBand4";
            this.gridBand4.OptionsBand.AllowHotTrack = false;
            this.gridBand4.OptionsBand.AllowMove = false;
            this.gridBand4.OptionsBand.AllowPress = false;
            this.gridBand4.OptionsBand.ShowInCustomizationForm = false;
            this.gridBand4.VisibleIndex = 3;
            this.gridBand4.Width = 80;
            // 
            // VesselDraft
            // 
            this.VesselDraft.AppearanceCell.Options.UseTextOptions = true;
            this.VesselDraft.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.VesselDraft.DisplayFormat.FormatString = "{0:#,##0.00}";
            this.VesselDraft.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.VesselDraft.FieldName = "Draft";
            this.VesselDraft.Name = "VesselDraft";
            this.VesselDraft.Visible = true;
            this.VesselDraft.Width = 80;
            // 
            // VesselLabel
            // 
            this.VesselLabel.AutoSize = true;
            this.VesselLabel.Dock = System.Windows.Forms.DockStyle.Top;
            this.VesselLabel.Font = new System.Drawing.Font("Leelawadee UI", 20F, System.Drawing.FontStyle.Bold);
            this.VesselLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(130)))), ((int)(((byte)(28)))), ((int)(((byte)(51)))));
            this.VesselLabel.Location = new System.Drawing.Point(0, 0);
            this.VesselLabel.Name = "VesselLabel";
            this.VesselLabel.Size = new System.Drawing.Size(229, 37);
            this.VesselLabel.TabIndex = 0;
            this.VesselLabel.Text = "Vessel Particular";
            // 
            // persistentRepository1
            // 
            this.persistentRepository1.Items.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.NameDetailBtn,
            this.ZoomBtn,
            this.CraneBtn,
            this.PortREditBtn,
            this.CargoEditBtn});
            // 
            // NameDetailBtn
            // 
            this.NameDetailBtn.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            serializableAppearanceObject13.BackColor = System.Drawing.Color.Transparent;
            serializableAppearanceObject13.Options.UseBackColor = true;
            serializableAppearanceObject13.Options.UseTextOptions = true;
            serializableAppearanceObject13.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.NameDetailBtn.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, editorButtonImageOptions4, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject13, serializableAppearanceObject14, serializableAppearanceObject15, serializableAppearanceObject16, "", null, null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.NameDetailBtn.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.NameDetailBtn.Name = "NameDetailBtn";
            // 
            // ZoomBtn
            // 
            this.ZoomBtn.Appearance.Options.UseTextOptions = true;
            this.ZoomBtn.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.ZoomBtn.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            editorButtonImageOptions5.Location = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            serializableAppearanceObject17.BackColor = System.Drawing.Color.Transparent;
            serializableAppearanceObject17.Options.UseBackColor = true;
            serializableAppearanceObject17.Options.UseTextOptions = true;
            serializableAppearanceObject17.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.ZoomBtn.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, true, editorButtonImageOptions5, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject17, serializableAppearanceObject18, serializableAppearanceObject19, serializableAppearanceObject20, "", null, null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.ZoomBtn.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.ZoomBtn.Name = "ZoomBtn";
            // 
            // CraneBtn
            // 
            this.CraneBtn.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            serializableAppearanceObject21.BackColor = System.Drawing.Color.Transparent;
            serializableAppearanceObject21.Options.UseBackColor = true;
            serializableAppearanceObject21.Options.UseTextOptions = true;
            serializableAppearanceObject21.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.CraneBtn.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, editorButtonImageOptions6, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject21, serializableAppearanceObject22, serializableAppearanceObject23, serializableAppearanceObject24, "", null, null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.CraneBtn.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.CraneBtn.Name = "CraneBtn";
            // 
            // PortREditBtn
            // 
            this.PortREditBtn.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            editorButtonImageOptions7.Location = DevExpress.XtraEditors.ImageLocation.MiddleRight;
            serializableAppearanceObject25.BackColor = System.Drawing.Color.Transparent;
            serializableAppearanceObject25.Options.UseBackColor = true;
            serializableAppearanceObject25.Options.UseTextOptions = true;
            serializableAppearanceObject25.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.PortREditBtn.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, editorButtonImageOptions7, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject25, serializableAppearanceObject26, serializableAppearanceObject27, serializableAppearanceObject28, "", null, null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.PortREditBtn.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.PortREditBtn.Name = "PortREditBtn";
            this.PortREditBtn.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.EditBtn_ButtonClick);
            // 
            // CargoEditBtn
            // 
            this.CargoEditBtn.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            editorButtonImageOptions8.Location = DevExpress.XtraEditors.ImageLocation.MiddleRight;
            serializableAppearanceObject29.BackColor = System.Drawing.Color.Transparent;
            serializableAppearanceObject29.Options.UseBackColor = true;
            serializableAppearanceObject29.Options.UseTextOptions = true;
            serializableAppearanceObject29.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.CargoEditBtn.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, editorButtonImageOptions8, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject29, serializableAppearanceObject30, serializableAppearanceObject31, serializableAppearanceObject32, "", null, null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.CargoEditBtn.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.CargoEditBtn.Name = "CargoEditBtn";
            this.CargoEditBtn.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.CargoEditBtn_ButtonClick);
            // 
            // voyage
            // 
            this.AutoScroll = true;
            this.AutoScrollMargin = new System.Drawing.Size(5, 5);
            this.Controls.Add(this.voyagepanel);
            this.Name = "voyage";
            this.Size = new System.Drawing.Size(1678, 1012);
            this.Load += new System.EventHandler(this.voyage_Load);
            this.voyagepanel.ResumeLayout(false);
            this.ResultPanel.ResumeLayout(false);
            this.ResultPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ResutlGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.resultBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sEDataBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ResultView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ResultCBGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.resultCBBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ResultCBView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ResultBEGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.resultBEBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ResultBEView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ResultOEGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.resultOEBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ResultOEView)).EndInit();
            this.PortRPanel.ResumeLayout(false);
            this.PortRPanel.PerformLayout();
            this.DurationPanel.ResumeLayout(false);
            this.DurationPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PortRotationGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.portRBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PortRotationView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PortTypeComboBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NonBtnDateEdit.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NonBtnDateEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateEdit.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateEdit)).EndInit();
            this.CargoPanel.ResumeLayout(false);
            this.CargoPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CargoGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cargoBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CargoView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CargoHistoryBtn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.QuantityTermComboBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TermComboBox)).EndInit();
            this.VesselPanel.ResumeLayout(false);
            this.VesselPanel.PerformLayout();
            this.FusselPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.VesselDOControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vPDOBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.VesselDOView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DoTypeComboBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.VesselFOControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vPFOBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.VesselFOView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox1)).EndInit();
            this.SpdPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.VesselSpdControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vPSpdBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.VesselSpdView)).EndInit();
            this.MVPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.VesselMVControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vPVesselBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.VesselMVView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NameDetailBtn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ZoomBtn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CraneBtn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PortREditBtn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CargoEditBtn)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel voyagepanel;
        private System.Windows.Forms.Panel ResultPanel;
        private System.Windows.Forms.Label BELabel;
        private System.Windows.Forms.Label ResultLabel;
        private System.Windows.Forms.Label OPLabel;
        private System.Windows.Forms.Panel PortRPanel;
        private System.Windows.Forms.Label PortRLabel;
        private System.Windows.Forms.Panel CargoPanel;
        private System.Windows.Forms.Label CargoLabel;
        private System.Windows.Forms.Panel VesselPanel;
        private System.Windows.Forms.Panel FusselPanel;
        private System.Windows.Forms.Panel SpdPanel;
        private System.Windows.Forms.Panel MVPanel;
        private System.Windows.Forms.Label VesselLabel;
        private DevExpress.XtraGrid.GridControl ResultOEGridControl;
        private DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView ResultOEView;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn ResultOEType1;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn ResultOEValue1;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn ResultOEType2;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn ResultOEValue2;
        private DevExpress.XtraGrid.GridControl CargoGridControl;
        private DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView CargoView;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn CargoIndex;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn CargoAccount;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn CargoHistory;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn AComm;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn CargoName;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn LoadingPort;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn DischargingPort;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn Quantity;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn QuantityTerm;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn Frt;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn Term;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn Revenue;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn Brkg;
        private DevExpress.XtraGrid.GridControl VesselFOControl;
        private DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView VesselFOView;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn LSFO;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn FOType;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn FOBallast;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn FOLaden;
        private DevExpress.XtraGrid.GridControl VesselSpdControl;
        private DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView VesselSpdView;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn BallastSpd;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn LadenSpd;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand5;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand6;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn FOIdle;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn FOWork;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand9;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand10;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand11;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand12;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand1;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand7;
        private DevExpress.XtraGrid.GridControl VesselDOControl;
        private DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView VesselDOView;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn LSDO;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn DOType;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn DOSea;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn DOIdle;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn DOWork;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn FrtTax;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn LinerTerm;
        private DevExpress.XtraGrid.GridControl PortRotationGridControl;
        private DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView PortRotationView;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn PortRIndex;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn PortRType;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn PortRName;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn PortRDem;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn PortRNameDetail;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn PortRDistance;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn PortRSeca;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn PortRWF;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn PortRSpd;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn PortRSea;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn PortRLDRate;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn PortRIdle;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn PortRWork;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn PortRCrane;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn PortRDes;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn PortRPortCharge;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn PortRArrival;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn PortRDepature;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox1;
        private DevExpress.XtraGrid.GridControl ResutlGridControl;
        private DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView ResultView;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand53;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn ResultType;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand54;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn ResultValue;
        private DevExpress.XtraGrid.GridControl ResultCBGridControl;
        private DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView ResultCBView;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand51;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn ReultCBType;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand52;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn ReultCBValue;
        private DevExpress.XtraGrid.GridControl ResultBEGridControl;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand43;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand44;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand45;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand46;
        public System.Windows.Forms.BindingSource sEDataBindingSource;
        private System.Windows.Forms.BindingSource vPSpdBindingSource;
        private DevExpress.XtraGrid.GridControl VesselMVControl;
        private DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView VesselMVView;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand112;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn VesselMV;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand2;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn VesselType;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand3;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn VesselDWT;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand4;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn VesselDraft;
        private System.Windows.Forms.BindingSource vPDOBindingSource;
        private System.Windows.Forms.BindingSource vPFOBindingSource;
        private System.Windows.Forms.BindingSource vPVesselBindingSource;
        private System.Windows.Forms.BindingSource resultBindingSource;
        private System.Windows.Forms.BindingSource resultCBBindingSource;
        private System.Windows.Forms.BindingSource resultBEBindingSource;
        private System.Windows.Forms.BindingSource resultOEBindingSource;
        private System.Windows.Forms.BindingSource portRBindingSource;
        private System.Windows.Forms.BindingSource cargoBindingSource;
        private DevExpress.XtraGrid.Views.Grid.GridView ResultBEView;
        private DevExpress.XtraGrid.Columns.GridColumn ResultBEType;
        private DevExpress.XtraGrid.Columns.GridColumn ResultBEFO;
        private DevExpress.XtraGrid.Columns.GridColumn ResultBEDO;
        private DevExpress.XtraGrid.Columns.GridColumn ResultBEDOValue;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit CargoHistoryBtn;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox QuantityTermComboBox;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox TermComboBox;
        private DevExpress.XtraEditors.Repository.PersistentRepository persistentRepository1;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand17;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand18;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand19;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand20;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand21;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand22;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand23;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand24;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand25;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand26;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand27;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand28;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand29;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit DateEdit;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox DoTypeComboBox;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit NonBtnDateEdit;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit NameDetailBtn;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit ZoomBtn;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit CraneBtn;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit PortREditBtn;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit CargoEditBtn;
        private System.Windows.Forms.Panel DurationPanel;
        private System.Windows.Forms.Label DurationLabel;
        private DevExpress.XtraEditors.SimpleButton CargoRemove;
        private DevExpress.XtraEditors.SimpleButton CargoInsert;
        private DevExpress.XtraEditors.SimpleButton CargoAppend;
        private DevExpress.XtraEditors.SimpleButton CargoClear;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox PortTypeComboBox;
        private DevExpress.XtraGrid.Columns.GridColumn ResultBEFOValue;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand8;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand13;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand15;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand16;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand59;
        private DevExpress.XtraEditors.SimpleButton PortRRemove;
        private DevExpress.XtraEditors.SimpleButton PortRInsert;
        private DevExpress.XtraEditors.SimpleButton PortRAppend;
        private DevExpress.XtraEditors.SimpleButton PortRClear;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand30;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand31;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand32;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand33;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand34;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand35;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand36;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand37;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand38;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand39;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand40;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand41;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand42;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand60;
    }
}
