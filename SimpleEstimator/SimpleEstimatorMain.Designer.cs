﻿namespace SimpleEstimator
{
    partial class SimpleEstimatorMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SimpleEstimatorMain));
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions1 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions2 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions3 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.bar4 = new DevExpress.XtraBars.Bar();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.barSubItem10 = new DevExpress.XtraBars.BarSubItem();
            this.barButtonItem43 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem44 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem45 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem46 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem47 = new DevExpress.XtraBars.BarButtonItem();
            this.barSubItem11 = new DevExpress.XtraBars.BarSubItem();
            this.barButtonItem48 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem49 = new DevExpress.XtraBars.BarButtonItem();
            this.barSubItem12 = new DevExpress.XtraBars.BarSubItem();
            this.barSubItem13 = new DevExpress.XtraBars.BarSubItem();
            this.barSubItem14 = new DevExpress.XtraBars.BarSubItem();
            this.barSubItem15 = new DevExpress.XtraBars.BarSubItem();
            this.barButtonItem50 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem51 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem52 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem53 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem54 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem55 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem56 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem57 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem60 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem59 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem58 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem61 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem62 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem63 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem64 = new DevExpress.XtraBars.BarButtonItem();
            this.bar3 = new DevExpress.XtraBars.Bar();
            this.barSubItem1 = new DevExpress.XtraBars.BarSubItem();
            this.BtnSignOut = new DevExpress.XtraBars.BarButtonItem();
            this.barSubItemNew = new DevExpress.XtraBars.BarSubItem();
            this.btnWorkBook = new DevExpress.XtraBars.BarButtonItem();
            this.btnVoyageSheet = new DevExpress.XtraBars.BarButtonItem();
            this.btnCargoReletSheet = new DevExpress.XtraBars.BarButtonItem();
            this.btnTimeCharterSheet = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem3 = new DevExpress.XtraBars.BarButtonItem();
            this.barSubItem2 = new DevExpress.XtraBars.BarSubItem();
            this.barButtonItem5 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem1 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem2 = new DevExpress.XtraBars.BarButtonItem();
            this.barSubItem3 = new DevExpress.XtraBars.BarSubItem();
            this.barButtonItem7 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem8 = new DevExpress.XtraBars.BarButtonItem();
            this.barSubItem4 = new DevExpress.XtraBars.BarSubItem();
            this.barButtonItem9 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem10 = new DevExpress.XtraBars.BarButtonItem();
            this.barSubItem5 = new DevExpress.XtraBars.BarSubItem();
            this.barButtonItem11 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem12 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem13 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem14 = new DevExpress.XtraBars.BarButtonItem();
            this.barSubItem6 = new DevExpress.XtraBars.BarSubItem();
            this.barButtonItem15 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem16 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem6 = new DevExpress.XtraBars.BarButtonItem();
            this.barSubItem7 = new DevExpress.XtraBars.BarSubItem();
            this.barButtonItem17 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem18 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem19 = new DevExpress.XtraBars.BarButtonItem();
            this.barSubItem8 = new DevExpress.XtraBars.BarSubItem();
            this.barButtonItem20 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem21 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem22 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem23 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem24 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem25 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem26 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem27 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem28 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem29 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem30 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem31 = new DevExpress.XtraBars.BarButtonItem();
            this.barSubItem9 = new DevExpress.XtraBars.BarSubItem();
            this.barButtonItem32 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem33 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem34 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem35 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem36 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem37 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem38 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem39 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem40 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem41 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem42 = new DevExpress.XtraBars.BarButtonItem();
            this.bar5 = new DevExpress.XtraBars.Bar();
            this.barAndDockingController1 = new DevExpress.XtraBars.BarAndDockingController(this.components);
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.barListItem1 = new DevExpress.XtraBars.BarListItem();
            this.barLinkContainerItem1 = new DevExpress.XtraBars.BarLinkContainerItem();
            this.barToolbarsListItem1 = new DevExpress.XtraBars.BarToolbarsListItem();
            this.barButtonItem4 = new DevExpress.XtraBars.BarButtonItem();
            this.barCheckItem1 = new DevExpress.XtraBars.BarCheckItem();
            this.barDockControl1 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl2 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl3 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl4 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl5 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl6 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl7 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl8 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl9 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl10 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl11 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl12 = new DevExpress.XtraBars.BarDockControl();
            this.xtraTabControl1 = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPage1 = new DevExpress.XtraTab.XtraTabPage();
            this.voyage1 = new SimpleEstimator.Views.voyage();
            this.xtraTabPage2 = new DevExpress.XtraTab.XtraTabPage();
            this.xtraTabPage3 = new DevExpress.XtraTab.XtraTabPage();
            this.xtraTabPage4 = new DevExpress.XtraTab.XtraTabPage();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barAndDockingController1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).BeginInit();
            this.xtraTabControl1.SuspendLayout();
            this.xtraTabPage1.SuspendLayout();
            this.SuspendLayout();
            // 
            // bar1
            // 
            this.bar1.BarName = "Main menu";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.OptionsBar.MultiLine = true;
            this.bar1.OptionsBar.UseWholeRow = true;
            this.bar1.Text = "Main menu";
            // 
            // bar4
            // 
            this.bar4.BarName = "Main menu";
            this.bar4.DockCol = 0;
            this.bar4.DockRow = 0;
            this.bar4.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar4.OptionsBar.MultiLine = true;
            this.bar4.OptionsBar.UseWholeRow = true;
            this.bar4.Text = "Main menu";
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar2,
            this.bar3,
            this.bar5});
            this.barManager1.Controller = this.barAndDockingController1;
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barListItem1,
            this.barLinkContainerItem1,
            this.barSubItem1,
            this.barToolbarsListItem1,
            this.BtnSignOut,
            this.barButtonItem3,
            this.barButtonItem5,
            this.barSubItemNew,
            this.btnWorkBook,
            this.btnVoyageSheet,
            this.btnCargoReletSheet,
            this.btnTimeCharterSheet,
            this.barSubItem2,
            this.barButtonItem1,
            this.barButtonItem2,
            this.barButtonItem4,
            this.barSubItem3,
            this.barSubItem4,
            this.barSubItem5,
            this.barSubItem6,
            this.barButtonItem6,
            this.barButtonItem7,
            this.barButtonItem8,
            this.barButtonItem9,
            this.barButtonItem10,
            this.barButtonItem11,
            this.barButtonItem12,
            this.barButtonItem13,
            this.barButtonItem14,
            this.barButtonItem15,
            this.barButtonItem16,
            this.barSubItem7,
            this.barButtonItem17,
            this.barButtonItem18,
            this.barButtonItem19,
            this.barSubItem8,
            this.barButtonItem20,
            this.barButtonItem21,
            this.barButtonItem22,
            this.barButtonItem23,
            this.barButtonItem24,
            this.barButtonItem25,
            this.barButtonItem26,
            this.barButtonItem27,
            this.barButtonItem28,
            this.barButtonItem29,
            this.barButtonItem30,
            this.barButtonItem31,
            this.barSubItem9,
            this.barButtonItem32,
            this.barButtonItem33,
            this.barButtonItem34,
            this.barButtonItem35,
            this.barButtonItem36,
            this.barButtonItem37,
            this.barButtonItem38,
            this.barButtonItem39,
            this.barButtonItem40,
            this.barButtonItem41,
            this.barButtonItem42,
            this.barCheckItem1,
            this.barSubItem10,
            this.barButtonItem43,
            this.barButtonItem44,
            this.barButtonItem45,
            this.barButtonItem46,
            this.barButtonItem47,
            this.barSubItem11,
            this.barButtonItem48,
            this.barButtonItem49,
            this.barSubItem12,
            this.barSubItem13,
            this.barSubItem14,
            this.barSubItem15,
            this.barButtonItem50,
            this.barButtonItem51,
            this.barButtonItem52,
            this.barButtonItem53,
            this.barButtonItem54,
            this.barButtonItem55,
            this.barButtonItem56,
            this.barButtonItem57,
            this.barButtonItem58,
            this.barButtonItem59,
            this.barButtonItem60,
            this.barButtonItem61,
            this.barButtonItem62,
            this.barButtonItem63,
            this.barButtonItem64});
            this.barManager1.MainMenu = this.bar3;
            this.barManager1.MaxItemId = 112;
            this.barManager1.StatusBar = this.bar5;
            // 
            // bar2
            // 
            this.bar2.BarAppearance.Normal.BackColor = System.Drawing.Color.White;
            this.bar2.BarAppearance.Normal.Options.UseBackColor = true;
            this.bar2.BarName = "Tools";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 1;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barSubItem10),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem47, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.barSubItem11),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem48, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem49),
            new DevExpress.XtraBars.LinkPersistInfo(this.barSubItem12, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.barSubItem13),
            new DevExpress.XtraBars.LinkPersistInfo(this.barSubItem14, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.barSubItem15),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem50, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem51),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem52),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem53),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem54, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem55),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem56, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem57),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem60, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem59, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem58, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem61, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem62, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem63),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem64, true)});
            this.bar2.OptionsBar.AllowQuickCustomization = false;
            this.bar2.OptionsBar.DisableClose = true;
            this.bar2.OptionsBar.DisableCustomization = true;
            this.bar2.OptionsBar.DrawDragBorder = false;
            this.bar2.OptionsBar.RotateWhenVertical = false;
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.Text = "Tools";
            // 
            // barSubItem10
            // 
            this.barSubItem10.AllowDrawArrow = DevExpress.Utils.DefaultBoolean.True;
            this.barSubItem10.Id = 79;
            this.barSubItem10.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barSubItem10.ImageOptions.Image")));
            this.barSubItem10.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barSubItem10.ImageOptions.LargeImage")));
            this.barSubItem10.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem43),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem44),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem45),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem46)});
            this.barSubItem10.Name = "barSubItem10";
            this.barSubItem10.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // barButtonItem43
            // 
            this.barButtonItem43.Caption = "&Workbook";
            this.barButtonItem43.Id = 80;
            this.barButtonItem43.Name = "barButtonItem43";
            // 
            // barButtonItem44
            // 
            this.barButtonItem44.Caption = "&Voyage Sheet";
            this.barButtonItem44.Id = 81;
            this.barButtonItem44.Name = "barButtonItem44";
            // 
            // barButtonItem45
            // 
            this.barButtonItem45.Caption = "&Cargo Relet Sheet";
            this.barButtonItem45.Id = 82;
            this.barButtonItem45.Name = "barButtonItem45";
            // 
            // barButtonItem46
            // 
            this.barButtonItem46.Caption = "&Time Charter Sheet";
            this.barButtonItem46.Id = 83;
            this.barButtonItem46.Name = "barButtonItem46";
            // 
            // barButtonItem47
            // 
            this.barButtonItem47.Id = 84;
            this.barButtonItem47.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem47.ImageOptions.Image")));
            this.barButtonItem47.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barButtonItem47.ImageOptions.LargeImage")));
            this.barButtonItem47.Name = "barButtonItem47";
            // 
            // barSubItem11
            // 
            this.barSubItem11.Id = 85;
            this.barSubItem11.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barSubItem11.ImageOptions.Image")));
            this.barSubItem11.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barSubItem11.ImageOptions.LargeImage")));
            this.barSubItem11.Name = "barSubItem11";
            this.barSubItem11.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // barButtonItem48
            // 
            this.barButtonItem48.Id = 86;
            this.barButtonItem48.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem48.ImageOptions.Image")));
            this.barButtonItem48.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barButtonItem48.ImageOptions.LargeImage")));
            this.barButtonItem48.Name = "barButtonItem48";
            // 
            // barButtonItem49
            // 
            this.barButtonItem49.Id = 87;
            this.barButtonItem49.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem49.ImageOptions.Image")));
            this.barButtonItem49.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barButtonItem49.ImageOptions.LargeImage")));
            this.barButtonItem49.Name = "barButtonItem49";
            // 
            // barSubItem12
            // 
            this.barSubItem12.Id = 90;
            this.barSubItem12.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barSubItem12.ImageOptions.Image")));
            this.barSubItem12.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barSubItem12.ImageOptions.LargeImage")));
            this.barSubItem12.Name = "barSubItem12";
            this.barSubItem12.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // barSubItem13
            // 
            this.barSubItem13.Id = 91;
            this.barSubItem13.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barSubItem13.ImageOptions.Image")));
            this.barSubItem13.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barSubItem13.ImageOptions.LargeImage")));
            this.barSubItem13.Name = "barSubItem13";
            this.barSubItem13.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // barSubItem14
            // 
            this.barSubItem14.Id = 92;
            this.barSubItem14.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barSubItem14.ImageOptions.Image")));
            this.barSubItem14.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barSubItem14.ImageOptions.LargeImage")));
            this.barSubItem14.Name = "barSubItem14";
            this.barSubItem14.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // barSubItem15
            // 
            this.barSubItem15.Id = 93;
            this.barSubItem15.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barSubItem15.ImageOptions.Image")));
            this.barSubItem15.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barSubItem15.ImageOptions.LargeImage")));
            this.barSubItem15.Name = "barSubItem15";
            this.barSubItem15.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // barButtonItem50
            // 
            this.barButtonItem50.Id = 95;
            this.barButtonItem50.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem50.ImageOptions.Image")));
            this.barButtonItem50.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barButtonItem50.ImageOptions.LargeImage")));
            this.barButtonItem50.Name = "barButtonItem50";
            // 
            // barButtonItem51
            // 
            this.barButtonItem51.Id = 96;
            this.barButtonItem51.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem51.ImageOptions.Image")));
            this.barButtonItem51.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barButtonItem51.ImageOptions.LargeImage")));
            this.barButtonItem51.Name = "barButtonItem51";
            // 
            // barButtonItem52
            // 
            this.barButtonItem52.Id = 97;
            this.barButtonItem52.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem52.ImageOptions.Image")));
            this.barButtonItem52.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barButtonItem52.ImageOptions.LargeImage")));
            this.barButtonItem52.Name = "barButtonItem52";
            // 
            // barButtonItem53
            // 
            this.barButtonItem53.Id = 98;
            this.barButtonItem53.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem53.ImageOptions.Image")));
            this.barButtonItem53.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barButtonItem53.ImageOptions.LargeImage")));
            this.barButtonItem53.Name = "barButtonItem53";
            // 
            // barButtonItem54
            // 
            this.barButtonItem54.Id = 99;
            this.barButtonItem54.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem54.ImageOptions.Image")));
            this.barButtonItem54.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barButtonItem54.ImageOptions.LargeImage")));
            this.barButtonItem54.Name = "barButtonItem54";
            // 
            // barButtonItem55
            // 
            this.barButtonItem55.Id = 102;
            this.barButtonItem55.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem55.ImageOptions.Image")));
            this.barButtonItem55.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barButtonItem55.ImageOptions.LargeImage")));
            this.barButtonItem55.Name = "barButtonItem55";
            // 
            // barButtonItem56
            // 
            this.barButtonItem56.Id = 103;
            this.barButtonItem56.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem56.ImageOptions.Image")));
            this.barButtonItem56.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barButtonItem56.ImageOptions.LargeImage")));
            this.barButtonItem56.Name = "barButtonItem56";
            // 
            // barButtonItem57
            // 
            this.barButtonItem57.Id = 104;
            this.barButtonItem57.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem57.ImageOptions.Image")));
            this.barButtonItem57.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barButtonItem57.ImageOptions.LargeImage")));
            this.barButtonItem57.Name = "barButtonItem57";
            // 
            // barButtonItem60
            // 
            this.barButtonItem60.Id = 107;
            this.barButtonItem60.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem60.ImageOptions.Image")));
            this.barButtonItem60.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barButtonItem60.ImageOptions.LargeImage")));
            this.barButtonItem60.Name = "barButtonItem60";
            // 
            // barButtonItem59
            // 
            this.barButtonItem59.Id = 106;
            this.barButtonItem59.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem59.ImageOptions.Image")));
            this.barButtonItem59.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barButtonItem59.ImageOptions.LargeImage")));
            this.barButtonItem59.Name = "barButtonItem59";
            // 
            // barButtonItem58
            // 
            this.barButtonItem58.Id = 105;
            this.barButtonItem58.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem58.ImageOptions.Image")));
            this.barButtonItem58.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barButtonItem58.ImageOptions.LargeImage")));
            this.barButtonItem58.Name = "barButtonItem58";
            // 
            // barButtonItem61
            // 
            this.barButtonItem61.Id = 108;
            this.barButtonItem61.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem61.ImageOptions.Image")));
            this.barButtonItem61.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barButtonItem61.ImageOptions.LargeImage")));
            this.barButtonItem61.Name = "barButtonItem61";
            // 
            // barButtonItem62
            // 
            this.barButtonItem62.Id = 109;
            this.barButtonItem62.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem62.ImageOptions.Image")));
            this.barButtonItem62.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barButtonItem62.ImageOptions.LargeImage")));
            this.barButtonItem62.Name = "barButtonItem62";
            // 
            // barButtonItem63
            // 
            this.barButtonItem63.Id = 110;
            this.barButtonItem63.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem63.ImageOptions.Image")));
            this.barButtonItem63.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barButtonItem63.ImageOptions.LargeImage")));
            this.barButtonItem63.Name = "barButtonItem63";
            // 
            // barButtonItem64
            // 
            this.barButtonItem64.Id = 111;
            this.barButtonItem64.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem64.ImageOptions.Image")));
            this.barButtonItem64.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barButtonItem64.ImageOptions.LargeImage")));
            this.barButtonItem64.Name = "barButtonItem64";
            // 
            // bar3
            // 
            this.bar3.BarAppearance.Hovered.BackColor = System.Drawing.Color.Red;
            this.bar3.BarAppearance.Hovered.BackColor2 = System.Drawing.Color.Red;
            this.bar3.BarAppearance.Hovered.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.bar3.BarAppearance.Hovered.Options.UseBackColor = true;
            this.bar3.BarAppearance.Hovered.Options.UseBorderColor = true;
            this.bar3.BarAppearance.Normal.BackColor = System.Drawing.Color.White;
            this.bar3.BarAppearance.Normal.Options.UseBackColor = true;
            this.bar3.BarName = "MainMenu";
            this.bar3.DockCol = 0;
            this.bar3.DockRow = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar3.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barSubItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.barSubItem7),
            new DevExpress.XtraBars.LinkPersistInfo(this.barSubItem8),
            new DevExpress.XtraBars.LinkPersistInfo(this.barSubItem9)});
            this.bar3.OptionsBar.AllowQuickCustomization = false;
            this.bar3.OptionsBar.DisableClose = true;
            this.bar3.OptionsBar.DisableCustomization = true;
            this.bar3.OptionsBar.DrawDragBorder = false;
            this.bar3.OptionsBar.RotateWhenVertical = false;
            this.bar3.OptionsBar.UseWholeRow = true;
            this.bar3.Text = "Main menu";
            // 
            // barSubItem1
            // 
            this.barSubItem1.Caption = "&File";
            this.barSubItem1.Id = 2;
            this.barSubItem1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.BtnSignOut),
            new DevExpress.XtraBars.LinkPersistInfo(this.barSubItemNew, true),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.barButtonItem3, DevExpress.XtraBars.BarItemPaintStyle.Standard),
            new DevExpress.XtraBars.LinkPersistInfo(this.barSubItem2),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem5),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem1, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem2),
            new DevExpress.XtraBars.LinkPersistInfo(this.barSubItem3, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.barSubItem4),
            new DevExpress.XtraBars.LinkPersistInfo(this.barSubItem5, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.barSubItem6, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem6, true)});
            this.barSubItem1.Name = "barSubItem1";
            // 
            // BtnSignOut
            // 
            this.BtnSignOut.Border = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.BtnSignOut.Caption = "Sign &Out";
            this.BtnSignOut.Id = 10;
            this.BtnSignOut.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("BtnSignOut.ImageOptions.Image")));
            this.BtnSignOut.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("BtnSignOut.ImageOptions.LargeImage")));
            this.BtnSignOut.Name = "BtnSignOut";
            // 
            // barSubItemNew
            // 
            this.barSubItemNew.Caption = "&New";
            this.barSubItemNew.Id = 14;
            this.barSubItemNew.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barSubItemNew.ImageOptions.Image")));
            this.barSubItemNew.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barSubItemNew.ImageOptions.LargeImage")));
            this.barSubItemNew.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.btnWorkBook),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnVoyageSheet),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnCargoReletSheet),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnTimeCharterSheet)});
            this.barSubItemNew.Name = "barSubItemNew";
            // 
            // btnWorkBook
            // 
            this.btnWorkBook.Caption = "&WorkBook";
            this.btnWorkBook.Id = 15;
            this.btnWorkBook.Name = "btnWorkBook";
            // 
            // btnVoyageSheet
            // 
            this.btnVoyageSheet.Caption = "&Voyage Sheet";
            this.btnVoyageSheet.Id = 16;
            this.btnVoyageSheet.Name = "btnVoyageSheet";
            // 
            // btnCargoReletSheet
            // 
            this.btnCargoReletSheet.Caption = "&Cargo Relet Sheet";
            this.btnCargoReletSheet.Id = 17;
            this.btnCargoReletSheet.Name = "btnCargoReletSheet";
            // 
            // btnTimeCharterSheet
            // 
            this.btnTimeCharterSheet.Caption = "&Time Charter Sheet";
            this.btnTimeCharterSheet.Id = 18;
            this.btnTimeCharterSheet.Name = "btnTimeCharterSheet";
            // 
            // barButtonItem3
            // 
            this.barButtonItem3.Caption = "Open &WorkBook     Ctrl+O";
            this.barButtonItem3.Id = 11;
            this.barButtonItem3.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem3.ImageOptions.Image")));
            this.barButtonItem3.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barButtonItem3.ImageOptions.LargeImage")));
            this.barButtonItem3.Name = "barButtonItem3";
            // 
            // barSubItem2
            // 
            this.barSubItem2.Caption = "Open Recent Files";
            this.barSubItem2.Id = 19;
            this.barSubItem2.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barSubItem2.ImageOptions.Image")));
            this.barSubItem2.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barSubItem2.ImageOptions.LargeImage")));
            this.barSubItem2.Name = "barSubItem2";
            // 
            // barButtonItem5
            // 
            this.barButtonItem5.Caption = "&Close WorkBook";
            this.barButtonItem5.Id = 13;
            this.barButtonItem5.Name = "barButtonItem5";
            // 
            // barButtonItem1
            // 
            this.barButtonItem1.Caption = "&Save";
            this.barButtonItem1.Id = 23;
            this.barButtonItem1.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem1.ImageOptions.Image")));
            this.barButtonItem1.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barButtonItem1.ImageOptions.LargeImage")));
            this.barButtonItem1.Name = "barButtonItem1";
            // 
            // barButtonItem2
            // 
            this.barButtonItem2.Caption = "Save &As ...";
            this.barButtonItem2.Id = 24;
            this.barButtonItem2.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem2.ImageOptions.Image")));
            this.barButtonItem2.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barButtonItem2.ImageOptions.LargeImage")));
            this.barButtonItem2.Name = "barButtonItem2";
            // 
            // barSubItem3
            // 
            this.barSubItem3.Caption = "Export to &Excel";
            this.barSubItem3.Id = 26;
            this.barSubItem3.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barSubItem3.ImageOptions.Image")));
            this.barSubItem3.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barSubItem3.ImageOptions.LargeImage")));
            this.barSubItem3.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem7),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem8)});
            this.barSubItem3.Name = "barSubItem3";
            // 
            // barButtonItem7
            // 
            this.barButtonItem7.Caption = "Export to Excel (&Portrait)";
            this.barButtonItem7.Id = 33;
            this.barButtonItem7.Name = "barButtonItem7";
            // 
            // barButtonItem8
            // 
            this.barButtonItem8.Caption = "Export to Excel (&Landscape)";
            this.barButtonItem8.Id = 34;
            this.barButtonItem8.Name = "barButtonItem8";
            // 
            // barSubItem4
            // 
            this.barSubItem4.Caption = "Expert to &PDF";
            this.barSubItem4.Id = 29;
            this.barSubItem4.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barSubItem4.ImageOptions.Image")));
            this.barSubItem4.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barSubItem4.ImageOptions.LargeImage")));
            this.barSubItem4.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem9),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem10)});
            this.barSubItem4.Name = "barSubItem4";
            // 
            // barButtonItem9
            // 
            this.barButtonItem9.Caption = "Export to PDF (&Portrait)";
            this.barButtonItem9.Id = 35;
            this.barButtonItem9.Name = "barButtonItem9";
            // 
            // barButtonItem10
            // 
            this.barButtonItem10.Caption = "Export to PDF (&Landscape)";
            this.barButtonItem10.Id = 36;
            this.barButtonItem10.Name = "barButtonItem10";
            // 
            // barSubItem5
            // 
            this.barSubItem5.Caption = "Send to &E-mail";
            this.barSubItem5.Id = 30;
            this.barSubItem5.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barSubItem5.ImageOptions.Image")));
            this.barSubItem5.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barSubItem5.ImageOptions.LargeImage")));
            this.barSubItem5.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem11),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem12),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem13),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem14)});
            this.barSubItem5.Name = "barSubItem5";
            // 
            // barButtonItem11
            // 
            this.barButtonItem11.Caption = "As Workbook file &attached";
            this.barButtonItem11.Id = 37;
            this.barButtonItem11.Name = "barButtonItem11";
            // 
            // barButtonItem12
            // 
            this.barButtonItem12.Caption = "As &Sheep file attached";
            this.barButtonItem12.Id = 38;
            this.barButtonItem12.Name = "barButtonItem12";
            // 
            // barButtonItem13
            // 
            this.barButtonItem13.Caption = "As &Excel file attached";
            this.barButtonItem13.Id = 39;
            this.barButtonItem13.Name = "barButtonItem13";
            // 
            // barButtonItem14
            // 
            this.barButtonItem14.Caption = "As &PDF file attached";
            this.barButtonItem14.Id = 40;
            this.barButtonItem14.Name = "barButtonItem14";
            // 
            // barSubItem6
            // 
            this.barSubItem6.Caption = "Print &Preview";
            this.barSubItem6.Id = 31;
            this.barSubItem6.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barSubItem6.ImageOptions.Image")));
            this.barSubItem6.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barSubItem6.ImageOptions.LargeImage")));
            this.barSubItem6.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem15),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem16)});
            this.barSubItem6.Name = "barSubItem6";
            // 
            // barButtonItem15
            // 
            this.barButtonItem15.Caption = "Print Preview (&Portrait)";
            this.barButtonItem15.Id = 41;
            this.barButtonItem15.Name = "barButtonItem15";
            // 
            // barButtonItem16
            // 
            this.barButtonItem16.Caption = "Print Preview (&Landscape)";
            this.barButtonItem16.Id = 42;
            this.barButtonItem16.Name = "barButtonItem16";
            // 
            // barButtonItem6
            // 
            this.barButtonItem6.Caption = "E&xit (Estimator Close)";
            this.barButtonItem6.Id = 32;
            this.barButtonItem6.Name = "barButtonItem6";
            // 
            // barSubItem7
            // 
            this.barSubItem7.Caption = "&View";
            this.barSubItem7.Id = 43;
            this.barSubItem7.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem17),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem18),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem19)});
            this.barSubItem7.Name = "barSubItem7";
            // 
            // barButtonItem17
            // 
            this.barButtonItem17.Caption = "&Reload          F5";
            this.barButtonItem17.Id = 44;
            this.barButtonItem17.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem17.ImageOptions.Image")));
            this.barButtonItem17.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barButtonItem17.ImageOptions.LargeImage")));
            this.barButtonItem17.Name = "barButtonItem17";
            // 
            // barButtonItem18
            // 
            this.barButtonItem18.Caption = "&Undo       Ctrl+Z";
            this.barButtonItem18.Id = 45;
            this.barButtonItem18.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem18.ImageOptions.Image")));
            this.barButtonItem18.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barButtonItem18.ImageOptions.LargeImage")));
            this.barButtonItem18.Name = "barButtonItem18";
            // 
            // barButtonItem19
            // 
            this.barButtonItem19.Caption = "Re&do       Ctrl+Y";
            this.barButtonItem19.Id = 46;
            this.barButtonItem19.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem19.ImageOptions.Image")));
            this.barButtonItem19.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barButtonItem19.ImageOptions.LargeImage")));
            this.barButtonItem19.Name = "barButtonItem19";
            // 
            // barSubItem8
            // 
            this.barSubItem8.Caption = "&Tools";
            this.barSubItem8.Id = 47;
            this.barSubItem8.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem20),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem21, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem22),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem23),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem24, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem25, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem26, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem27),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem28, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem29, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem30, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem31, true)});
            this.barSubItem8.Name = "barSubItem8";
            // 
            // barButtonItem20
            // 
            this.barButtonItem20.Caption = "V&essel Manager";
            this.barButtonItem20.Id = 48;
            this.barButtonItem20.Name = "barButtonItem20";
            // 
            // barButtonItem21
            // 
            this.barButtonItem21.Caption = "&Result Plus";
            this.barButtonItem21.Id = 49;
            this.barButtonItem21.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem21.ImageOptions.Image")));
            this.barButtonItem21.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barButtonItem21.ImageOptions.LargeImage")));
            this.barButtonItem21.Name = "barButtonItem21";
            // 
            // barButtonItem22
            // 
            this.barButtonItem22.Caption = "Estimation &Analyzer";
            this.barButtonItem22.Id = 50;
            this.barButtonItem22.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem22.ImageOptions.Image")));
            this.barButtonItem22.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barButtonItem22.ImageOptions.LargeImage")));
            this.barButtonItem22.Name = "barButtonItem22";
            // 
            // barButtonItem23
            // 
            this.barButtonItem23.Caption = "Remark";
            this.barButtonItem23.Id = 51;
            this.barButtonItem23.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem23.ImageOptions.Image")));
            this.barButtonItem23.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barButtonItem23.ImageOptions.LargeImage")));
            this.barButtonItem23.Name = "barButtonItem23";
            // 
            // barButtonItem24
            // 
            this.barButtonItem24.Caption = "&Loadable Qunatity Calculator";
            this.barButtonItem24.Id = 52;
            this.barButtonItem24.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem24.ImageOptions.Image")));
            this.barButtonItem24.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barButtonItem24.ImageOptions.LargeImage")));
            this.barButtonItem24.Name = "barButtonItem24";
            // 
            // barButtonItem25
            // 
            this.barButtonItem25.Caption = "Anti Piracy Route";
            this.barButtonItem25.Id = 53;
            this.barButtonItem25.Name = "barButtonItem25";
            // 
            // barButtonItem26
            // 
            this.barButtonItem26.Caption = "Bunker Simulator";
            this.barButtonItem26.Id = 54;
            this.barButtonItem26.Name = "barButtonItem26";
            // 
            // barButtonItem27
            // 
            this.barButtonItem27.Caption = "Bunker Index";
            this.barButtonItem27.Id = 55;
            this.barButtonItem27.Name = "barButtonItem27";
            // 
            // barButtonItem28
            // 
            this.barButtonItem28.Caption = "Netpas Weather Service";
            this.barButtonItem28.Id = 56;
            this.barButtonItem28.Name = "barButtonItem28";
            // 
            // barButtonItem29
            // 
            this.barButtonItem29.Caption = "&Workbook Result";
            this.barButtonItem29.Id = 57;
            this.barButtonItem29.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem29.ImageOptions.Image")));
            this.barButtonItem29.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barButtonItem29.ImageOptions.LargeImage")));
            this.barButtonItem29.Name = "barButtonItem29";
            // 
            // barButtonItem30
            // 
            this.barButtonItem30.Caption = "Port &Update";
            this.barButtonItem30.Id = 58;
            this.barButtonItem30.Name = "barButtonItem30";
            // 
            // barButtonItem31
            // 
            this.barButtonItem31.Caption = "&Options";
            this.barButtonItem31.Id = 59;
            this.barButtonItem31.Name = "barButtonItem31";
            // 
            // barSubItem9
            // 
            this.barSubItem9.Caption = "&Help";
            this.barSubItem9.Id = 61;
            this.barSubItem9.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem32),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem33, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem34),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem35, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem36, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem37),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem38),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem39),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem40),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem41, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem42, true)});
            this.barSubItem9.Name = "barSubItem9";
            // 
            // barButtonItem32
            // 
            this.barButtonItem32.Caption = "&Help";
            this.barButtonItem32.Id = 62;
            this.barButtonItem32.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem32.ImageOptions.Image")));
            this.barButtonItem32.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barButtonItem32.ImageOptions.LargeImage")));
            this.barButtonItem32.Name = "barButtonItem32";
            // 
            // barButtonItem33
            // 
            this.barButtonItem33.Caption = "MY &Account";
            this.barButtonItem33.Id = 63;
            this.barButtonItem33.Name = "barButtonItem33";
            // 
            // barButtonItem34
            // 
            this.barButtonItem34.Caption = "&Register License Key";
            this.barButtonItem34.Id = 64;
            this.barButtonItem34.Name = "barButtonItem34";
            // 
            // barButtonItem35
            // 
            this.barButtonItem35.Caption = "Check For Updates";
            this.barButtonItem35.Id = 65;
            this.barButtonItem35.Name = "barButtonItem35";
            // 
            // barButtonItem36
            // 
            this.barButtonItem36.Caption = "&Purchase ...";
            this.barButtonItem36.Id = 66;
            this.barButtonItem36.Name = "barButtonItem36";
            // 
            // barButtonItem37
            // 
            this.barButtonItem37.Caption = "&FAQ";
            this.barButtonItem37.Id = 67;
            this.barButtonItem37.Name = "barButtonItem37";
            // 
            // barButtonItem38
            // 
            this.barButtonItem38.Caption = "&Quick Help";
            this.barButtonItem38.Id = 68;
            this.barButtonItem38.Name = "barButtonItem38";
            // 
            // barButtonItem39
            // 
            this.barButtonItem39.Caption = "&Contact Us";
            this.barButtonItem39.Id = 69;
            this.barButtonItem39.Name = "barButtonItem39";
            // 
            // barButtonItem40
            // 
            this.barButtonItem40.Caption = "Go to &Homepage";
            this.barButtonItem40.Id = 70;
            this.barButtonItem40.Name = "barButtonItem40";
            // 
            // barButtonItem41
            // 
            this.barButtonItem41.Caption = "&Send Comments";
            this.barButtonItem41.Id = 71;
            this.barButtonItem41.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem41.ImageOptions.Image")));
            this.barButtonItem41.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barButtonItem41.ImageOptions.LargeImage")));
            this.barButtonItem41.Name = "barButtonItem41";
            // 
            // barButtonItem42
            // 
            this.barButtonItem42.Caption = "About Netpas Estimator 4.0";
            this.barButtonItem42.Id = 72;
            this.barButtonItem42.Name = "barButtonItem42";
            // 
            // bar5
            // 
            this.bar5.BarName = "Status bar";
            this.bar5.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar5.DockCol = 0;
            this.bar5.DockRow = 0;
            this.bar5.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar5.OptionsBar.AllowQuickCustomization = false;
            this.bar5.OptionsBar.DrawDragBorder = false;
            this.bar5.OptionsBar.UseWholeRow = true;
            this.bar5.Text = "Status bar";
            // 
            // barAndDockingController1
            // 
            this.barAndDockingController1.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.UltraFlat;
            this.barAndDockingController1.LookAndFeel.UseDefaultLookAndFeel = false;
            this.barAndDockingController1.PropertiesDocking.ViewStyle = DevExpress.XtraBars.Docking2010.Views.DockingViewStyle.Classic;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Manager = this.barManager1;
            this.barDockControlTop.Size = new System.Drawing.Size(1688, 48);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 1086);
            this.barDockControlBottom.Manager = this.barManager1;
            this.barDockControlBottom.Size = new System.Drawing.Size(1688, 20);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 48);
            this.barDockControlLeft.Manager = this.barManager1;
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 1038);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(1688, 48);
            this.barDockControlRight.Manager = this.barManager1;
            this.barDockControlRight.Size = new System.Drawing.Size(0, 1038);
            // 
            // barListItem1
            // 
            this.barListItem1.Caption = "barListItem1";
            this.barListItem1.Id = 0;
            this.barListItem1.Name = "barListItem1";
            this.barListItem1.Strings.AddRange(new object[] {
            "test1",
            "test2",
            "teset5"});
            // 
            // barLinkContainerItem1
            // 
            this.barLinkContainerItem1.Caption = "barLinkContainerItem1";
            this.barLinkContainerItem1.Id = 1;
            this.barLinkContainerItem1.Name = "barLinkContainerItem1";
            // 
            // barToolbarsListItem1
            // 
            this.barToolbarsListItem1.Caption = "barToolbarsListItem1";
            this.barToolbarsListItem1.Id = 3;
            this.barToolbarsListItem1.Name = "barToolbarsListItem1";
            // 
            // barButtonItem4
            // 
            this.barButtonItem4.Caption = "barButtonItem4";
            this.barButtonItem4.Id = 25;
            this.barButtonItem4.Name = "barButtonItem4";
            // 
            // barCheckItem1
            // 
            this.barCheckItem1.Caption = "barCheckItem1";
            this.barCheckItem1.Id = 78;
            this.barCheckItem1.Name = "barCheckItem1";
            // 
            // barDockControl1
            // 
            this.barDockControl1.CausesValidation = false;
            this.barDockControl1.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControl1.Location = new System.Drawing.Point(0, 48);
            this.barDockControl1.Manager = null;
            this.barDockControl1.Size = new System.Drawing.Size(0, 1038);
            // 
            // barDockControl2
            // 
            this.barDockControl2.CausesValidation = false;
            this.barDockControl2.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControl2.Location = new System.Drawing.Point(0, 48);
            this.barDockControl2.Manager = null;
            this.barDockControl2.Size = new System.Drawing.Size(1688, 0);
            // 
            // barDockControl3
            // 
            this.barDockControl3.CausesValidation = false;
            this.barDockControl3.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControl3.Location = new System.Drawing.Point(0, 48);
            this.barDockControl3.Manager = null;
            this.barDockControl3.Size = new System.Drawing.Size(0, 1038);
            // 
            // barDockControl4
            // 
            this.barDockControl4.CausesValidation = false;
            this.barDockControl4.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControl4.Location = new System.Drawing.Point(0, 48);
            this.barDockControl4.Manager = null;
            this.barDockControl4.Size = new System.Drawing.Size(1688, 0);
            // 
            // barDockControl5
            // 
            this.barDockControl5.CausesValidation = false;
            this.barDockControl5.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControl5.Location = new System.Drawing.Point(0, 48);
            this.barDockControl5.Manager = null;
            this.barDockControl5.Size = new System.Drawing.Size(0, 1038);
            // 
            // barDockControl6
            // 
            this.barDockControl6.CausesValidation = false;
            this.barDockControl6.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControl6.Location = new System.Drawing.Point(0, 48);
            this.barDockControl6.Manager = null;
            this.barDockControl6.Size = new System.Drawing.Size(1688, 0);
            // 
            // barDockControl7
            // 
            this.barDockControl7.CausesValidation = false;
            this.barDockControl7.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControl7.Location = new System.Drawing.Point(0, 48);
            this.barDockControl7.Manager = null;
            this.barDockControl7.Size = new System.Drawing.Size(0, 1038);
            // 
            // barDockControl8
            // 
            this.barDockControl8.CausesValidation = false;
            this.barDockControl8.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControl8.Location = new System.Drawing.Point(0, 48);
            this.barDockControl8.Manager = null;
            this.barDockControl8.Size = new System.Drawing.Size(1688, 0);
            // 
            // barDockControl9
            // 
            this.barDockControl9.CausesValidation = false;
            this.barDockControl9.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControl9.Location = new System.Drawing.Point(0, 48);
            this.barDockControl9.Manager = null;
            this.barDockControl9.Size = new System.Drawing.Size(0, 1038);
            // 
            // barDockControl10
            // 
            this.barDockControl10.CausesValidation = false;
            this.barDockControl10.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControl10.Location = new System.Drawing.Point(0, 48);
            this.barDockControl10.Manager = null;
            this.barDockControl10.Size = new System.Drawing.Size(1688, 0);
            // 
            // barDockControl11
            // 
            this.barDockControl11.CausesValidation = false;
            this.barDockControl11.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControl11.Location = new System.Drawing.Point(0, 48);
            this.barDockControl11.Manager = null;
            this.barDockControl11.Size = new System.Drawing.Size(0, 1038);
            // 
            // barDockControl12
            // 
            this.barDockControl12.CausesValidation = false;
            this.barDockControl12.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControl12.Location = new System.Drawing.Point(0, 48);
            this.barDockControl12.Manager = null;
            this.barDockControl12.Size = new System.Drawing.Size(1688, 0);
            // 
            // xtraTabControl1
            // 
            this.xtraTabControl1.ClosePageButtonShowMode = DevExpress.XtraTab.ClosePageButtonShowMode.InActiveTabPageHeaderAndOnMouseHover;
            editorButtonImageOptions1.Image = ((System.Drawing.Image)(resources.GetObject("editorButtonImageOptions1.Image")));
            editorButtonImageOptions2.Image = ((System.Drawing.Image)(resources.GetObject("editorButtonImageOptions2.Image")));
            editorButtonImageOptions3.Image = ((System.Drawing.Image)(resources.GetObject("editorButtonImageOptions3.Image")));
            this.xtraTabControl1.CustomHeaderButtons.AddRange(new DevExpress.XtraTab.Buttons.CustomHeaderButton[] {
            new DevExpress.XtraTab.Buttons.CustomHeaderButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, editorButtonImageOptions1, serializableAppearanceObject1, "", null, null),
            new DevExpress.XtraTab.Buttons.CustomHeaderButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, editorButtonImageOptions2, serializableAppearanceObject2, "", null, null),
            new DevExpress.XtraTab.Buttons.CustomHeaderButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, editorButtonImageOptions3, serializableAppearanceObject3, "", null, null)});
            this.xtraTabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraTabControl1.Location = new System.Drawing.Point(0, 48);
            this.xtraTabControl1.LookAndFeel.UseDefaultLookAndFeel = false;
            this.xtraTabControl1.Name = "xtraTabControl1";
            this.xtraTabControl1.SelectedTabPage = this.xtraTabPage1;
            this.xtraTabControl1.ShowTabHeader = DevExpress.Utils.DefaultBoolean.True;
            this.xtraTabControl1.Size = new System.Drawing.Size(1688, 1038);
            this.xtraTabControl1.TabIndex = 16;
            this.xtraTabControl1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPage1,
            this.xtraTabPage2,
            this.xtraTabPage3,
            this.xtraTabPage4});
            this.xtraTabControl1.TabStop = false;
            // 
            // xtraTabPage1
            // 
            this.xtraTabPage1.Controls.Add(this.voyage1);
            this.xtraTabPage1.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("xtraTabPage1.ImageOptions.Image")));
            this.xtraTabPage1.Name = "xtraTabPage1";
            this.xtraTabPage1.Size = new System.Drawing.Size(1682, 1007);
            this.xtraTabPage1.Text = "voyage1";
            // 
            // voyage1
            // 
            this.voyage1.AutoScroll = true;
            this.voyage1.AutoScrollMargin = new System.Drawing.Size(5, 5);
            this.voyage1.AutoScrollMinSize = new System.Drawing.Size(1600, 1000);
            this.voyage1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.voyage1.Location = new System.Drawing.Point(0, 0);
            this.voyage1.Name = "voyage1";
            this.voyage1.Size = new System.Drawing.Size(1682, 1007);
            this.voyage1.TabIndex = 0;
            this.voyage1.Load += new System.EventHandler(this.voyage1_Load);
            // 
            // xtraTabPage2
            // 
            this.xtraTabPage2.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("xtraTabPage2.ImageOptions.Image")));
            this.xtraTabPage2.Name = "xtraTabPage2";
            this.xtraTabPage2.Size = new System.Drawing.Size(1682, 1007);
            this.xtraTabPage2.Text = "voyage2";
            // 
            // xtraTabPage3
            // 
            this.xtraTabPage3.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("xtraTabPage3.ImageOptions.Image")));
            this.xtraTabPage3.Name = "xtraTabPage3";
            this.xtraTabPage3.Size = new System.Drawing.Size(1682, 1007);
            this.xtraTabPage3.Text = "cargo relet1";
            // 
            // xtraTabPage4
            // 
            this.xtraTabPage4.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("xtraTabPage4.ImageOptions.Image")));
            this.xtraTabPage4.Name = "xtraTabPage4";
            this.xtraTabPage4.Size = new System.Drawing.Size(1682, 1007);
            this.xtraTabPage4.Text = "time charter1";
            // 
            // SimpleEstimatorMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(1688, 1106);
            this.Controls.Add(this.xtraTabControl1);
            this.Controls.Add(this.barDockControl1);
            this.Controls.Add(this.barDockControl2);
            this.Controls.Add(this.barDockControl3);
            this.Controls.Add(this.barDockControl4);
            this.Controls.Add(this.barDockControl5);
            this.Controls.Add(this.barDockControl6);
            this.Controls.Add(this.barDockControl7);
            this.Controls.Add(this.barDockControl8);
            this.Controls.Add(this.barDockControl9);
            this.Controls.Add(this.barDockControl10);
            this.Controls.Add(this.barDockControl11);
            this.Controls.Add(this.barDockControl12);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Name = "SimpleEstimatorMain";
            this.Text = "Netpas Simple Estimator";
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barAndDockingController1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).EndInit();
            this.xtraTabControl1.ResumeLayout(false);
            this.xtraTabPage1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.Bar bar4;
        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.Bar bar3;
        private DevExpress.XtraBars.BarListItem barListItem1;
        private DevExpress.XtraBars.BarLinkContainerItem barLinkContainerItem1;
        private DevExpress.XtraBars.BarSubItem barSubItem1;
        private DevExpress.XtraBars.Bar bar5;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarToolbarsListItem barToolbarsListItem1;
        private DevExpress.XtraBars.BarButtonItem BtnSignOut;
        private DevExpress.XtraBars.BarButtonItem barButtonItem3;
        private DevExpress.XtraBars.BarSubItem barSubItemNew;
        private DevExpress.XtraBars.BarButtonItem btnWorkBook;
        private DevExpress.XtraBars.BarButtonItem btnVoyageSheet;
        private DevExpress.XtraBars.BarButtonItem btnCargoReletSheet;
        private DevExpress.XtraBars.BarButtonItem btnTimeCharterSheet;
        private DevExpress.XtraBars.BarButtonItem barButtonItem5;
        private DevExpress.XtraBars.BarSubItem barSubItem2;
        private DevExpress.XtraBars.BarDockControl barDockControl1;
        private DevExpress.XtraBars.BarDockControl barDockControl2;
        private DevExpress.XtraBars.BarDockControl barDockControl3;
        private DevExpress.XtraBars.BarDockControl barDockControl4;
        private DevExpress.XtraBars.BarDockControl barDockControl5;
        private DevExpress.XtraBars.BarDockControl barDockControl6;
        private DevExpress.XtraBars.BarDockControl barDockControl7;
        private DevExpress.XtraBars.BarDockControl barDockControl8;
        private DevExpress.XtraBars.BarDockControl barDockControl9;
        private DevExpress.XtraBars.BarDockControl barDockControl10;
        private DevExpress.XtraBars.BarDockControl barDockControl11;
        private DevExpress.XtraBars.BarDockControl barDockControl12;
        private DevExpress.XtraBars.BarButtonItem barButtonItem1;
        private DevExpress.XtraBars.BarButtonItem barButtonItem2;
        private DevExpress.XtraBars.BarSubItem barSubItem3;
        private DevExpress.XtraBars.BarButtonItem barButtonItem4;
        private DevExpress.XtraBars.BarSubItem barSubItem4;
        private DevExpress.XtraBars.BarSubItem barSubItem5;
        private DevExpress.XtraBars.BarSubItem barSubItem6;
        private DevExpress.XtraBars.BarButtonItem barButtonItem6;
        private DevExpress.XtraBars.BarButtonItem barButtonItem7;
        private DevExpress.XtraBars.BarButtonItem barButtonItem8;
        private DevExpress.XtraBars.BarButtonItem barButtonItem9;
        private DevExpress.XtraBars.BarButtonItem barButtonItem10;
        private DevExpress.XtraBars.BarButtonItem barButtonItem11;
        private DevExpress.XtraBars.BarButtonItem barButtonItem12;
        private DevExpress.XtraBars.BarButtonItem barButtonItem13;
        private DevExpress.XtraBars.BarButtonItem barButtonItem14;
        private DevExpress.XtraBars.BarButtonItem barButtonItem15;
        private DevExpress.XtraBars.BarButtonItem barButtonItem16;
        private DevExpress.XtraBars.BarSubItem barSubItem7;
        private DevExpress.XtraBars.BarButtonItem barButtonItem17;
        private DevExpress.XtraBars.BarButtonItem barButtonItem18;
        private DevExpress.XtraBars.BarButtonItem barButtonItem19;
        private DevExpress.XtraBars.BarSubItem barSubItem8;
        private DevExpress.XtraBars.BarButtonItem barButtonItem20;
        private DevExpress.XtraBars.BarButtonItem barButtonItem21;
        private DevExpress.XtraBars.BarButtonItem barButtonItem22;
        private DevExpress.XtraBars.BarButtonItem barButtonItem23;
        private DevExpress.XtraBars.BarButtonItem barButtonItem24;
        private DevExpress.XtraBars.BarButtonItem barButtonItem25;
        private DevExpress.XtraBars.BarButtonItem barButtonItem26;
        private DevExpress.XtraBars.BarButtonItem barButtonItem27;
        private DevExpress.XtraBars.BarButtonItem barButtonItem28;
        private DevExpress.XtraBars.BarButtonItem barButtonItem29;
        private DevExpress.XtraBars.BarButtonItem barButtonItem30;
        private DevExpress.XtraBars.BarButtonItem barButtonItem31;
        private DevExpress.XtraBars.BarSubItem barSubItem9;
        private DevExpress.XtraBars.BarButtonItem barButtonItem32;
        private DevExpress.XtraBars.BarButtonItem barButtonItem33;
        private DevExpress.XtraBars.BarButtonItem barButtonItem34;
        private DevExpress.XtraBars.BarButtonItem barButtonItem35;
        private DevExpress.XtraBars.BarButtonItem barButtonItem36;
        private DevExpress.XtraBars.BarButtonItem barButtonItem37;
        private DevExpress.XtraBars.BarButtonItem barButtonItem38;
        private DevExpress.XtraBars.BarButtonItem barButtonItem39;
        private DevExpress.XtraBars.BarButtonItem barButtonItem40;
        private DevExpress.XtraBars.BarButtonItem barButtonItem41;
        private DevExpress.XtraBars.BarButtonItem barButtonItem42;
        private DevExpress.XtraBars.BarAndDockingController barAndDockingController1;
        private DevExpress.XtraBars.BarCheckItem barCheckItem1;
        private DevExpress.XtraBars.BarSubItem barSubItem10;
        private DevExpress.XtraBars.BarButtonItem barButtonItem43;
        private DevExpress.XtraBars.BarButtonItem barButtonItem44;
        private DevExpress.XtraBars.BarButtonItem barButtonItem45;
        private DevExpress.XtraBars.BarButtonItem barButtonItem46;
        private DevExpress.XtraBars.BarButtonItem barButtonItem47;
        private DevExpress.XtraBars.BarSubItem barSubItem11;
        private DevExpress.XtraBars.BarButtonItem barButtonItem48;
        private DevExpress.XtraBars.BarButtonItem barButtonItem49;
        private DevExpress.XtraBars.BarSubItem barSubItem12;
        private DevExpress.XtraBars.BarSubItem barSubItem13;
        private DevExpress.XtraBars.BarSubItem barSubItem14;
        private DevExpress.XtraBars.BarSubItem barSubItem15;
        private DevExpress.XtraBars.BarButtonItem barButtonItem50;
        private DevExpress.XtraBars.BarButtonItem barButtonItem51;
        private DevExpress.XtraBars.BarButtonItem barButtonItem52;
        private DevExpress.XtraBars.BarButtonItem barButtonItem53;
        private DevExpress.XtraBars.BarButtonItem barButtonItem54;
        private DevExpress.XtraBars.BarButtonItem barButtonItem55;
        private DevExpress.XtraBars.BarButtonItem barButtonItem56;
        private DevExpress.XtraBars.BarButtonItem barButtonItem57;
        private DevExpress.XtraBars.BarButtonItem barButtonItem58;
        private DevExpress.XtraBars.BarButtonItem barButtonItem60;
        private DevExpress.XtraBars.BarButtonItem barButtonItem59;
        private DevExpress.XtraBars.BarButtonItem barButtonItem61;
        private DevExpress.XtraBars.BarButtonItem barButtonItem62;
        private DevExpress.XtraBars.BarButtonItem barButtonItem63;
        private DevExpress.XtraBars.BarButtonItem barButtonItem64;
        private DevExpress.XtraTab.XtraTabControl xtraTabControl1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage2;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage4;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage3;
        private Views.voyage voyage1;
    }
}

