﻿using System;
using System.Collections.Generic;
using System.Linq;
using Model.DataSet;

namespace ViewModel
{
    public class DataControl : IDisposable
    {
        public DataControl(SEData Data)
        {
            DataSource = Data;
        }
        private Model.DataSet.SEData DataSource;
        private Service.Service service = new Service.Service();
        public void Dispose()
        {
            if (DataSource != null)
            {
                DataSource.Dispose();
                DataSource = null;
            }
        }// DataSource 
        //private SimpleEstimator.Views.voyage VoyageView = new SimpleEstimator.Views.voyage();
        // View

        public int getNextPortIndex(int CurrentIndex)
        {
            SEData.PortRDataTable table = DataSource.PortR;
            int NextPortIndex = 0;
            for (int i = CurrentIndex + 1; i < table.Rows.Count; i++)
            {
                if (table.Rows[i]["Name"].ToString() != "")
                {
                    NextPortIndex = i;
                    break;
                }
            }
            return NextPortIndex; // NextPortIndex == 0 일경우 해당하는 값을 찾지 못한것으로 간주한다.
        }
        #region Vessel
        //Validate
        public void VesselSpdValidate()
        {
            if (Convert.ToDouble(DataSource.VPSpd.Rows[0]["Ballast"]) > 99)
            {
                DataSource.VPSpd.Rows[0]["Ballast"] = 99;
            }
            if (Convert.ToDouble(DataSource.VPSpd.Rows[0]["Laden"]) > 99)
            {
                DataSource.VPSpd.Rows[0]["Laden"] = 99;
            }
        }
        //getSpeed
        public double getBallastSpeed()
        {
            if (DataSource.VPSpd.Rows[0]["Ballast"].ToString() != "")
            {
                return Convert.ToDouble(DataSource.VPSpd.Rows[0]["Ballast"]);
            }
            else
            {
                return 0;
            }
        }

        public double getLadenSpeed()
        {
            if (DataSource.VPSpd.Rows[0]["Laden"].ToString() != "")
            {
                return Convert.ToDouble(DataSource.VPSpd.Rows[0]["Laden"]);
            }
            else
            {
                return 0;
            }
        }
        //get Fussel
        public double getFOBallast()
        {   //FO
            if (DataSource.VPFO.Rows[0]["Ballast"].ToString() != "")
            {
                return Convert.ToDouble(DataSource.VPFO.Rows[0]["Ballast"]);
            }
            else
            {
                return 0;
            }
        }
        public double getFOLaden()
        {
            if (DataSource.VPFO.Rows[0]["Laden"].ToString() != "")
            {
                return Convert.ToDouble(DataSource.VPFO.Rows[0]["Laden"]);
            }
            else
            {
                return 0;
            }
        }
        public double getFOIdle()
        {
            if (DataSource.VPFO.Rows[0]["Idle"].ToString() != "")
            {
                return Convert.ToDouble(DataSource.VPFO.Rows[0]["Idle"]);
            }
            else
            {
                return 0;
            }
        }
        public double getFOWork()
        {
            if (DataSource.VPFO.Rows[0]["Work"].ToString() != "")
            {
                return Convert.ToDouble(DataSource.VPFO.Rows[0]["Work"]);
            }
            else
            {
                return 0;
            }
        }//LSFO
        public double getLSFOBallast()
        {
            if (DataSource.VPFO.Rows[1]["Ballast"].ToString() != "")
            {
                return Convert.ToDouble(DataSource.VPFO.Rows[1]["Ballast"]);
            }
            else
            {
                return 0;
            }
        }
        public double getLSFOLaden()
        {
            if (DataSource.VPFO.Rows[1]["Laden"].ToString() != "")
            {
                return Convert.ToDouble(DataSource.VPFO.Rows[1]["Laden"]);
            }
            else
            {
                return 0;
            }
        }
        public double getLSFOIdle()
        {
            if (DataSource.VPFO.Rows[1]["Idle"].ToString() != "")
            {
                return Convert.ToDouble(DataSource.VPFO.Rows[1]["Idle"]);
            }
            else
            {
                return 0;
            }
        }
        public double getLSFOWork()
        {
            if (DataSource.VPFO.Rows[1]["Work"].ToString() != "")
            {
                return Convert.ToDouble(DataSource.VPFO.Rows[1]["Work"]);
            }
            else
            {
                return 0;
            }
        }//DO
        public double getDOSea()
        {
            if (DataSource.VPDO.Rows[0]["Sea"].ToString() != "")
            {
                return Convert.ToDouble(DataSource.VPDO.Rows[0]["Sea"]);
            }
            else
            {
                return 0;
            }
        }
        public double getDOLaden()
        {
            if (DataSource.VPDO.Rows[0]["Laden"].ToString() != "")
            {
                return Convert.ToDouble(DataSource.VPDO.Rows[0]["Laden"]);
            }
            else
            {
                return 0;
            }
        }
        public double getDOIdle()
        {
            if (DataSource.VPDO.Rows[0]["Idle"].ToString() != "")
            {
                return Convert.ToDouble(DataSource.VPDO.Rows[0]["Idle"]);
            }
            else
            {
                return 0;
            }
        }
        public double getDOWork()
        {
            if (DataSource.VPDO.Rows[0]["Work"].ToString() != "")
            {
                return Convert.ToDouble(DataSource.VPDO.Rows[0]["Work"]);
            }
            else
            {
                return 0;
            }
        }//LSDO
        public double getLSDOSea()
        {
            if (DataSource.VPDO.Rows[1]["Sea"].ToString() != "")
            {
                return Convert.ToDouble(DataSource.VPDO.Rows[1]["Sea"]);
            }
            else
            {
                return 0;
            }
        }
        public double getLSDOLaden()
        {
            if (DataSource.VPDO.Rows[1]["Laden"].ToString() != "")
            {
                return Convert.ToDouble(DataSource.VPDO.Rows[1]["Laden"]);
            }
            else
            {
                return 0;
            }
        }
        public double getLSDOIdle()
        {
            if (DataSource.VPDO.Rows[1]["Idle"].ToString() != "")
            {
                return Convert.ToDouble(DataSource.VPDO.Rows[1]["Idle"]);
            }
            else
            {
                return 0;
            }
        }
        public double getLSDOWork()
        {
            if (DataSource.VPDO.Rows[1]["Work"].ToString() != "")
            {
                return Convert.ToDouble(DataSource.VPDO.Rows[1]["Work"]);
            }
            else
            {
                return 0;
            }

        }

        #endregion
        #region Cargo
        //Set
        public void CargoChanged()
        {
            Service.Service.CargoService cargoservice = new Service.Service.CargoService();
            double FrtSummary = 0;
            double ACommSummary = 0;
            double BrkgSummary = 0;
            double FrtTaxSummary = 0;
            List<double> Revenue = cargoservice.CalRevenue(GetQuantity(), GetFrt());
            FrtSummary = cargoservice.CalFrtSummary(GetQuantity(), GetFrt());
            ACommSummary = cargoservice.CalAcommSummary(Revenue, GetAComm());
            BrkgSummary = cargoservice.CalBrkgSummary(Revenue, GetBrkg());
            FrtTaxSummary = cargoservice.CalFrtTaxSummary(Revenue, GetFrtTax());

            CargoSet(FrtSummary, ACommSummary, BrkgSummary, FrtTaxSummary, Revenue);
        }
        
        public void CargoSet(double FrtSummary,double ACommSummary,double BrkgSummary,double FrtTaxSummary,List<double> Revenue)
        {
            for (int i = 0; i < DataSource.Cargo.Rows.Count; i++)
            {
                DataSource.Cargo.Rows[i]["Revenue"] = Revenue[i];
            }//RevenueSet
            DataSource.SummaryValues.Rows[0]["Acomm"] = ACommSummary;
            DataSource.SummaryValues.Rows[0]["Frt"] = FrtSummary;
            DataSource.SummaryValues.Rows[0]["Brkg"] = BrkgSummary;
            DataSource.SummaryValues.Rows[0]["FrtTax"] =FrtTaxSummary;
        }
        
        // Cargo GET
        public List<double> GetRevenue()
        {
            SEData.CargoDataTable Cargo = DataSource.Cargo;
            List<double> revenue = new List<double>();
            for (int i = 0; i < Cargo.Rows.Count ; i++)
            {
                if (Cargo.Rows[i]["Revenue"].ToString()!= "")
                {
                    revenue.Add(Convert.ToDouble(Cargo.Rows[i]["Revenue"]));
                }
                else
                {
                    revenue.Add(0);
                }
            }
            return revenue;
        }
        public List<double> GetAComm()
        {
            SEData.CargoDataTable Cargo = DataSource.Cargo;
            List<double> AComm = new List<double>();
            for (int i = 0; i < Cargo.Rows.Count; i++)
            {
                if (Cargo.Rows[i]["AComm"].ToString() != "")
                {
                    if (Convert.ToDouble(Cargo.Rows[i]["AComm"]) < 99)
                        AComm.Add(Convert.ToDouble(Cargo.Rows[i]["AComm"]));
                    else
                        AComm.Add(99);                   
                }
                else
                {
                    AComm.Add(0);
                }
            }
            return AComm;
        }
        public List<double> GetBrkg()
        {
            SEData.CargoDataTable Cargo = DataSource.Cargo;
            List<double> Brkg = new List<double>();
            for (int i = 0; i < Cargo.Rows.Count; i++)
            {
                if (Cargo.Rows[i]["Brkg"].ToString() != "")
                {
                    if (Convert.ToDouble(Cargo.Rows[i]["Brkg"]) < 99)
                        Brkg.Add(Convert.ToDouble(Cargo.Rows[i]["Brkg"]));
                    else
                        Brkg.Add(99);                    
                }
                else
                {
                    Brkg.Add(0);
                }
            }
            return Brkg;
        }
        public List<double> GetFrtTax()
        {
            SEData.CargoDataTable Cargo = DataSource.Cargo;
            List<double> FrtTax = new List<double>();
            for (int i = 0; i < Cargo.Rows.Count; i++)
            {
                if (Cargo.Rows[i]["FrtTax"].ToString() != "")
                {
                    if(Convert.ToDouble(Cargo.Rows[i]["FrtTax"])<99)
                        FrtTax.Add(Convert.ToDouble(Cargo.Rows[i]["FrtTax"]));
                    else
                        FrtTax.Add(99);
                }
                else
                {
                    FrtTax.Add(0);
                }
            }
            return FrtTax;
        }
        public List<double> GetLinerTerm()
        {
            SEData.CargoDataTable Cargo = DataSource.Cargo;
            List<double> LinerTerm = new List<double>();
            for (int i = 0; i < Cargo.Rows.Count; i++)
            {
                if (Cargo.Rows[i]["LinerTerm"].ToString() != "")
                {
                    LinerTerm.Add(Convert.ToDouble(Cargo.Rows[i]["LinerTerm"]));
                }
                else
                {
                    LinerTerm.Add(0);
                }
            }
            return LinerTerm;
        }
        public List<double> GetQuantity()
        {
            SEData.CargoDataTable Cargo = DataSource.Cargo;
            List<double> Quantity = new List<double>();
            for (int i = 0; i < Cargo.Rows.Count; i++)
            {
                if (Cargo.Rows[i]["Quantity"].ToString() != "")
                {
                   Quantity.Add(Convert.ToDouble(Cargo.Rows[i]["Quantity"]));
                }
                else
                {
                   Quantity.Add(0);
                }
            }
            return Quantity;
        }
        public List<double> GetFrt()
        {
            SEData.CargoDataTable Cargo = DataSource.Cargo;
            List<double> Frt = new List<double>();
            for (int i = 0; i < Cargo.Rows.Count; i++)
            {
                if (Cargo.Rows[i]["Frt"].ToString() != "")
                {
                    Frt.Add(Convert.ToDouble(Cargo.Rows[i]["Frt"]));
                }
                else
                {
                    Frt.Add(0);
                }
            }
            return Frt;
        }


        #endregion
        #region PortRotation
        //Set
        public void PortRotationSet()
        {
            for (int i = 0; i < DataSource.PortR.Rows.Count; i++)
            {
                if ((DataSource.PortR.Rows[i]["Name"].ToString() != "") && (getNextPortIndex(i) != 0))
                {
                    if (DataSource.PortR.Rows[i]["Type"].Equals("Ballast"))
                    {
                        DataSource.PortR.Rows[getNextPortIndex(i)]["Spd"] = DataSource.VPSpd.Rows[0]["Ballast"];
                    }
                    else
                    {
                        DataSource.PortR.Rows[getNextPortIndex(i)]["Spd"] = DataSource.VPSpd.Rows[0]["Laden"];
                    }
                }
            }//VesselSpeedSet
        }
        public void PortRSeaSet()
        {
            SEData.PortRDataTable PortR = DataSource.PortR;
            Service.Service.PortRotationService Portservice = new Service.Service.PortRotationService();
            int SeaIndex = 0;
            List<double> SeaList = new List<double>();
            List<double> distance = getDistance(), wf = getWF(), spd = getSpd();
            // Cal Value
            for (int i = 0; i < getSpd().Count; i++)
            {
                double sea = Portservice.CalSea(distance[i], wf[i], spd[i]);
                SeaList.Add(sea);
            }
            // Set Value
            for (int i = 0; i < PortR.Rows.Count; i++)
            {
                if ((PortR.Rows[i]["Name"].ToString() != ""))
                {                    
                    if (SeaIndex !=0)
                    PortR.Rows[i]["Sea"] = SeaList[SeaIndex];
                    SeaIndex++;
                }
            }           
        }
        public string PortRTimeSet()
        {
            List<DateTime> TimeList = new List<DateTime>();
            Service.Service.PortRotationService Portservice = new Service.Service.PortRotationService();
            SEData.PortRDataTable PortR = DataSource.PortR;
            // Cal Value
            bool AssignBallast = true;
            DateTime datetime = new DateTime(DateTime.MinValue.Ticks);
            DateTime datetimeCheck = new DateTime(DateTime.MinValue.Ticks);
            int NextIndex = getNextPortIndex(0);
            if (PortR.Rows[0]["Name"].ToString() == "")
            {
                if (PortR.Rows[NextIndex]["Type"].ToString() != "Ballast" && PortR.Rows[NextIndex]["Arrival"].ToString() != "")
                    datetime = (DateTime)PortR.Rows[NextIndex]["Arrival"];
                else if (PortR.Rows[NextIndex]["Depature"].ToString() != "")
                    datetime = (DateTime)PortR.Rows[NextIndex]["Depature"];  
            }
            else if (PortR.Rows[0]["Depature"].ToString() != "")
            {
                datetime = (DateTime)PortR.Rows[0]["Depature"];
                AssignBallast = false;
            }
            if(datetime != datetimeCheck)
            {
                TimeList = Portservice.CalTime(datetime, getSea(), getSeaMargin(), getIdle(), getIdleMargin(), getWork(), getWorkMargin(), AssignBallast);
                // Set Value
                int TimeListIndex = 0;
                for (int i = 0; i < PortR.Rows.Count; i++)
                {
                    if (PortR.Rows[i]["Name"].ToString() != "")
                    {
                        PortR.Rows[i]["Arrival"] = TimeList[TimeListIndex];
                        TimeListIndex++;
                        PortR.Rows[i]["Depature"] = TimeList[TimeListIndex];
                        TimeListIndex++;
                    }
                }
                //TimeMin
                DataSource.SummaryValues.Rows[0]["TimeMin"] = TimeList.First();
            }            
            if (TimeList.Count > 0)
                return TimeList.First().ToString() + " ~ " + TimeList.Last().ToString();
            else
                return "";
        }
        public void PortRotationValidate()
        {
            SEData.PortRDataTable PortR = DataSource.PortR;
            for (int i = 0; i < PortR.Rows.Count; i++)
            {
                if (Convert.ToDouble(PortR.Rows[i]["Spd"].ToString()) > 99)
                {
                    PortR.Rows[i]["Spd"] = 99;
                }
                if (Convert.ToDouble(PortR.Rows[i]["Idle"].ToString()) > 99)
                {
                    PortR.Rows[i]["Idle"] = 99;
                }
                if (Convert.ToDouble(PortR.Rows[i]["Work"].ToString()) > 99)
                {
                    PortR.Rows[i]["Work"] = 99;
                }
            }
            if (Convert.ToDouble(PortR.Rows[PortR.Rows.Count - 1]["Sea"].ToString()) > 99)
            {
                PortR.Rows[PortR.Rows.Count - 1]["Sea"] = 99;
            }

        }
        //GET
        public List<double> getDistance()
        {
            SEData.PortRDataTable PortR = DataSource.PortR;
            List<double> DistanceList = new List<double>();
            for (int i = 0; i < PortR.Rows.Count; i++)
            {
                if (PortR.Rows[i]["Distance"].ToString() != "")
                {
                    DistanceList.Add((double)PortR.Rows[i]["Distance"]);
                }
                else if (PortR.Rows[i]["Name"].ToString() != "")
                {
                    DistanceList.Add(0);
                }
            }
            return DistanceList;
        }
        public List<double> getSeca()
        {
            SEData.PortRDataTable PortR = DataSource.PortR;
            List<double> SecaList = new List<double>();
            for (int i = 0; i < PortR.Rows.Count; i++)
            {
                if (PortR.Rows[i]["Seca"].ToString() != "")
                {
                    SecaList.Add((double)PortR.Rows[i]["Seca"]);
                }
                else if (PortR.Rows[i]["Name"].ToString() != "")
                {
                    SecaList.Add(0);
                }
            }
            return SecaList;
        }
        public List<double> getWF()
        {
            SEData.PortRDataTable PortR = DataSource.PortR;
            List<double> WFList = new List<double>();
            for (int i = 0; i < PortR.Rows.Count; i++)
            {
                if (PortR.Rows[i]["WF"].ToString() != "")
                {
                    WFList.Add((double)PortR.Rows[i]["WF"]);
                }
                else if (PortR.Rows[i]["Name"].ToString() != "")
                {
                    WFList.Add(0);
                }
            }
            return WFList;
        }
        public List<double> getSpd()
        {
            SEData.PortRDataTable PortR = DataSource.PortR;
            List<double> SpdList = new List<double>();
            for (int i = 0; i < PortR.Rows.Count; i++)
            {
                if (PortR.Rows[i]["Spd"].ToString() != "")
                {
                    SpdList.Add((double)PortR.Rows[i]["Spd"]);
                }
                else if (PortR.Rows[i]["Name"].ToString() != "")
                {
                    SpdList.Add(0);
                }
            }
            return SpdList;
        }
        public List<double> getSea()
        {
            SEData.PortRDataTable PortR = DataSource.PortR;
            List<double> SeaList = new List<double>();
            for (int i = 0; i < PortR.Rows.Count; i++)
            {
                if (PortR.Rows[i]["Sea"].ToString() != "" && PortR.Rows[i]["Name"].ToString()!="")
                {
                    SeaList.Add((double)PortR.Rows[i]["Sea"]);
                }
                else if (PortR.Rows[i]["Name"].ToString() != "")
                {
                    SeaList.Add(0);
                }
            }
            return SeaList;
        }
        public List<double> getDistanceSea()
        {
            Service.Service.PortRotationService Portservice = new Service.Service.PortRotationService();
            List<double> DistanceSea = new List<double>();
            SEData.PortRDataTable PortR = DataSource.PortR;
            List<double> SpdList = getSpd();
            List<double> DistanceList = getDistance();
            List<double> SecaList = getSeca();
            List<double> WFList = getWF();
            for (int i = 0; i < DistanceList.Count; i++)
            {
                if (SpdList.Count > 0)
                    DistanceSea.Add(Portservice.CalSea(DistanceList[i]-SecaList[i],WFList[i],SpdList[i]));
                else
                    DistanceSea.Add(0);
            }
            return DistanceSea;
        }
        public List<double> getSecaSea()
        {
            Service.Service.PortRotationService Portservice = new Service.Service.PortRotationService();
            List<double> SecaSeaList = new List<double>();
            SEData.PortRDataTable PortR = DataSource.PortR;
            List<double> SpdList = getSpd();
            List<double> SecaList = getSeca();
            List<double> WFList = getWF();
            for (int i = 0; i < SecaList.Count; i++)
            {
                if (SpdList.Count>0)
                {
                    SecaSeaList.Add(Portservice.CalSea(SecaList[i], WFList[i], SpdList[i]));
                }
                else
                {
                    SecaList.Add(0);
                }
            }
            return SecaSeaList;
        }
        public List<double> getIdle()
        {
            SEData.PortRDataTable PortR = DataSource.PortR;
            List<double> IdleList = new List<double>();
            for (int i = 0; i < PortR.Rows.Count; i++)
            {
                if (PortR.Rows[i]["Idle"].ToString() != "" && PortR.Rows[i]["Name"].ToString() != "")
                {
                    IdleList.Add((double)PortR.Rows[i]["Idle"]);
                }
                else if (PortR.Rows[i]["Name"].ToString() != "")
                {
                    IdleList.Add(0);
                }
            }
            return IdleList;
        }
        public List<double> getWork()
        {
            SEData.PortRDataTable PortR = DataSource.PortR;
            List<double> WorkList = new List<double>();
            for (int i = 0; i < PortR.Rows.Count; i++)
            {
                if (PortR.Rows[i]["Work"].ToString() != "" && PortR.Rows[i]["Name"].ToString() != "")
                {
                    WorkList.Add((double)PortR.Rows[i]["Work"]);
                }
                else if (PortR.Rows[i]["Name"].ToString() != "")
                {
                    WorkList.Add(0);
                }
            }
            return WorkList;
        }
        public List<double> getDem()
        {
            SEData.PortRDataTable PortR = DataSource.PortR;
            List<double> DemList = new List<double>();
            for (int i = 0; i < PortR.Rows.Count; i++)
            {
                if (PortR.Rows[i]["Dem"].ToString() != "")
                {
                    DemList.Add((double)PortR.Rows[i]["Dem"]);
                }
                else if(PortR.Rows[i]["Name"].ToString() != "")
                {
                    DemList.Add(0);
                }
            }
            return DemList;
        }
        public List<double> getDes()
        {
            SEData.PortRDataTable PortR = DataSource.PortR;
            List<double> DesList = new List<double>();
            for (int i = 0; i < PortR.Rows.Count; i++)
            {
                if (PortR.Rows[i]["Des"].ToString() != "")
                {
                    DesList.Add((double)PortR.Rows[i]["Des"]);
                }
                else if (PortR.Rows[i]["Name"].ToString() != "")
                {
                    DesList.Add(0);
                }
            }
            return DesList;
        }
        public List<double> getPortCharge()
        {
            SEData.PortRDataTable PortR = DataSource.PortR;
            List<double> PortChargeList = new List<double>();
            for (int i = 0; i < PortR.Rows.Count; i++)
            {
                if (PortR.Rows[i]["PortCharge"].ToString() != "")
                {
                    PortChargeList.Add((double)PortR.Rows[i]["PortCharge"]);
                }
                else if (PortR.Rows[i]["Name"].ToString() != "")
                {
                    PortChargeList.Add(0);
                }
            }
            return PortChargeList;
        }
        public double getSeaMargin()
        {
            object value = DataSource.PortR.Rows[DataSource.PortR.Rows.Count - 1]["Sea"];
            if (value.ToString() != "")
            {
                return (double)value;
            }
            else
            {
                return 0;
            }
        }
        public double getSecaSeaMargin()
        {           
            double sumDistance, sumSeca;
            sumDistance = Service.CalCulator.Sum(getDistance());
            sumSeca = Service.CalCulator.Sum(getSeca());

            object value = DataSource.PortR.Rows[DataSource.PortR.Rows.Count - 1]["Sea"];
            if (value.ToString() != "" && sumDistance > 0)
            {
                return (double)value*(sumSeca/sumDistance);
            }
            else
            {
                return 0;
            }
        }
        public double getWorkMargin()
        {
            object value = DataSource.PortR.Rows[DataSource.PortR.Rows.Count - 1]["Work"];
            if (value.ToString() != "")
            {
                return (double)value;
            }
            else
            {
                return 0;
            }
        }
        public double getIdleMargin()
        {
            object value = DataSource.PortR.Rows[DataSource.PortR.Rows.Count - 1]["Idle"];
            if (value.ToString() != "")
            {
                return (double)value;
            }
            else
            {
                return 0;
            }
        }
        public List<bool> getBallastTypes()
        {
            SEData.PortRDataTable PortR = DataSource.PortR;
            List<bool> result = new List<bool>();
            for (int i = 0; i < PortR.Rows.Count; i++)
            {
                if (PortR.Rows[i]["Name"].ToString() != "" && PortR.Rows[i]["Type"].ToString()=="Ballast")
                {
                    result.Add(true);
                }
                else if(PortR.Rows[i]["Name"].ToString() != "")
                {
                    result.Add(false);
                }
            }
            return result;
        }
        public double getPortTime()
        {
            return Service.CalCulator.Sum(getIdle()) + getIdleMargin() + Service.CalCulator.Sum(getWork()) + getWorkMargin();
        }

        public double getSecaTime()
        {

            return Service.CalCulator.Sum(getSecaSea())+ getSecaSeaMargin();
        }

        public double getLadenTime()
        {

            return Service.CalCulator.Sum(getSea()) - getBallastTime() + getSeaMargin();
        }

        public double getBallastTime()
        {
            SEData.PortRDataTable PortR = DataSource.PortR;
            List<double> result = new List<double>();
            for (int i = 0; i < PortR.Rows.Count; i++)
            {
                if (PortR.Rows[i]["Name"].ToString() != "" && PortR.Rows[i]["Type"].ToString() == "Ballast" && PortR.Rows[i]["Sea"].ToString() != "")
                {
                    result.Add((double)PortR.Rows[i]["Sea"]);
                }
                else
                {
                    result.Add(0);
                }                
            }
            if (Service.CalCulator.Sum(getDistance()) >0)
            {

            }
            double ballastMargin = (Service.CalCulator.Sum(result) / Service.CalCulator.Sum(getDistance())) * getSeaMargin();
            return Service.CalCulator.Sum(result)+ ballastMargin;
        }

        //Remove Event
        public void PortRemoveEvent(int currrowindex)
        {
            SEData.PortRDataTable PortR = DataSource.PortR;
            object dbnull = DBNull.Value;
            int nextrow = getNextPortIndex(currrowindex);
            PortR.Rows[nextrow]["Distance"] = dbnull;
            PortR.Rows[nextrow]["Seca"] = dbnull;
            PortR.Rows[nextrow]["Sea"] = dbnull;
        }
        #endregion
        #region Operation Expense
        public void PortRChangedOE()
        {
            double DemDes;
            DemDes = Service.CalCulator.Sum(getDes())-Service.CalCulator.Sum(getDem());
            SetDemDes(DemDes);

            double PortCharge;
            PortCharge = Service.CalCulator.Sum(getPortCharge());
            SetPortCharge(PortCharge);
        }
        public void CargoChangedOE()
        {   
            double Acomm =0, Brkg=0, FrtTax=0;
            List<double> AcommList = GetAComm();
            List<double> BrkgList = GetBrkg();
            List<double> FrtTaxList = GetFrtTax();
            List<double> RevenueList = GetRevenue();
            for (int i = 0; i < RevenueList.Count; i++)
            {
                Acomm += RevenueList[i] * AcommList[i];
                Brkg += RevenueList[i] * BrkgList[i];
                FrtTax += RevenueList[i] * FrtTaxList[i];
            }
            SetAddComm(Acomm / 100);
            SetBrokerage(Brkg / 100);
            SetFreightTax(FrtTax / 100);

            double LinerTerm;
            LinerTerm = Service.CalCulator.Sum(GetLinerTerm());
            SetLinerTerms(LinerTerm);
        }
        public List<double> getOEValue()
        {
            List<double> OEValueList = new List<double>();
            for (int i = 0; i < DataSource.ResultOE.Rows.Count; i++)
            {
                if (DataSource.ResultOE.Rows[i]["Value1"].ToString() != "")
                {
                    OEValueList.Add((double)DataSource.ResultOE.Rows[i]["Value1"]);
                }
                else
                {
                    OEValueList.Add(0);
                }
                if (DataSource.ResultOE.Rows[i]["Value2"].ToString() != "")
                {
                    OEValueList.Add((double)DataSource.ResultOE.Rows[i]["Value2"]);
                }
                else
                {
                    OEValueList.Add(0);
                }
            }
            return OEValueList;
        }
        //Set
        public void SetDemDes(double DemDes)
        {
            DataSource.ResultOE.Rows[0]["Value1"] = DemDes;
        }
        public void SetBrokerage(double Brokerage)
        {
            DataSource.ResultOE.Rows[2]["Value1"] = Brokerage;
        }
        public void SetAddComm(double Addcomm)
        {
            DataSource.ResultOE.Rows[1]["Value1"] = Addcomm;
        }
        public void SetFreightTax(double FreightTax)
        {
            DataSource.ResultOE.Rows[3]["Value1"] = FreightTax;
        }
        public void SetLinerTerms(double LinerTerms)
        {
            DataSource.ResultOE.Rows[4]["Value1"] = LinerTerms;
        }
        public void SetPortCharge(double PortCharge)
        {
            DataSource.ResultOE.Rows[5]["Value1"] = PortCharge;
        }
        public void SetBunkerExpense()
        {   
            DataSource.ResultOE.Rows[0]["Value2"] = SetBE();
        }
        #endregion
        #region Bunker Expense
        //Get
        public double getFOPrice()
        {
            object FOPrice = DataSource.ResultBE.Rows[0]["ValueFO"];
            if (FOPrice.ToString() != "")
            {
                return (double)FOPrice;
            }
            else
            {
                return 0;
            }
        }
        public double getLSFOPrice()
        {
            object LSFOPrice = DataSource.ResultBE.Rows[3]["ValueFO"];
            if (LSFOPrice.ToString() != "")
            {
                return (double)LSFOPrice;
            }
            else
            {
                return 0;
            }
        }
        public double getDOPrice()
        {
            object DOPrice = DataSource.ResultBE.Rows[0]["ValueDO"];
            if (DOPrice.ToString() != "")
            {
                return (double)DOPrice;
            }
            else
            {
                return 0;
            }
        }
        public double getLSDOPrice()
        {
            object LSDOPrice = DataSource.ResultBE.Rows[3]["ValueDO"];
            if (LSDOPrice.ToString() != "")
            {
                return (double)LSDOPrice;
            }
            else
            {
                return 0;
            }
        }
        //Set
        public double SetBE()
        {
            //Get
            double BunkerExpense;
            double Ballast = getFOBallast();
            double Laden = getFOLaden();
            double Idle = getFOIdle();
            double Work = getFOWork();
            double Sea = getDOSea();
            double DOIdle = getDOIdle();
            double DoWork = getDOWork();
            double LSFOBallast = getLSFOBallast();
            double LSFOLaden = getLSFOLaden();
            double LSFOIdle = getLSFOIdle();
            double LSFOWork = getLSFOWork();
            double LSDOSea = getLSDOSea();
            double LSDOIdle = getLSDOIdle();
            double LSDOWork = getLSDOWork();
            List<double> DistanceSeaList = getDistanceSea();
            double SeaMargin = getSeaMargin() - getSecaSeaMargin();
            List<double> SecaSeaList = getSecaSea();
            double SecaMargin = getSecaSeaMargin();
            List<double> IdleList = getIdle();
            double IdleMargin = getIdleMargin();
            List<double> WorkList = getWork();
            double WorkMargin = getWorkMargin();
            List<bool> AssignBallast = getBallastTypes();
            List<double> Zero = new List<double>();
            Zero.Add(0);
            Service.Service.BEService beservice = new Service.Service.BEService();
            //Cal
            double FOconsumption = beservice.CalConsumption(Ballast, Laden, Idle, Work, DistanceSeaList, SeaMargin, IdleList, IdleMargin, WorkList, WorkMargin, AssignBallast);
            double LSFOconsumption = beservice.CalConsumption(LSFOBallast, LSFOLaden, LSFOIdle, LSFOWork, SecaSeaList, SecaMargin, Zero, IdleMargin, Zero, WorkMargin, AssignBallast);
            double DOconsumption = beservice.CalConsumption(Sea,Sea,DOIdle,DoWork, DistanceSeaList, SeaMargin,IdleList,IdleMargin, WorkList, WorkMargin, AssignBallast);
            double LSDOconsumption = beservice.CalConsumption(LSDOSea, LSDOSea, LSDOIdle, LSDOWork, SecaSeaList, SecaMargin, Zero, IdleMargin, Zero, WorkMargin, AssignBallast); ;
            double FOExpense = FOconsumption * getFOPrice();
            double LSFOExpense = LSFOconsumption * getLSFOPrice();
            double DOExpense = DOconsumption * getDOPrice();
            double LSDOExpense = LSDOconsumption * getLSDOPrice();
            BunkerExpense = FOExpense + LSFOExpense + DOExpense + LSDOExpense;

            //Set

            DataSource.ResultBE.Rows[1]["ValueFO"] = FOconsumption;
            DataSource.ResultBE.Rows[1]["ValueDO"] = DOconsumption;
            DataSource.ResultBE.Rows[2]["ValueFO"] = FOExpense;
            DataSource.ResultBE.Rows[2]["ValueDO"] = DOExpense;

            DataSource.ResultBE.Rows[4]["ValueFO"] = LSFOconsumption;
            DataSource.ResultBE.Rows[4]["ValueDO"] = LSDOconsumption;
            DataSource.ResultBE.Rows[5]["ValueFO"] = LSFOExpense;
            DataSource.ResultBE.Rows[5]["ValueDO"] = LSDOExpense;

            return BunkerExpense;

        }
        public void SetFO (double FOConsumption, double FOExpense)
        {
            DataSource.ResultBE.Rows[1]["ResultBEFOValue"] = FOConsumption;
            DataSource.ResultBE.Rows[2]["ResultBEFOValue"] = FOExpense;
        }
        public void SetLSFO(double LSFOConsumption, double LSFOExpense)
        {
            DataSource.ResultBE.Rows[4]["ResultBEFOValue"] = LSFOConsumption;
            DataSource.ResultBE.Rows[5]["ResultBEFOValue"] = LSFOExpense;
        }

        public void SetDO(double DOConsumption, double DOExpense)
        {
            DataSource.ResultBE.Rows[1]["ResultBEDOValue"] = DOConsumption;
            DataSource.ResultBE.Rows[2]["ResultBEDOValue"] = DOExpense;
        }
        public void SetLSDO (double LSDOConsumption, double LSDOExpense)
        {
            DataSource.ResultBE.Rows[4]["ResultBEDOValue"] = LSDOConsumption;
            DataSource.ResultBE.Rows[5]["ResultBEDOValue"] = LSDOExpense;
        }
        #endregion
        #region Result
        public void setResult()
        {
            double TotalHire = 0, TotalExpense = 0, Profit = 0, TotalRevenue = 0;
            TotalHire = getTotalDuration() * getNetHire();
            TotalExpense = getOpExpense() + TotalHire;
            Profit = getOpProfit() - TotalHire;
            TotalRevenue = Service.CalCulator.Sum(GetRevenue());
            setTotalRevenue(TotalRevenue);
            setTotalHire(TotalHire);
            setTotalExpense(TotalExpense);
            setPROFIT(Profit);
        }

        public double getTotalDuration()
        {
            return Service.CalCulator.Sum(getSea()) + Service.CalCulator.Sum(getIdle()) + Service.CalCulator.Sum(getWork()) + getSeaMargin() + getIdleMargin() + getWorkMargin();
        }

        public void setCB()
        {
            double NetHire=0, CBase=0;
            NetHire = getHireDay() * (100 - getHAdComm()) / 100;
            double TotalRevenue = 0, OpExpense = 0, OpProfit = 0, TotalDuration =0 ;
            TotalRevenue = Service.CalCulator.Sum(GetRevenue());
            OpExpense = Service.CalCulator.Sum(getOEValue());
            OpProfit = TotalRevenue - OpExpense;
            setTotalExpense(TotalRevenue);
            setOpExpense(OpExpense);
            TotalDuration = getTotalDuration();
            setOpProfit(OpProfit);
            if (TotalDuration >0 )
            {
                CBase = (getTotalRevenue() - getOpExpense()) / TotalDuration;
            }         

            setNetHire(NetHire);
            setCBase(CBase);
        }
        //Get
        public double getHireDay()
        {
            object Value = DataSource.ResultCB.Rows[0]["Value"];
            if (Value.ToString() != "")
            {
                return (double)Value;
            }
            else
            {
                return 0;
            }
        }
        public double getHAdComm()
        {
            object Value = DataSource.ResultCB.Rows[1]["Value"];
            if (Value.ToString() != "")
            {
                return (double)Value;
            }
            else
            {
                return 0;
            }
        }
        public double getNetHire()
        {
            object Value = DataSource.ResultCB.Rows[2]["Value"];
            if (Value.ToString() != "")
            {
                return (double)Value;
            }
            else
            {
                return 0;
            }
        }
        public double getCBase()
        {
            object Value = DataSource.ResultCB.Rows[3]["Value"];
            if (Value.ToString() != "")
            {
                return (double)Value;
            }
            else
            {
                return 0;
            }
        }
        public double getTotalRevenue()
        {
            object Value = DataSource.Result.Rows[0]["Value"];
            if (Value.ToString() != "")
            {
                return (double)Value;
            }
            else
            {
                return 0;
            }
        }
        public double getOpExpense()
        {
            object Value = DataSource.Result.Rows[1]["Value"];
            if (Value.ToString() != "")
            {
                return (double)Value;
            }
            else
            {
                return 0;
            }
        }
        public double getOpProfit()
        {
            object Value = DataSource.Result.Rows[2]["Value"];
            if (Value.ToString() != "")
            {
                return (double)Value;
            }
            else
            {
                return 0;
            }
        }
        public double getTotalHire()
        {
            object Value = DataSource.Result.Rows[3]["Value"];
            if (Value.ToString() != "")
            {
                return (double)Value;
            }
            else
            {
                return 0;
            }
        }
        public double getTotalExpense()
        {
            object Value = DataSource.Result.Rows[4]["Value"];
            if (Value.ToString() != "")
            {
                return (double)Value;
            }
            else
            {
                return 0;
            }
        }
        //Set
        public void setNetHire(double NetHire)
        {
            DataSource.ResultCB.Rows[2]["Value"] = NetHire;
        }
        public void setCBase(double CBase)
        {
            DataSource.ResultCB.Rows[3]["Value"] = CBase;
        }
        public void setTotalRevenue(double TotalRevenue)
        {
            DataSource.Result.Rows[0]["Value"] = TotalRevenue;
        }
         public void setOpExpense(double OpExpense)
        {
            DataSource.Result.Rows[1]["Value"] = OpExpense;
        }
        public void setOpProfit(double OpProfit)
        {
            DataSource.Result.Rows[2]["Value"] = OpProfit;
        }
        public void setTotalHire(double TotalHire)
        {
            DataSource.Result.Rows[3]["Value"] = TotalHire;
        }
        public void setTotalExpense(double TotalExpense)
        {
            DataSource.Result.Rows[4]["Value"] = TotalExpense;
        }
        public void setPROFIT(double PROFIT)
        {
            DataSource.Result.Rows[5]["Value"] = PROFIT;
        }
        #endregion
    }
}
