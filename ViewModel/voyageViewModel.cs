﻿using System;
using Service;
using Model.DataSet;
namespace ViewModel
{
    public class voyageViewModel
    {
        public voyageViewModel(SEData Data)
        {
            dataControl = new DataControl(Data);
        }
        private DataControl dataControl;

        public void VesselSpeedChanged()
        {
            dataControl.PortRotationSet();
        }
        public void VesselFusselChanged()
        {
            BEChanged();
            ResultChanged();
        }     
        public void CargoChanged()
        {
            dataControl.CargoChanged();
            BEChanged();
            dataControl.CargoChangedOE();
            ResultChanged();
        }
        public void PortRotationChanged()
        {
            dataControl.PortRSeaSet();
            dataControl.PortRChangedOE();
            BEChanged();
            ResultChanged();
        }

        public void BEChanged()
        {
            dataControl.SetBunkerExpense();
        }

        public void ResultChanged()
        {
            dataControl.setCB();
            dataControl.setResult();
        }

        public void OEChanged()
        {
            ResultChanged();
        }

        public string DurationChanged()
        {
            string Duration = String.Format("Total Duration : {0:#0.00} Days (Ballast: {1:#0.00}, Laden: {2:#0.00}, Seca: {3:#0.00}, Port: {4:#0.00})", dataControl.getTotalDuration(),dataControl.getBallastTime(),dataControl.getLadenTime(),
                dataControl.getSecaTime(),dataControl.getPortTime());
            string Time = dataControl.PortRTimeSet();
            if (Time != "")
                return Duration + " / " + Time;
            else
                return Duration;
        }
        public void PortRemoveEvent(int rowindex)
        {
            dataControl.PortRemoveEvent(rowindex);
        }
    }
}
