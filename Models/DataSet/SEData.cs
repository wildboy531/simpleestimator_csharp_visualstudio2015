﻿using System.Data;
using System.Collections.Generic;

namespace Model.DataSet
{
}

namespace Model.DataSet
{
    public partial class SEData
    {
        public System.Boolean DataInit()
        {
            VPVessel.Rows.Add();
            VPSpd.Rows.Add();
            VPDO.Rows.Add();
            VPDO.Rows[0]["LSDO"] = "DO";
            VPDO.Rows.Add();
            VPDO.Rows[1]["LSDO"] = "LSDO";
            VPFO.Rows.Add();
            VPFO.Rows[0]["LSFO"] = "FO";
            VPFO.Rows.Add();
            VPFO.Rows[1]["LSFO"] = "LSFO";
            //
            Cargo.Rows.Add();
            Cargo.Rows[0]["Index"] = 1;
            Cargo.Rows.Add();
            Cargo.Rows[1]["Index"] = 2;
            //
            PortR.Rows.Add();
            PortR.Rows[0]["Index"] = 1;
            PortR.Rows[0]["Type"] = "Ballast";
            PortR.Rows.Add();
            PortR.Rows[1]["Index"] = 2;
            PortR.Rows.Add();
            PortR.Rows[2]["Index"] = 3;
            PortR.Rows.Add();
            PortR.Rows[3]["Index"] = 4;
            PortR.Rows.Add();
            PortR.Rows[4]["Index"] = 5;
            PortR.Rows.Add();
            PortR.Rows[5]["Type"] = "Margin";
            //
            ResultOE.Rows.Add();
            ResultOE.Rows[0]["Type1"] = "Dem/Des";
            ResultOE.Rows[0]["Type2"] = "Bunker Expense";
            ResultOE.Rows.Add();
            ResultOE.Rows[1]["Type1"] = "Add Comm.";
            ResultOE.Rows[1]["Type2"] = "C.E.V";
            ResultOE.Rows.Add();
            ResultOE.Rows[2]["Type1"] = "Brokerage";
            ResultOE.Rows[2]["Type2"] = "ILOHC";
            ResultOE.Rows.Add();
            ResultOE.Rows[3]["Type1"] = "Freight Tax";
            ResultOE.Rows[3]["Type2"] = "Ballast Bonus";
            ResultOE.Rows.Add();
            ResultOE.Rows[4]["Type1"] = "Liner Terms";
            ResultOE.Rows[4]["Type2"] = "Routing Service";
            ResultOE.Rows.Add();
            ResultOE.Rows[5]["Type1"] = "Port Charge";
            ResultOE.Rows[5]["Type2"] = "Others";
            //
            ResultBE.Rows.Add();
            ResultBE.Rows[0]["Type"] = "Price / MT";
            ResultBE.Rows[0]["TypeFO"] = "FO";
            ResultBE.Rows[0]["TypeDO"] = "DO";
            ResultBE.Rows.Add();
            ResultBE.Rows[1]["Type"] = "ConSumption";
            ResultBE.Rows[1]["TypeFO"] = "FO";
            ResultBE.Rows[1]["TypeDO"] = "DO";
            ResultBE.Rows.Add();
            ResultBE.Rows[2]["Type"] = "Expense";
            ResultBE.Rows[2]["TypeFO"] = "FO";
            ResultBE.Rows[2]["TypeDO"] = "DO";
            ResultBE.Rows.Add();
            ResultBE.Rows[3]["Type"] = "Price / MT";
            ResultBE.Rows[3]["TypeFO"] = "LSFO";
            ResultBE.Rows[3]["TypeDO"] = "LSDO";
            ResultBE.Rows.Add();
            ResultBE.Rows[4]["Type"] = "ConSumption";
            ResultBE.Rows[4]["TypeFO"] = "LSFO";
            ResultBE.Rows[4]["TypeDO"] = "LSDO";
            ResultBE.Rows.Add();
            ResultBE.Rows[5]["Type"] = "Expense";
            ResultBE.Rows[5]["TypeFO"] = "LSFO";
            ResultBE.Rows[5]["TypeDO"] = "LSDO";
            //
            ResultCB.Rows.Add();
            ResultCB.Rows[0]["Type"] = "Hire / Day";
            ResultCB.Rows.Add();
            ResultCB.Rows[1]["Type"] = "H/Add Comm.";
            ResultCB.Rows.Add();
            ResultCB.Rows[2]["Type"] = "Net Hire";
            ResultCB.Rows.Add();
            ResultCB.Rows[3]["Type"] = "C/Base";
            //
            Result.Rows.Add();
            Result.Rows[0]["Type"] = "Total Revenue";
            Result.Rows.Add();
            Result.Rows[1]["Type"] = "Op. Expense";
            Result.Rows.Add();
            Result.Rows[2]["Type"] = "Op. Profit";
            Result.Rows.Add();
            Result.Rows[3]["Type"] = "Total Hire";
            Result.Rows.Add();
            Result.Rows[4]["Type"] = "Total Expense";
            Result.Rows.Add();
            Result.Rows[5]["Type"] = "PROFIT";

            SummaryValues.Rows.Add();
            SummaryValues.Rows[0]["Frt"] = 0;
            SummaryValues.Rows[0]["AComm"] = 0;
            SummaryValues.Rows[0]["FrtTax"] = 0;
            SummaryValues.Rows[0]["Brkg"] = 0;
            return true;
        }
        public void PortRInit()
        {
            PortR.Rows.Add();
            PortR.Rows[0]["Index"] = 1;
            PortR.Rows[0]["Type"] = "Ballast";
            PortR.Rows.Add();
            PortR.Rows[1]["Index"] = 2;
            PortR.Rows.Add();
            PortR.Rows[2]["Index"] = 3;
            PortR.Rows.Add();
            PortR.Rows[3]["Index"] = 4;
            PortR.Rows.Add();
            PortR.Rows[4]["Index"] = 5;
            PortR.Rows.Add();
            PortR.Rows[5]["Type"] = "Margin";
        }
    }
}
