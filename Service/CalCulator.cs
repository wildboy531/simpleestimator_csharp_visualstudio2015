﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service
{
    public class CalCulator
    {
        public static double Sum(List<double> List)
        {
            double result = 0;
            for (int i = 0; i < List.Count; i++)
            {
                result += List[i];
            }
            return result;
        }
    }
}
