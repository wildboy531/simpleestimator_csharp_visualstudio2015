﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Service
{
    public class Service
    {
        public class Duration
        {
            public double Ballast, Laden, Seca, Port, Total;
            public void InitMemberTo0()
            {
                Ballast = 0;
                Laden = 0;
                Seca = 0;
                Port = 0;
                Total = 0;
            }
        }
        public static List<double> CalMarginAdapt (List<double> AList, double AMargin)
        {
            List<double> ResultList = new List<double>();
            double TotalSum = CalCulator.Sum(AList);
            for (int i = 0; i < AList.Count; i++)
            {
                if (TotalSum != 0)
                {
                    ResultList.Add(AList[i] + (AMargin * (AList[i] / TotalSum)));
                }
                else
                {
                    ResultList.Add(0);
                }
            }
            return ResultList;
        }
        public class PortRotationService
        {            
            public double CalSea(double distance, double wf, double spd)
            {
                double Sea=0;
                if (spd != 0)
                {
                    Sea = (distance / (spd * 24)) + ((distance / (spd*24)) * (wf/100));
                }               
                return Sea;
            }         
            public Duration CalDuration(List<double> SeaList, List<double>SecaSeaList, double SeaMargin, double SecaMargin, List<double> IdleList, double IdleMargin,
                List<double> WorkList, double WorkMargin, List<Boolean> AssignBallast)
            {
                List<double> SeaMarginAdaptList = CalMarginAdapt(SeaList, SeaMargin);
                List<double> IdleMarginAdaptList = CalMarginAdapt(IdleList,IdleMargin);
                List<double> WorkMarginAdaptList = CalMarginAdapt(WorkList,WorkMargin);
                List<double> SecaMarginAdaptList = CalMarginAdapt(SecaSeaList,SecaMargin);
                double SecaPort = 0;
                Duration duration = new Duration();
                duration.InitMemberTo0();

                for (int i = 0; i < SeaMarginAdaptList.Count; i++)
                {
                    if (AssignBallast[i])
                    {
                        duration.Ballast += SeaMarginAdaptList[i];
                    }
                    else
                    {
                        duration.Laden += SeaMarginAdaptList[i];
                    }
                }
                for (int i = 0; i < IdleList.Count ; i++)
                {
                    if (SecaSeaList[i] != 0) {
                        SecaPort += IdleMarginAdaptList[i] + WorkMarginAdaptList[i];
                    }
                }
                duration.Seca = CalCulator.Sum(SecaMarginAdaptList) + SecaPort;
                duration.Port = CalCulator.Sum(IdleMarginAdaptList) + CalCulator.Sum(WorkMarginAdaptList);
                duration.Total = duration.Seca + duration.Port + duration.Ballast + duration.Laden;
                return duration;
            }        
            // 날짜 사용
            public List<DateTime> CalTime (DateTime datetime, List<double> SeaList, double SeaMargin, List<double> IdleList, double IdleMargin,
                List<double> WorkList, double WorkMargin, Boolean AssignBallast)
            {
                List<double> SeaMarginAdaptList = CalMarginAdapt(SeaList, SeaMargin);
                List<double> IdleMarginAdaptList = CalMarginAdapt(IdleList, IdleMargin);
                List<double> WorkMarginAdaptList = CalMarginAdapt(WorkList, WorkMargin);

                List<DateTime> datetimeList = new List<DateTime>();
                double OADate = 0;
                DateTime tempDate;
                if (AssignBallast)
                {
                    datetimeList.Add(DateTime.FromOADate(datetime.ToOADate() - IdleMarginAdaptList[0] - WorkMarginAdaptList[0]));
                    datetimeList.Add(datetime);
                    OADate = datetime.ToOADate();
                }
                else
                {
                    datetimeList.Add(datetime);
                    datetimeList.Add(DateTime.FromOADate(datetime.ToOADate() + IdleMarginAdaptList[0] + WorkMarginAdaptList[0]));
                    OADate = datetimeList[1].ToOADate();
                }
                for (int i = 1; i < SeaMarginAdaptList.Count; i++)
                {
                    OADate += SeaMarginAdaptList[i];
                    tempDate = DateTime.FromOADate(OADate);
                    datetimeList.Add(tempDate);

                    OADate += IdleMarginAdaptList[i] + WorkMarginAdaptList[i];
                    tempDate = DateTime.FromOADate(OADate);
                    datetimeList.Add(tempDate);
                }
                return datetimeList;
            }
        }
        public class CargoService
        {
            public List<double> CalRevenue (List<double> quantity, List<double> Frt)
            {
                List<double> Revenue = new List<double>();
                for (int i = 0; i < Frt.Count; i++)
                {
                    Revenue.Add(quantity[i] * Frt[i]);
                }
                return Revenue;
            }
            public double CalFrtSummary(List<double> quantity, List<double> Frt)
            {
                double FrtSummary = 0;
                double QuantitySum = 0;
                double TotalSum = 0;
                for (int i = 0; i <quantity.Count; i++)
                {
                    QuantitySum += quantity[i];
                    TotalSum += quantity[i] * Frt[i]; 
                }
                if (QuantitySum != 0)
                {
                    FrtSummary = TotalSum / QuantitySum;
                }
                return FrtSummary;
            }
            public double CalAcommSummary(List<double> Revenue, List<double> AComm)
            {
                double AcommSummary = 0;
                double TotalSum = 0;
                for (int i = 0; i < Revenue.Count; i++)
                {
                    TotalSum += Revenue[i] * AComm[i];
                }
                if (CalCulator.Sum(Revenue) > 0)
                {
                    AcommSummary = TotalSum / CalCulator.Sum(Revenue);
                }
                return AcommSummary;
            }
            public double CalBrkgSummary(List<double> Revenue, List<double> Brkg)
            {
                double BrkgSummary = 0;
                double TotalSum = 0;
                for (int i = 0; i < Revenue.Count; i++)
                {
                    TotalSum += Revenue[i] * Brkg[i];
                }
                if (CalCulator.Sum(Revenue) > 0)
                {
                    BrkgSummary = TotalSum / CalCulator.Sum(Revenue);
                }
                return BrkgSummary;
            }
            public double CalFrtTaxSummary(List<double> Revenue, List<double> FrtTax)
            {
                double FrtTaxSummary = 0;
                double TotalSum = 0;
                for (int i = 0; i < Revenue.Count; i++)
                {
                    TotalSum += Revenue[i] * FrtTax[i];
                }
                if (CalCulator.Sum(Revenue) > 0)
                {
                    FrtTaxSummary = TotalSum / CalCulator.Sum(Revenue);
                }
                return FrtTaxSummary;
            }
        }
        public class OEService
        {
            public double CalDemDes (List<double> Dem , List<double> Des)
            {
                double result = 0;
                for (int i = 0; i < Dem.Count; i++)
                {
                    result += (Des[i] - Dem[i]);
                }
                return result;
            }
            public double CalAddComm(List<double> AComm , List<double> Revenue)
            {
                double result = 0;
                for (int i = 0; i < AComm.Count; i++)
                {
                    result += (AComm[i] * Revenue[i]);
                }
                return result;
            }
            public double CalBrokerage(List<double> Brkg , List<double> Revenue)
            {
                double result = 0;
                for (int i = 0; i < Brkg.Count; i++)
                {
                    result += (Brkg[i] * Revenue[i]);
                }
                return result;
            }
            public double CalFreightTax(List<double> FrtTax, List<double> Revenue)
            {
                double result = 0;
                for (int i = 0; i < FrtTax.Count; i++)
                {
                    result += (FrtTax[i] * Revenue[i]);
                }
                return result;
            }
            public double CalLinerTerms(List<double> LinerTerm)
            {
                double result = 0;
                result += CalCulator.Sum(LinerTerm);
                return result;
            }
            public double CalPortCharge(List<double> PortCharge)
            {
                double result = 0;
                result += CalCulator.Sum(PortCharge);
                return result;
            }
            public double CalBunkerExpense(List<double> ExpenseList)
            {
                return CalCulator.Sum(ExpenseList);
            }
        }
        public class BEService
        {            
            public double CalConsumption(double Ballast, double Laden, double Idle, double Work, List<double> SeaList, double SeaMargin, List<double> IdleList, double IdleMargin, List<double> WorkList, double WorkMargin, List<Boolean> AssignBallast)
            {
                double TotalSum =0;
                double SeaSum = 0, IdleSum = 0, WorkSum = 0, BallastSum = 0, LadenSum = 0;
                List<double> SeaMarginAdaptList = new List<double>();
                List<double> IdleMarginAdaptList = new List<double>();
                List<double> WorkMarginAdaptList = new List<double>();
                SeaMarginAdaptList = Service.CalMarginAdapt(SeaList, SeaMargin);
                IdleMarginAdaptList = Service.CalMarginAdapt(IdleList, IdleMargin);
                WorkMarginAdaptList = Service.CalMarginAdapt(WorkList, WorkMargin);
                
                for (int i = 0; i < SeaMarginAdaptList.Count; i++)
                {
                    if (AssignBallast[i])
                    {
                        BallastSum += SeaMarginAdaptList[i] * Ballast;
                    }
                    else
                    {
                        LadenSum += SeaMarginAdaptList[i] * Laden;
                    }
                }
                for (int i = 0; i < IdleMarginAdaptList.Count; i++)
                {
                    IdleSum += IdleMarginAdaptList[i] * Idle;
                }
                for (int i = 0; i < WorkMarginAdaptList.Count; i++)
                {
                    WorkSum += WorkMarginAdaptList[i] * Work;
                }
                SeaSum = BallastSum + LadenSum;
                TotalSum = SeaSum + IdleSum + WorkSum;
                return TotalSum;
            }
            public double CalExpense(double Consumption, double Price)
            {
                return Consumption * Price;
            }
        } 
        public class ResultService
        {
            public double CalNetHire(double HireDay, double HAddcomm)
            {
                return HireDay * (100 - HAddcomm)/100;
            }
            public double CalCBase(double OperationProfit, double TotalDuration)
            {
                if (TotalDuration == 0)
                {
                    return OperationProfit / TotalDuration;
                }
                else
                {
                    return 0;
                }                    
            }
            public double CalTotalRevenue(List<double> Revenue)
            {
                return CalCulator.Sum(Revenue);
            }
            public double CalOPExpense(List<double> OpExpenseList)
            {
                return CalCulator.Sum(OpExpenseList); 
            }
            public double CalOPProfit(double TotalRevenue, double OpExpense)
            {
                return TotalRevenue - OpExpense;
            }
            public double CalTotalHire (double TotalDuration, double NetHire)
            {
                return TotalDuration * NetHire;
            }
            public double CalTotalExpense (double OpExpense, double ToTalHire)
            {
                return OpExpense + ToTalHire;
            }
            public double CalProfit(double TotalRevenue, double OperationExpense, double TotalHire)
            {
                return TotalRevenue - OperationExpense - TotalHire;
            }
        }
    }
}
